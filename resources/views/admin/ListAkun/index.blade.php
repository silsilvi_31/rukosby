@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Form Acoount User</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal form-label-left">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kode
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="hidden" id="" name="id_user" class="form-control" readonly>
                  <input type="text" id="" name="kode" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="nama" class="form-control " readonly>
                </div>
              </div>
              
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Username
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="username" class="form-control ">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Password
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="password" class="form-control ">
                </div>
              </div>
              {{-- <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Akses
                </label>
                <div class="col-md-6 col-sm-6 ">
                  @php
                      $akses = DB::table('akses')->get();
                  @endphp
                  <select name="akses" id="selAkses" class="form-control">
                    <option value="">-</option>
                    @foreach ($akses as $i)
                      <option value="{{ $i->id }}">{{ $i->nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div> --}}
              
                            
              <div class="ln_solid"></div>
              <div class="item form-group">
                <div class="col-md-6 col-sm-6 offset-md-3">
                  <button type="button" class="btn btn-success btn-kry" onclick="update_account()">Update</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-form-akses" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Form Akses User</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal form-label-left">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kode
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="hidden" id="" name="id_user_akses" class="form-control" readonly>
                  <input type="text" id="" name="kode_akses" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="nama_akses" class="form-control " readonly>
                </div>
              </div>
              
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Akses
                </label>
                <div class="col-md-6 col-sm-6 ">
                  @php
                      $akses = DB::table('akses')->get();
                  @endphp
                  <select name="akses_s" id="selAksesA" class="form-control">
                    <option value="">-</option>
                    @foreach ($akses as $i)
                      <option value="{{ $i->id }}">{{ $i->nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
                                          
              <div class="ln_solid"></div>
              <div class="item form-group">
                <div class="col-md-6 col-sm-6 offset-md-3">
                  <button type="button" class="btn btn-success btn-kry" onclick="update_akses()">Update</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-form-permis" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Form Permission</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kode
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="ed_permis" name="id_user_permis" class="form-control" value="" readonly>
                  <input type="text" id="" name="kode_permis" class="form-control" value="" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="nama_permis" class="form-control " readonly>
                </div>
              </div>
           
              <div class="ln_solid"></div>
              <div class="item form-group">
                <div class="col-md-6 col-sm-6 offset-md-3">
                  {{-- <button type="button" class="btn btn-primary" onclick="tessi()">tes</button> --}}
                  <button type="button" class="btn btn-success btn-kry" onclick="save_permis()">Save</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>List Kode Akun</h2>

            <ul class="nav navbar-right panel_toolbox">
              <a href=" {{ route('account.form') }} " class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <form action="" class="form-verical form-label-left">
                  <div class="col-md-4">
                    <div class="item form-group">
                      <label class="col-form-label col-md-3 label-align" for="first-name">Tanggal
                      </label>
                      <div class="col-md-8">
                        <input type="text" id="tglAkun" name="tgl" class="form-control" value="">
                      </div>
                    </div>                    
                  </div>
                </form>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Kode</th>
                        <th>Akun</th>
                        <th>Qty</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody id="tbListAkun">
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('uyeee');
    // list_akun();

    $('#tglAkun').datetimepicker({
      format: 'DD-MM-YYYY'
    });

    $('#tglAkun').on("dp.change", function (e) {
        var tgl = $(this).val();
        // console.log(tgl);    
      list_akun(tgl);
    }); 


    function list_akun(tgl) {
      // var tgl = $('#tglAkun').val();
      var no = 1;

      $.ajax({
        type    : 'POST',
        url     : '{{ route('akuntansi_list.json') }}',
        data    : { 
                    '_tgl' : tgl 
                  },
        success : function (e) {
          // console.log(e.akun);
          var tbody = '';
          $('#tbListAkun').html('');
          $.each(e.akun, function (key, item) {
            console.log(item);
            tbody += '<tr>';
            tbody += '<td>'+no+++'</td>';
            tbody += '<td>'+item.no_akun+'</td>';
            tbody += '<td>'+item.akun+'</td>';
            tbody += '<td>'+item.qty+'</td>';
            tbody += '<td>'+item.total+'</td>';
            tbody += '</tr>';
          });
          $('#tbListAkun').append(tbody);
        }
      });
    }

    // var datatableAccount = $('#tbAccount').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     ajax: {
    //         type: 'GET',
    //         url: '{{ route('account.datatable') }}', 
    //     },
    //     columns: [
    //       { data: 'DT_RowIndex', name: 'DT_RowIndex' },
    //       { data: 'kode', name: 'kode' },
    //       { data: 'nama', name: 'nama' },
    //       { data: 'username', name: 'username' },
    //       { data: 'email', name: 'email' },
    //       { data: 'role', name: 'role' },
    //       { data: 'akses', name: 'akses' },
    //       { data: 'permis', name: 'permis' },
    //       { data: 'created_at', name: 'created_at' },
    //       { data: 'updated_at', name: 'updated_at' },
    //       { data: 'opsi', name: 'opsi' }
    //     ]
    // });

  </script>
@endpush