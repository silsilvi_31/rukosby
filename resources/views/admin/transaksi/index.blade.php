@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    @php
      $kode = Session::get('code');
      $pesan = Session::get('msg');
    @endphp
    {{-- notif --}}
    @if ($kode)
        @push('js')
          <script type="text/javascript">
            notif({{ $kode }}, "{{ $pesan }}");
          </script>
        @endpush
        @php
            Session::forget('code');
        @endphp
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Transaksi Belum Lunas</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('transaksi.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tb_transaksi" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No. </th>
                        <th>ID Sj</th>
                        <th>Tgl</th>
                        <th>Jenis Bayar</th>
                        <th>Tagihan</th>
                        <th>Keterangan</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    var tb_transaksi = $('#tb_transaksi').DataTable({
        processing: true,
        serverSide: true,
        order: false,
        ajax: {
            type: 'GET',
            url: '{{ route('transaksi.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'id_sj', name: 'id_sj' },
          { data: 'tgl', name: 'tgl' },
          { data: 'jenis_byr', data: 'jenis_byr' },
          { data: 'total', name: 'total' },
          { data: 'keterangan', name: 'keterangan' },
        ]
    });

  </script>
@endpush