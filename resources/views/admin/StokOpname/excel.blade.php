{{-- @extends('master.print')
    
@section('content') --}}
<table>
        <tr>
            <th>Tgl</th>
            <th>No Sj</th>
            <th>Nama Pelanggan</th>
            <th>Ketebalan</th>
            <th>Byk</th>
            <th>Jenis</th>
            <th>Keterangan</th>
            <th>Harga</th>
            <th>Catatan</th>
            <th>Total</th>
            <th>Piutang</th>
            <th>Tgl Lunas</th>
            <th>Bank</th>
            <th>Lebar</th>
            <th>Panjang</th>
            <th>m3</th>
        </tr>
        @foreach ($data as $value)
            <tr>
                <td>{{ date('d-m-yy', strtotime($value->tgl)) }}</td>
                <td>{{ $value->id }}</td>
                <td>{{ $value->nama_pelanggan }}</td>
                <td>{{ $value->ketebalan }}</td>
                <td>{{ $value->qty }}</td>
                <td>{{ $value->jenis }} </td>
                <td>{{ $value->ketr  }}</td>
                <td>{{ $value->harga }}</td>
                <td>{{ $value->catatan }}</td>
                <td>{{ $value->subtotal }}</td>
                <td>{{ $value->piutang }}</td>
                <td>{{ $value->tgl_lunas }}</td>
                <td>{{ $value->bank }}</td>
                <td>{{ $value->lebar }}</td>
                <td>{{ $value->panjang }}</td>
                <td>{{ $value->m3 }}</td>
            </tr>
        @endforeach
    </table>
{{-- @endsection --}}