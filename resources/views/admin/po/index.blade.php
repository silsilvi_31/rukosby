@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <!-- Awal Modal pelunasan -->
    <div class="modal fade bs-example-modal-lg" id="modal_pelunasan" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Pelunasan</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                  <div class="col-md-6 col-sm-6">
                    <div class="item form-group">
                      <label class="col-form-label col-md-4 col-sm-4 label-align" for="first-name">ID</label>
                      <div class="col-md-8 col-sm-8 ">
                        <input type="text" id="Id" name="id" required='required' class="form-control" readonly>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-form-label col-md-4 col-sm-4 label-align" for="first-name">Total Bayar</label>
                      <div class="col-md-8 col-sm-8 ">
                        <input type="text" id="grandTotal" name="grand_total" class="form-control" readonly>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-form-label col-md-4 col-sm-4 label-align" for="first-name">Sisa Tagihan</label>
                      <div class="col-md-8 col-sm-8 ">
                        <input type="text" id="sisaTagihan" name="sisa_tagihan" class="form-control" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                    <div class="item form-group">
                      <label class="col-form-label col-md-4 col-sm-4 label-align" for="first-name">Tgl <span class="required">*</span>
                      </label>
                      <div class="col-md-8 col-sm-8 ">
                        <input type="text" id="Tgl" name="tgl" required='required' class="form-control" value="" autocomplete="off">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-form-label col-md-4 col-sm-4 label-align" for="first-name">Jenis Bayar <span class="required">*</span>
                      </label>
                      <div class="col-md-8 col-sm-8 ">
                          <select class="form-control" id="jenisPembayaran" name="pembayaran" autocomplete="off" required='required' autocomplete="off">
                            <option></option>
                            <option>Tunai</option>
                            <option>Transfer</option>
                          </select>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-form-label col-md-4 col-sm-4 label-align" for="first-name">Total Bayar <span class="required">*</span>
                      </label>
                      <div class="col-md-8 col-sm-8 ">
                        <input type="text" id="totalBayar" name="total_bayar" class="form-control" autocomplete="off">
                      </div>
                    </div>
                  </div>
                </form>

                <div class="modal-footer">
                  <div class="item form-group">
                      <button type="button" class="btn btn-sm btn-success" id="btn-setuju" onclick="simpan_pelunasan()" >Bayar</button>
                  </div>
                </div>
                
                <table id="tb_pelunasan" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                      <tr>
                          <th>No.</th>
                          {{-- <th>No PO</th> --}}
                          <th>Tgl</th>
                          <th>Jenis Bayar</th>
                          <th>Total</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
              <div class="clearfix"></div>
                <div class="ln_solid"></div>
              
                
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal pelunasan -->

     {{-- Modal Batal PO --}}
     <div id="modal_btl_po" class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Batal PO</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- Kiri --}}
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                </label>
                <div class="col-md-7 ">
                  <input type="text" id="" name="id_md" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Keterangan
                </label>
                <div class="col-md-7">
                  <input type="text" id="" name="ket_md" class="form-control">
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="offset-md-3">
              <button type="button" class="btn btn-success" id="btn-setuju" onclick="batal_po()" >Submit</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- Modal Batal PO --}}

    
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Surat Jalan Default</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('po.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                {{-- <label class="col-form-label col-md-3 label-align" for="first-name">Tanggal</label>
                <div class="col-md-2">
                  <input type="text" id="tglExport_m" name="tanggal_export" class="form-control" autocomplete="off">
                </div>
                <div class="col-md-2">
                  <input type="text" id="tglExport_a" name="tanggal_export" class="form-control" autocomplete="off">
                </div>
                  <button type="button" onclick="export_excel()" class="btn btn-primary">Excel</button>
                </div> --}}
                
                <div class="card-box table-responsive">
                  <table id="tbSuratJalan" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th style="width:10px;">No. </th>
                        <th>tgl</th>
                        <th>Kepada</th>
                        <th>Tagihan</th>
                        <th>Total Bayar</th>
                        <th>Sisa Tagihan</th>
                        <th>Status</th>
                        <th>Cek Batal</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
   $('#Tgl').datetimepicker({
      format: 'DD-MM-YYYY'
    });

      $('#tglExport_m').datetimepicker({
        format: 'YYYY-MM-DD'
      });

      $('#tglExport_a').datetimepicker({
        format: 'YYYY-MM-DD'
      });

      var datatableSj = $('#tbSuratJalan').DataTable({
        processing: true,
        serverSide: true,
        order: false,
        ajax: {
            type: 'GET',
            url: '{{ route('po.datatable') }}', 
        },
        columns: [
          { data: 'id', name: 'id' },
          { data: 'tgl', name: 'tgl' },
          { data: 'nama', name: 'nama' },
          { data: 'total', name: 'total' },
          { data: 'total_bayar', name: 'total_bayar' },
          { data: 'sisa_tagihan', name: 'sisa_tagihan' },
          { data: 'status', name: 'status' },
          { data: 'cek_batal', name: 'cek_batal' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    $('#modal_pelunasan').on('shown.bs.modal', function (event) {
     var $button = $(event.relatedTarget);
     var id = $button.data('id');
     
      $('input[name=id]').val(id);
      hitung_total_byr(id);
      tb_pelunasan.draw();
   });

   var tb_pelunasan = $('#tb_pelunasan').DataTable({
      processing : true,
      serverSide : true,
      ajax : {
        type : 'POST',
        url : '{{ route('po.datatable_lunas') }}',
        data : function (e) {
          e._id = $('input[name=id]').val();
        }
      },
      columns : [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            // { data: 'id_sj', name:'id_sj' },
            { data: 'tgl', name:'tgl' },
            { data: 'jenis_byr', name:'jenis_byr' },
            { data: 'total', name:'total' },
      ]
    }); 

    function hitung_total_byr(id) {
      $.ajax({
        type: 'POST',
        url: '{{ route('po.total') }}',
        data: { '_id' : id },
        success : function (e) {
          $('#grandTotal').val(e.total);
          $('#sisaTagihan').val(e.sisa_tagihan);
        }
      });
    }

    function simpan_pelunasan() {
      var id = $('#Id').val();
      var tgl = $('#Tgl').val();
      var total_byr = $('#totalBayar').val();
      var jenis_byr = $('#jenisPembayaran').val();
      
      $.ajax({
      type : 'POST',
      url : '{{ route('po.simpan_pelunasan') }}',
      data : {
                '_id' : id,
                '_tgl' : tgl,
                '_totalByr' : total_byr,
                '_jenisByr' : jenis_byr
              },
      success : function (e) {
        $('#modal_pelunasan').modal('hide');
        tb_pelunasan.draw();
        datatableSj.draw();
        hitung_total_byr();
        notif(e.response.code, e.response.msg)
        console.log(e);
      }
    });
    }

    function delete_po(id) {
      var txt;
      var r = confirm('Apakah Anda yakin ingin menghapus?')

      if(r == true) {
        $.ajax({
          type    : 'GET',
          url     : '{{ route('po.delete') }}',
          data    : { '_id' : id },
          success : function (e) {
            notif(e.response.code, e.response.msg);
            datatableSj.draw(); 
          }
        });
      } else {
        txt = 'n';
      }
    }

    $('#modal_btl_po').on('shown.bs.modal', function (event) {
      var $button = $(event.relatedTarget);
      var id = $button.data('id');
      var ket = $button.data('ket');
      $('input[name=id_md]').val(id);
      $('input[name=ket_md]').val(ket);
    });

    function batal_po(id) {
     var id = $('input[name=id_md]').val();
     var ket = $('input[name=ket_md]').val();
     $.ajax({
       type : 'POST',
       url: '{{ route('po.batal') }}',
       data : {
                '_id' :  id,
                '_ket' : ket  
      },
      success: function (e) {
        $('#modal_btl_po').modal('hide');
        datatableSj.draw();
        notif(e.code, e.msg);
      }
     });
   }
  </script>
@endpush