{{-- @extends('master.print') --}}
    
{{-- @section('content') --}}
<style>
    td .total {
        background-color : #f39c12 !important;
    }
</style>
<h3>Rekap Neraca {{ date('d-m-Y', strtotime($data['tgl_m'])) }} - {{ date('d-m-Y', strtotime($data['tgl_a'])) }}</h3>
<br>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
            Aktiva
            <table>
                <tr>
                    <th>No Akun</th>
                    <th>Keterangan</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    <th>Total</th>
                </tr>
                @foreach ($data['rekap'] as $value)
                    <tr>
                        <td>{{ $value->no_akun_aktiva }}</td>
                        <td>{{ $value->akun_aktiva }}</td>
                        <td>{{ $value->debit_aktiva }}</td>
                        <td>{{ $value->kredit_aktiva}}</td>
                        <td>{{ $value->total_aktiva}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2"><strong>Total</strong></td>
                    <td>{{ $data['tt_debit_aktiva'] }}</td>
                    <td>{{ $data['tt_kredit_aktiva'] }}</td>
                    <td class="total">{{ $data['tt_fix_aktiva'] }}</td>
                </tr>
            </table>
        </div>

        <div class="col-md-6">
            Pasiva
            <table>
                <tr>
                    <th>No Akun</th>
                    <th>Keterangan</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    <th>Total</th>
                </tr>
                @foreach ($data['rekap_pasiva'] as $value)
                    <tr>
                        <td>{{ $value->no_akun_pasiva }}</td>
                        <td>{{ $value->akun_pasiva }}</td>
                        <td>{{ $value->debit_pasiva }}</td>
                        <td>{{ $value->kredit_pasiva}}</td>
                        <td>{{ $value->total_pasiva}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2"><strong>Total</strong></td>
                    <td>{{ $data['tt_debit_pasiva'] }}</td>
                    <td>{{ $data['tt_kredit_pasiva'] }}</td>
                    <td class="total">{{ $data['tt_fix_pasiva'] }}</td>
                </tr>
            </table>
        </div>
        
    </div>
</div>

{{-- @endsection --}}