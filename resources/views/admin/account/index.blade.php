@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    {{-- @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif --}}
    {{-- notif --}}

    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Form Acoount User</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal form-label-left">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kode
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="hidden" id="" name="id_user" class="form-control" readonly>
                  <input type="text" id="" name="kode" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="nama" class="form-control " readonly>
                </div>
              </div>
              
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Username
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="username" class="form-control ">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Password
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="password" class="form-control ">
                </div>
              </div>
              {{-- <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Akses
                </label>
                <div class="col-md-6 col-sm-6 ">
                  @php
                      $akses = DB::table('akses')->get();
                  @endphp
                  <select name="akses" id="selAkses" class="form-control">
                    <option value="">-</option>
                    @foreach ($akses as $i)
                      <option value="{{ $i->id }}">{{ $i->nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div> --}}
              
                            
              <div class="ln_solid"></div>
              <div class="item form-group">
                <div class="col-md-6 col-sm-6 offset-md-3">
                  <button type="button" class="btn btn-success btn-kry" onclick="update_account()">Update</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-form-akses" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Form Akses User</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal form-label-left">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kode
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="hidden" id="" name="id_user_akses" class="form-control" readonly>
                  <input type="text" id="" name="kode_akses" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="nama_akses" class="form-control " readonly>
                </div>
              </div>
              
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Akses
                </label>
                <div class="col-md-6 col-sm-6 ">
                  @php
                      $akses = DB::table('akses')->get();
                  @endphp
                  <select name="akses_s" id="selAksesA" class="form-control">
                    <option value="">-</option>
                    @foreach ($akses as $i)
                      <option value="{{ $i->id }}">{{ $i->nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              
                            
              <div class="ln_solid"></div>
              <div class="item form-group">
                <div class="col-md-6 col-sm-6 offset-md-3">
                  <button type="button" class="btn btn-success btn-kry" onclick="update_akses()">Update</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-form-permis" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Form Permission</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kode
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="ed_permis" name="id_user_permis" class="form-control" value="" readonly>
                  <input type="text" id="" name="kode_permis" class="form-control" value="" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="nama_permis" class="form-control " readonly>
                </div>
              </div>

              <div class="item form-group">
                <div class="">
                  <ul class="to_do">
                    @foreach ($permis as $i)
                      <li>
                        <p>
                          <input type="checkbox" id="{{$i}}" class="flat ck" autocomplete="off"> {{ $i }} </p>
                      </li>    
                    @endforeach
                  </ul>
                </div>
              </div>
           
              <div class="ln_solid"></div>
              <div class="item form-group">
                <div class="col-md-6 col-sm-6 offset-md-3">
                  {{-- <button type="button" class="btn btn-primary" onclick="tessi()">tes</button> --}}
                  <button type="button" class="btn btn-success btn-kry" onclick="save_permis()">Save</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Account User</h2>

            <ul class="nav navbar-right panel_toolbox">
              <a href=" {{ route('account.form') }} " class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbAccount" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>KD karyawan</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Akses</th>
                        <th>Permission</th>
                        <th>Tgl Buat</th>
                        <th>Tgl Ubah</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('uyeee');
    var datatableAccount = $('#tbAccount').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('account.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'kode', name: 'kode' },
          { data: 'nama', name: 'nama' },
          { data: 'username', name: 'username' },
          { data: 'email', name: 'email' },
          { data: 'role', name: 'role' },
          { data: 'akses', name: 'akses' },
          { data: 'permis', name: 'permis' },
          { data: 'created_at', name: 'created_at' },
          { data: 'updated_at', name: 'updated_at' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    $('#modal-form').on('shown.bs.modal', function (event) {
      var $button = $(event.relatedTarget);
      var id = $button.data('id');
      var kd_karyawan = $button.data('kd_karyawan');
      var nama = $button.data('nama');
      var username = $button.data('username');
      
      $('input[name=id_user]').val(id);
      $('input[name=kode]').val(kd_karyawan);
      $('input[name=nama]').val(nama);
      $('input[name=username]').val(username);
      $('input[name=password]').val('');
    });

    $('#modal-form-akses').on('shown.bs.modal', function (event) {
      var $button = $(event.relatedTarget);
      var id = $button.data('id');
      var kd_karyawan = $button.data('kd_karyawan');
      var nama = $button.data('nama');
      var id_akses = $button.data('id_akses');
      
      $('input[name=id_user_akses]').val(id);
      $('input[name=kode_akses]').val(kd_karyawan);
      $('input[name=nama_akses]').val(nama);
      $('#selAksesA').val(id_akses);
    });

    $('#modal-form-permis').on('shown.bs.modal', function (event) {
      var $button = $(event.relatedTarget);
      var ids = $button.data('id_pr');
      var kd_karyawan = $button.data('kd_karyawan');
      var nama = $button.data('nama');

      // $('.flat').iCheck('uncheck');

      $('input[name=id_user_permis]').val(ids);
      $('input[name=kode_permis]').val(kd_karyawan);
      $('input[name=nama_permis]').val(nama);
      get_permis();
    });

    function update_account() {
      var id = $('input[name=id_user]').val();
      var username = $('input[name=username]').val();
      var password = $('input[name=password]').val();
      // var akses = $('#selAkses').val();

      if (!password) {
        notif(300, 'Password tidak boleh kosong !');
      }else if(!username) {
        notif(300, 'Username tidak boleh kosong !');
      }else{
        $.ajax({
          type    : 'POST',
          url     : '{{ route('account.update') }}',
          data    : {
                      '_id' : id,
                      '_username' : username,
                      '_password' : password
                      // '_akses' : akses
                    },
          success : function (e) {
            $('#modal-form').modal('hide');
            notif(e.response.code, e.response.msg);
            datatableAccount.draw();
          }
        });
      }
    }

    function update_akses() {
      var id_user = $('input[name=id_user_akses]').val();
      var akses = $('#selAksesA').val();

      $.ajax({
        type    : 'POST',
        url     : '{{ route('account_akses.update') }}',
        data    : { 
                  '_idUser' : id_user,
                  '_akses' : akses
                },
        success : function (e) {
          $('#modal-form-akses').modal('hide');
          notif(e.response.code, e.response.msg);
          datatableAccount.draw();
        }
      });
    }

    function save_permis() {
      var id = $('input[name=id_user_permis]').val();
      var permis = [];
      var pilih = {};
      var pilihno = {};

      $.each($("input[type='checkbox']:checked"), function () {
        var $check = $(this);
        var id_permis = $check.attr('id');
        
        pilih = {
          '_permis' : id_permis,
          '_v' : 1 
        };

        permis.push(pilih);
      });

      $.each($("input:checkbox:not(:checked)"), function () {
        var $nocheck = $(this);
        var id_permis = $nocheck.attr('id');
        
        pilihno = {
          '_permis' : id_permis,
          '_v' : null
        };

        permis.push(pilihno);
      });

      $.ajax({
        type    : 'POST',
        url     : '{{ route('account_permis.update') }}',
        contentType  : "application/json",
        dataType  : 'json',
        data    : JSON.stringify({ '_id' : id ,permis }),
        success : function (e) {
          $('#modal-form-permis').modal('hide');
          notif(e.response.code, e.response.msg);
        }
      });
    }
    
    function get_permis() {
      var idp = $('input[name=id_user_permis]').val();
      $.ajax({
        type    : 'POST',
        url     : '{{ route('account_permis.get') }}',
        data    : { '_id' : idp },
        success : function (e) { 
          $.each(e.permis, function (key, val) {
            if (val.val == 1) {
              $('#'+val.name).iCheck('check');              
            }else{
              $('#'+val.name).iCheck('uncheck');              
            }
          });
        } 
      });
    }

    function delete_account(id) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('account.delete') }}',
        data    : { '_id' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableAccount.draw();          
        }
      });
    }

  </script>
@endpush