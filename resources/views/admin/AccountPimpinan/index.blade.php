@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Form</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal form-label-left">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kode
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="id_user" class="form-control" readonly>
                  <input type="text" id="" name="kode" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="nama" class="form-control " readonly>
                </div>
              </div>
              
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Username
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="username" class="form-control ">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Password
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="password" class="form-control ">
                </div>
              </div>
                            
              <div class="ln_solid"></div>
              <div class="item form-group">
                <div class="col-md-6 col-sm-6 offset-md-3">
                  <button type="button" class="btn btn-success btn-kry" onclick="update_account()">Update</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Account Pimpinan</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href=" {{ route('account.form') }} " class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbAccount" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>No. Telp</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('uyeee');
    var datatableAccount = $('#tbAccount').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('account_pimpinan.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama', name: 'nama' },
          { data: 'alamat', name: 'alamat' },
          { data: 'no_telp', name: 'no_telp' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    $('#modal-form').on('shown.bs.modal', function (event) {
      var $button = $(event.relatedTarget);
      var id = $button.data('id');
      var kd_karyawan = $button.data('kd_karyawan');
      var nama = $button.data('nama');
      var username = $button.data('username');
      
      $('input[name=id_user]').val(id);
      $('input[name=kode]').val(kd_karyawan);
      $('input[name=nama]').val(nama);
      $('input[name=username]').val(username);
      $('input[name=password]').val('');
    });

    function update_account() {
      var id = $('input[name=id_user]').val();
      var username = $('input[name=username]').val();
      var password = $('input[name=password]').val();

      if (!password) {
        notif(300, 'Password tidak boleh kosong !');
      }else if(!username) {
        notif(300, 'Username tidak boleh kosong !');
      }else{
        $.ajax({
          type    : 'POST',
          url     : '{{ route('account.update') }}',
          data    : {
                      '_id' : id,
                      '_username' : username,
                      '_password' : password
                    },
          success : function (e) {
            $('#modal-form').modal('hide');
            notif(e.response.code, e.response.msg);
            datatableAccount.draw();
          }
        });
      }
    }

    function delete_account(id) {
      console.log(id);
      
      $.ajax({
        type    : 'POST',
        url     : '{{ route('account.delete') }}',
        data    : { '_id' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableAccount.draw();          
        }
      });
    }

  </script>
@endpush