@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    <div class="modal fade" id="modal-list-karyawan" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
  
            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel">Data Karyawan</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="card-box table-responsive">
                    <table id="tbKaryawan" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>KD karyawan</th>
                            <th>Nama</th>
                            <th>Jabatan</th>
                            <th>Opsi</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form Biodata</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <br/>
            <form id="" action="{{ route('account_pimpinan.save') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                {{ csrf_field() }}
                
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama
                    </label>
                    <div class="col-md-7">
                      <input type="hidden" id="" name="kode" class="form-control" value="{{ $kode }}" readonly>
                      <input type="text" id="" name="nama" class="form-control" value="{{ $nama }}">
                    </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Alamat
                  </label>
                  <div class="col-md-7">
                    <textarea name="" id="" cols="30" rows="10" class="form-control">{{ $alamat }}</textarea>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Telp
                  </label>
                  <div class="col-md-7">
                    <input type="text" id="" name="nama" class="form-control" value="{{ $no_telp }}">
                  </div>
                </div>
                
                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-7 col-sm-7 offset-md-3">
                    <a href="{{ route('account_pimpinan.index') }}" class="btn btn-warning" type="button">Cancel</a>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>
            </form>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="x_panel">
            <div class="x_title">
              <h2>Account</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <br/>
            <form id="" action="{{ route('account_pimpinan.create') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                {{ csrf_field() }}
                
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Username 
                    </label>
                    <div class="col-md-7">
                      <input type="hidden" id="" name="kode_q" class="form-control" value="{{ $kode }}" readonly>
                      <input type="text" id="" name="username" required class="form-control ">
                    </div>
                </div>
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Password 
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="" name="password" required class="form-control ">
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-7 col-sm-7 offset-md-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('form');
    var list_karyawan = $('#tbKaryawan').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('karyawan.list') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'kode', name: 'kode' },
          { data: 'nama', name: 'nama' },
          { data: 'jabatan', name: 'jabatan' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function select_karyawan(kode, nama) {
        $('input[name=kode]').val(kode);
        $('input[name=nama]').val(nama);
        $('#modal-list-karyawan').modal('hide');
    }

  </script>
@endpush