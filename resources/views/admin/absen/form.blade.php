@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <!-- Awal Modal Akun -->
    <div class="modal fade bs-example-modal-lg" id="modal_akun" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Akun</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tb_akun" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>No. Akun</th>
                              <th>Nama</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Akun -->

    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form Absen</h2>
              <ul class="nav navbar-right panel_toolbox">
                <a href=" {{ route('absen.form') }} " class="btn btn-sm btn-success">New</a>
                <a href=" {{  route('absen.index') }} " class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back </a>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-12 col-sm-12">
                  <div class="item form-group"> 
                    <label class="col-form-label col-md-3 col-sm-3" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control" value="{{ !empty($tgl) ? $tgl : '' }}">
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="tabel_absen" style="width:100%">
                      <thead>
                        <tr>
                          <th>Nama</th>
                          <th>Kodep</th>
                          <th>Jam Masuk</th>
                          <th>Jam Pulang</th>
                          <th>Izin</th>
                          <th>Potongan</th>
                          <th>Keterangan</th>
                          <th>Aksi</th>
                        </tr>
                        <tr>
                          <td><input type="text" id="Nama" name="nama" required='required' class="form-control" readonly></td>
                          <td><input type="text" id="Kodep" name="kodep" required="required" class="form-control kodepQ"></td>
                          <td><input type="time" id="jamHadir" name="jam_hadir" required="required" class="form-control"></td>
                          <td><input type="time" id="jamPulang" name="jam_pulang" required="required" class="form-control"></td>
                          <td><input type="text" id="Izin" name="izin" required="required" class="form-control"></td>
                          <td><input type="text" id="Potongan" name="potongan" required="required" class="form-control"></td>
                          <td><input type="text" id="Ketr" name="ketr" required="required" class="form-control"></td>
                          <td><button type="button" class="btn btn-sm btn-primary" onclick="add_absen()"><i class="fa fa-plus"></i></button></td>
                        </tr>
                      </thead>
                      <tbody id="bd_absen">
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>

                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      {{-- <button type="button" class="btn btn-primary">Cancel</button> --}}
                      {{-- <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="save_jurnal()">Submit</button> --}}
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    get_absen();

    $(document).on('keyup', '.kodepQ', function(){
      var kodep = $(this).val();

      $.ajax({
          type    : 'POST',
          url     : '{{ route('get_nama') }}',
          data    : {
                      '_kodep' : kodep,
                  },
          success : function (e) {
            $('#Nama').val(e.nama);
            console.log(e);
          }    
      });
    });

    function add_absen() {
      var tgl = $('#Tgl').val();
      var kodep = $('#Kodep').val();
      var jam_hadir = $('#jamHadir').val();
      var jam_pulang = $('#jamPulang').val();
      var izin = $('#Izin').val();
      var potongan = $('#Potongan').val();
      var ketr = $('#Ketr').val();

      $.ajax({
        type : 'POST',
        url : '{{ route('absen.add_absen') }}',
        data : {
              '_tgl'                : tgl,
              '_kodep'              : kodep,
              '_jamHadir'           : jam_hadir,
              '_jamPulang'          : jam_pulang,
              '_izin'               : izin,
              '_potongan'           : potongan,
              '_ketr'               : ketr
              },
        success: function (e) {
          console.log(e);
          tampil_absen(tgl);
          clear_absen();
          // notif(e.code, e.msg);
          $('#Kodep').focus();
        }
      });
    }

    function clear_absen() {
      $('#Nama').val('');
      $('#Kodep').val('');
      $('#jamHadir').val('');
      $('#jamPulang').val('');
    }

    function tampil_absen(tgl) {
      var tb = '';
      $.ajax({
        type : 'POST',
        url : '{{ route('absen.tampil_absen') }}',
        data : { '_tgl' : tgl },
        success: function (e) {
          // console.log(e);  
          $('#bd_absen').empty();
          $.each(e, function (key, value) {
            tb += '<tr>';
            tb += '<td><input type="text" class="form-control" value='+value.nama+' readonly></td>';
            tb += '<td><input type="text" class="form-control" value='+value.kodep+' readonly></td>';
            tb += '<td><input type="text" class="form-control" value='+value.jamhadir+' readonly></td>';
            tb += '<td><input type="text" class="form-control" value='+value.jampulang+' readonly></td>';
            tb += '<td><input type="text" class="form-control" value='+value.izin+' readonly></td>';
            tb += '<td><input type="text" class="form-control" value='+value.potongan+' readonly></td>';
            tb += '<td><input type="text" class="form-control" value='+value.ketr+' readonly></td>';
            tb += '<td><button type="text" class="btn btn-sm btn-danger" onclick=delete_item_absen('+value.id+')><i class="fa fa-remove"></i></td>';
            tb += '</tr>';
            // console.log(value);
          });
          $('#tabel_absen').append(tb);
        }
      });
    }

    function get_absen() {
      var tgl = $('#Tgl').val();
      var tb = ''

      $.ajax({
        type : 'POST',
        url : '{{ route('get_absen.json') }}',
        data : { '_tgl' : tgl },
        success : function (e) {
          console.log(e);
          $('#bd_absen').empty();
          $.each(e, function (key, value) {
            tb += '<tr>';
            tb += '<td><input type="text" class="form-control" value='+value.nama+' readonly></td>';
            tb += '<td><input type="text" class="form-control" value='+value.kodep+' readonly></td>';
            tb += '<td><input type="text" class="form-control" value='+value.jamhadir+' readonly></td>';
            tb += '<td><input type="text" class="form-control" value='+value.jampulang+' readonly></td>';   
            tb += '<td><input type="text" class="form-control" value='+value.izin+' readonly></td>';         
            tb += '<td><input type="text" class="form-control" value='+value.potongan+' readonly></td>';
            tb += '<td><input type="text" class="form-control" value='+value.ketr+' readonly></td>';
            tb += '<td><button type="text" class="btn btn-sm btn-danger" onclick=delete_item_absen('+value.id+')><i class="fa fa-remove"></i></td>';
            tb += '</tr>';
            // console.log(value);
          });
          $('#tabel_absen').append(tb);
        }
      });
    }

    function delete_item_absen(id) {
      $.ajax({
        type : 'POST',
        url : '{{ route('absen.delete_item_absen') }}',
        data : { '_id' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          get_absen();
        }
      });
    }

  </script>
@endpush