@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Absensi</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('absen.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                  <table id="tb_absen" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tgl</th>
                        <th>Total Claim</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">  
    var tb_absen = '';

    tb_absen = $('#tb_absen').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
          type : 'GET',
          url : '{{ route('absen.datatable') }}',

      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'tgl', name: 'tgl' },
        { data: 'total_claim', name:'total_claim' },
        { data: 'opsi', name: 'opsi' }
      ]
    });

    function delete_absen(tgl) {
      var txt;
      var r =  confirm('Apakah Anda yakin ingin menghapus?');

      if (r == true) {
        $.ajax({
            type : 'POST',
            url : '{{ route('absen.delete') }}',
            data : { '_tgl' : tgl },
            success : function (e) {
              notif(e.response.code, e.response.msg);
              tb_absen.draw();
            }
        });
      } else {
        txt = 'n';
      } 
    }

  </script>
@endpush