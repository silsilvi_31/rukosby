{{-- @extends('master.print')
    
@section('content') --}}
<table>
        <tr>
            <th>Tanggal</th>
            <th>No BK</th>
            <th>Nama</th>
            <th>Keterangan</th>
            <th>Banyak</th>
            <th>Harga</th>
            <th>Jenis BK</th>
            <th>Total</th>
        </tr>
        @foreach ($data as $value)
            <tr>
                 @if ($value->jenis_bk == 'U')
                <td>{{ date('d-m-y', strtotime($value->tanggal)) }}</td>
                <td>{{ $value->no_bk }}</td>
                <td>{{ $value->kepada }}</td>
                <td>{{ $value->keterangan }}</td>
                <td>{{ $value->qty }}</td>
                <td>{{ $value->harga }} </td>
                <td>{{ $value->jenis_bk }} </td>
                <td>{{ $value->total }} </td>
                @else
                <td>{{ date('d-m-y', strtotime($value->tanggal)) }}</td>
                <td>{{ $value->no_bk }}</td>
                <td>{{ $value->kepada }}</td>
                <td>{{ $value->keterangan }} </td>
                <td>{{ $value->qty }}</td>
                <td></td>
                <td>{{ $value->jenis_bk }}</td>
                <td></td>
                @endif
            </tr>
        @endforeach
    </table>
{{-- @endsection --}}