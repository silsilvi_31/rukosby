@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Rekap Barang Keluar</h2>
            {{-- <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('sjdefault.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul> --}}
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal</label>
                <div class="col-md-2 col-sm-2 ">
                  <input type="text" id="tglExport" name="tanggal_export" class="form-control" autocomplete="off">
                </div>
                <div class="col-md-2">
                  <input type="text" id="tglExport_a" name="tanggal_export" class="form-control" autocomplete="off">
                </div>
                  <button type="button" onclick="export_excel()" class="btn btn-primary">Excel</button>
                  {{-- <button type="button" onclick="delete_tgl()" class="btn btn-danger">Hapus</button> --}}
                </div>
                <div class="col-md-2 col-sm-2 ">
                  {{-- <a href="{{ route('jurnal.excel') }}" class="btn btn-primary" >Export</a> --}}
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
      $('#tglExport').datetimepicker({
        format: 'YYYY-MM-DD'
      });

      $('#tglExport_a').datetimepicker({
        format: 'YYYY-MM-DD'
      });

    // console.log('uyeee');

    function export_excel() {
     var tgl = $('#tglExport').val();
     var tgl_a = $('#tglExport_a').val();

     window.location.href = '/dashboard/rekap/excel_bk/'+tgl+'&'+tgl_a; 
    }

    function delete_tgl(no_bk) {
      var tgl = $('#tglExport').val();
      var tgl_a = $('#tglExport_a').val();
      $.ajax({

        type    : 'POST',
        url     : '{{ route('rekap.delete_tgl') }}',
        data    : { '_tgl' : tgl,
                    '_tgla' : tgl_a,
                    '_noBk' : no_bk,
                  },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          // datatableSj.draw(); 
        }
      })
    }

  </script>
@endpush