@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Jenis Barang</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href=" {{ route('jenisbarang.form') }} " class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbJenisBarang" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Jenis Barang</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('uyeee');
    var datatableJenisBarang = $('#tbJenisBarang').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('jenisbarang.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'jenis_brg', name: 'jenis_brg' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_jenis_barang(id) {
      $.ajax({
        type    : 'POST',
        url     : '{{route ('jenisbarang.delete')}}',
        data    : {'_idJenisbarang' : id},
        success : function (e) {
          console.log(e);
          
          notif(e.response.code, e.response.msg);
          datatableJenisBarang.draw();
        }
      });
           
    }

  </script>
@endpush