@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Sample</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form id="" action="{{ route('satuan.save') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                {{ csrf_field() }}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Satuan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="satuan" required="required" class="form-control ">
                  </div>
                </div>
                
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align">Auto
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="tesAuto" name="tes" class="form-control">
                  </div>
                </div>

                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-6 col-sm-6 offset-md-3">
                    <button type="button" class="btn btn-danger" onclick="call()">Call</button>
                    <button class="btn btn-danger" type="reset">Reset</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    var dataQ = [
      { value: 'And', data: 'And' },
      { value: 'A', data: 'A' },
      { value: 'ZZ', data: 'ZZ' },
      { value: 'AD', data: 'AD' }
    ];

    var dataQQ = [];
    // call();

    $("#tesAuto").autocomplete({
      serviceUrl: function(request, response) {
        // console.log(response);
        
        $.ajax({
            type : 'GET',
            url: "{{ route('sopir.json') }}",
            data: {
                    term : request
             },
            success: function(data){
              console.log(data);
              
              //  var resp = $.map(data,function(obj){
              //       console.log(obj);
              //       // return obj.name;
              //  }); 

              //  response(resp);
            }
        });
      }
    });

    function call() {
      $.ajax({
        type    : 'GET',
        url     : '{{ route('sopir.json') }}',
        success : function (e) {
          console.log(dataQ);
          console.log(e);

          dataQQ = $.map(e, function (obj) {
            console.log('=========');
            console.log(obj.data);
            return obj.data;
          });          
          
        }
      });
    }
    // var datatablePresensi = $('#tbJabatan').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     ajax: {
    //         type: 'GET',
    //         url: '{{ route('jabatan.datatable') }}', 
    //     },
    //     columns: [
    //       { data: 'DT_RowIndex', name: 'DT_RowIndex' },
    //       { data: 'jabatan', name: 'jabatan' },
    //       { data: 'opsi', name: 'opsi' }
    //     ]
    // });

  </script>
@endpush