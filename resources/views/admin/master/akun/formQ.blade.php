@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
      
    {{-- Modal --}}
      <div class="modal fade bs-example-modal-lg" id="modal-akun" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">

            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel2">Daftar Akun</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              {{-- <div class="row"> --}}
                <div class="col-md-12">
                  <div class="card-box table-responsive">
                    <table id="tbAkun" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>No. Akun</th>
                          <th>Nama</th>
                          <th>Opsi</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              {{-- </div> --}}
            </div>
          </div>
        </div>
      </div>
      {{-- /Modal --}}
    
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form Akun</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form id="" action="{{ route('akun.saveQ') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                {{ csrf_field() }}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Akun <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="noAkun" name="no_akun" required="required" class="form-control " maxlength="7">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Akun
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="namaAkun" name="nama_akun" class="form-control">                    
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kelompok Akun
                  </label>
                  <div class="col-md-3 col-sm-3 ">
                    <input type="text" id="parentId" name="parent_id" class="form-control" readonly>                    
                    <input type="text" id="Kelompok" name="kelompok" class="form-control" readonly>
                  </div>
                  <div class="col-md-3 col-sm-3 ">
                    <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal-akun"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                
                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-6 col-sm-6 offset-md-3">
                    <a href="{{ route('akun.index') }}" class="btn btn-warning" type="button">Cancel</a>
                    <button class="btn btn-danger" type="reset">Reset</button>
                    <button type="button" class="btn btn-success" onclick="save_akun()">Submit</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    console.log('form');
    var datatableAkun = $('#tbAkun').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('akun.listQ') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'akun', name: 'akun' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function save_akun() {
      var no_akun = $('#noAkun').val();
      var nama_akun = $('#namaAkun').val();

      $.ajax({
        type : 'POST',
        url : '{{ route('akun.saveQ')}}',
        data : {
                  '_noAkun' : no_akun,
                  '_namaAkun' : nama_akun
        },
        success: function (e) {
          notif(e.code, e.msg);
        }
      });
    }

    function select_akun(no_akun, nama_akun) {
      $('#modal-akun').modal('hide');
      $('#parentId').val(no_akun);
      $('#Kelompok').val(nama_akun);
    }

  //   $(window).load(function(){
  //   $("#noAkun").inputmask("9999-99");
  // });

  </script>
@endpush