@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    {{-- @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif --}}
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Barang</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href=" {{ route('hargakayubakar.form') }} " class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbBarang" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No. </th>
                        <th>Nama Pelanggan</th>
                        <th>Nama Barang</th>
                        <th>Harga</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('uyeee');
    var datatableBarang = $('#tbBarang').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('hargakayubakar.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'kepada', name: 'kepada' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'harga', name: 'harga' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_barang(kode) {
      // console.log(kode);
      $.ajax({
        type    : 'POST',
        url     : '{{ route('hargakayubakar.delete') }}',
        data    : {'_id' : kode},
        success : function (e) {
          console.log(e);
          notif(e.response.code, e.response.msg);
          datatableBarang.draw();
        }
      });
    }

    // function delete_jenis_barang(id) {
    //   $.ajax({
    //     type    : 'POST',
    //     url     : '{{route ('jenisbarang.delete')}}',
    //     data    : {'_idJenisbarang' : id},
    //     success : function (e) {
    //       console.log(e);
          
    //       notif(e.response.code, e.response.msg);
    //       datatableJenisBarang.draw();
    //     }
    //   });
           
    //  }

  </script>
@endpush