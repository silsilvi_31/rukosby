@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    {{-- Modal Cek Jurnal Umum --}}
    <div id="modal_cekju" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Cek Jurnal Umum</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
              {{-- Kiri --}}
              <div class="col-md-6 col-sm-6">
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Jurnal <span class="required">*</span>
                  </label>
                  <div class="col-md-7 ">
                    <input type="text" id="noJu" name="no_ju" class="form-control" readonly>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                  </label>
                  <div class="col-md-7">
                    <input type="text" id="Tgl" name="tgl" class="form-control" readonly>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama
                  </label>
                  <div class="col-md-7">
                    <input type="text" id="Nama" name="nama" class="form-control" readonly>
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-sm-6">
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Total
                  </label>
                  <div class="col-md-7">
                    <input type="text" id="Total" name="total" class="form-control" readonly>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Status
                  </label>
                  <div class="col-md-7">
                    <input type="text" id="Status" name="status" class="form-control" readonly>
                  </div>
                </div>
              </div>
            </form>
            <div class="clearfix"></div>
            <div class="ln_solid"></div>

            <div class="card-box table-responsive">
              <table id="tb_detail_cekju" class="table table-sm table-striped table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>No Akun</th>
                    <th>Keterangan</th>
                    <th>Map</th>
                    <th>Hit</th>
                    <th>Harga</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btn-setuju" onclick="acc_ju()" >Setuju</button>
            <button type="button" class="btn btn-danger " id="btn-batal" onclick="batal_ju()" >Batal</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    {{-- akhir modal cek Jurnal Umum --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Cek Jurnal Umum</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tb_cekju" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>tgl</th>
                        <th>Kepada</th>
                        <th>Total</th>
                        <th>Cek Jurnal</th>
                        {{-- <th>Cek Batal</th> --}}
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    var datatableCekJu = $('#tb_cekju').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        ajax: {
            type: 'GET',
            url: '{{ route('cekjurnalumum.datatable') }}', 
        },
        columns: [
          
          { data: 'id_ju', name: 'id_ju' },
          { data: 'tgl', name: 'tgl' },
          { data: 'nama', name: 'nama' },
          { data: 'total', name: 'total' },
          { data: 'is_cek_jurnal', name: 'is_cek_jurnal' },
          // { data: 'batal', name: 'batal' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    var tb_detail_cekju = $('#tb_detail_cekju').DataTable({
      processing : true,
      serverSide : true,
      scrollY: true,
      ajax : {
        type : 'POST',
        url : '{{ route('cekjurnalumum.datatable_ju') }}',
        data : function (e) {
          e._noJu = $('input[name=no_ju]').val();
        }
      },
      columns : [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'no_akun', name: 'no_akun' },
          { data: 'keterangan', name: 'keterangan' },
          { data: 'map', name: 'map' },
          { data: 'hit', name: 'hit' },
          { data: 'harga', name: 'harga' },
          { data: 'total', name: 'total' },
      ]
    }); 

    // function delete_suratjalan(id) {
    //   $.ajax({
    //     type    : 'GET',
    //     url     : '{{ route('ceksj.delete') }}',
    //     data    : { '_idSj' : id },
    //     success : function (e) {
    //       notif(e.response.code, e.response.msg);
    //       datatableCekbeli.draw(); 
          
    //     }
    //   });
    // }

    // $('#modal_btl').on('shown.bs.modal', function (event) {
    //   var $button = $(event.relatedTarget);
    //   var id = $button.data('id');
    //   var ket = $button.data('ket');
    //   $('input[name=id_md]').val(id);
    //   $('input[name=ket_md]').val(ket);
    // });

   $('#modal_cekju').on('shown.bs.modal', function (event) {
     var $button = $(event.relatedTarget);
     var form = $button.data('form');
     var no_ju = $button.data('id');
     var tgl = $button.data('tgl');
     var nama = $button.data('nama');
     var total = $button.data('total');
     var cek_jurnal = $button.data('status');
     
      $('input[name=no_ju]').val(no_ju);
      $('input[name=tgl]').val(tgl);
      $('input[name=nama]').val(nama);
      $('input[name=total]').val(total);
      $('input[name=status]').val(cek_jurnal);

      if(cek_jurnal == 'acc') {
        $('#btn-setuju').attr('hidden', true);
        $('#btn-batal').attr('hidden', true);
      } else {
        $('#btn-setuju').removeAttr('hidden');
        $('#btn-batal').removeAttr('hidden',);
      }

      tb_detail_cekju.draw();
   });

   function acc_ju() {
    var no_ju = $('input[name=no_ju]').val();
    var status = $('input[name=status]').val();
    var btn = 'setuju';
    console.log(btn);
    
    $.ajax({
      type : 'POST',
      url : '{{ route('cekjurnalumum.acc_ju') }}',
      data : {
                '_noJu' : no_ju,
                '_btn' : btn,
                '_status' : status
              },
      success : function (e) {
        datatableCekJu.draw();
        $('#modal_cekju').modal('hide');
        notif(e.code, e.msg);
      }
    });
   }

   function batal_ju() {
    var id = $('input[name=no_ju]').val();
    var batal = 'batal';
    console.log(batal);
    
    $.ajax({
      type : 'POST',
      url : '{{ route('cekjurnalumum.acc_ju') }}',
      data : {
                '_noJu' : id,
                '_btn' : batal
              },
      success : function (e) {
        datatableCekJu.draw();
        $('#modal_cekju').modal('hide');
        notif(e.code, e.msg);
      }
    });
   }

   function batal_suratjalan(id) {
     var id = $('input[name=id_md]').val();
     var ket = $('input[name=ket_md]').val();
     $.ajax({
       type : 'POST',
       url: '{{ route('ceksj.batal_suratjalan') }}',
       data : {
                '_id' :  id,
                '_ket' : ket  
      },
      success: function (e) {
        $('#modal_btl').modal('hide');
        datatableCekSj.draw();
        notif(e.code, e.msg);
      }
     });
   }

   $(document).on('click', '.print_nota', function() {
      var id_sj = $(this).attr('id');
      var jenis = 'print_nota';
      $.ajax({
        type    : 'POST',
        url     : '{{ route('ceksj.print_sj_count') }}',
        data    : { '_idSj' : id_sj,
                    '_jenis' : jenis },
        success : function (e) {
          notif(e.code, e.msg);
          datatableSj.ajax.reload(); 
            window.location.href = 'cek-sj/nota/'+id_sj;
        }
      });
    });
  </script>
@endpush