@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Jurnal Umum</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('jurnalUmum.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Stok</a>
              <a href="{{ route('jurnalUmumDua.form') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Lain</a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                  <table id="tb_jurnal" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No Jurnal</th>
                        <th>Tgl</th>
                        <th>Nama</th>
                        <th>Total</th>
                        <th>Cek Jurnal</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">  
    var tb_jurnalUmum = '';

    tb_jurnalUmum = $('#tb_jurnal').DataTable({
      processing    : true,
      serverSide    : true,
      ordering      : false,
      ajax  : {
          type : 'GET',
          url : '{{ route('jurnalUmum.datatable') }}',

      },
      columns: [
        { data: 'id_ju', name: 'id_ju' },
        { data: 'tgl', name: 'tgl' },
        { data: 'nama', name: 'nama' },
        { data: 'total', name: 'total' },
        { data: 'is_cek_jurnal', name:'is_cek_jurnal' },
        { data: 'opsi', name: 'opsi' }
      ]
    });

    function delete_jurnalUmum(id) {
      $.ajax({
        type        : 'POST',
        url         : '{{ route('jurnalUmum.delete') }}',
        data        : { '_id' : id },
        success     : function (e) {
          notif(e.response.code, e.response.msg);
          tb_jurnalUmum.draw();
        }
      });
    }

  </script>
@endpush