@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Tutup Buku</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Periode Finansial</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('bukaBuku.index') }}" class="btn btn-sm btn-success">Buka Buku</a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box">
                  <br />
                 <center><h2>Anda akan melakukan proses <strong> tutup buku </strong> untuk periode finansial:</h2></center> 
                  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                    <br>
                    <div class="item form-group">
                      <label class="col-form-label col-md-5 col-sm-5 label-align" for="first-name">Dari Tanggal <span class="required">*</span>
                      </label>
                      <div class="col-md-3 col-sm-3 ">
                        <input type="text" id="dariTgl" name="dari_tgl" required="required" class="form-control " autocomplete="off" value="{{ $tgl_awalan }}">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-form-label col-md-5 col-sm-5 label-align" for="last-name">Sampai Tanggal <span class="required">*</span>
                      </label>
                      <div class="col-md-3 col-sm-3 ">
                        <input type="text" id="sampaiTgl" name="sampai_tgl" required="required" class="form-control" autocomplete="off">
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-3"></div>
                    <div class="col-md-6">
                      <div class="alert alert-danger alert-dismissible " role="alert">
                        <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                        </button>
                        Setelah proses tutup buku, Anda tidak dapat melakukan perubahan terhadap buku Anda pada tanggal SEBELUM <strong><span id="tglQ">06/01/2021</span></strong>
                      </div>
                    </div>

                    <div class="col-md-5 col-sm-5 offset-md-5">
                      <button class="btn btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-success" onclick="list_tutup_buku()">Submit</button>
                    </div>
                    </div>

                  </form>

                  <div class="card-box table-responsive">
                    <table id="tb_parent_jurnal" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Tgl Awal</th>
                          <th>Tgl Akhir</th>
                          <th>Labarugi</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
     $('#dariTgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });
   
    $('#sampaiTgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $("#sampaiTgl").on("dp.change", function (e) {
        var tgl = $(this).val();
        $('#tglQ').text(tgl);
    });

    

    // function list_tutup_buku() {
    //   var dari_tgl = $('#dariTgl').val();
    //   var sampai_tgl = $('#sampaiTgl').val();

    //   var tgl_gabung = tgl_m+'&'+tgl_a;

    //   var url = '{{ route('sjdefault.excel_sj', ['.p']) }}';
    //   var url_fix = url.replaceAll('.p',tgl_gabung);
    //   // console.log(url_fix);
    //   window.location.href = url_fix;
    // }

    function list_tutup_buku() {
      var dari_tgl = $('#dariTgl').val();
      var sampai_tgl = $('#sampaiTgl').val();

      var txt;
      var r =  confirm('Apakah Anda yakin Melakuan tutup buku pada '+dari_tgl+' sampai '+sampai_tgl+'?');

      if (r == true) {
        $.ajax({
        type  : 'POST',
        url : '{{ route('tutupBuku.simpan_tutupbuku') }}',
        data : {
              '_tgl' : dari_tgl,
              '_tglDua' : sampai_tgl,
        },
        success: function (e) {
          console.log(e);
          notif(e.response.code, e.response.msg);
          datatable_parent_jurnal.draw();
        }
      });
      } else {
        txt = 'n';
      }

      
    }

    var datatable_parent_jurnal = '';

    var datatable_parent_jurnal = $('#tb_parent_jurnal').DataTable({
      processing: true,
      serverSide: true,
      order : false,
      ajax : {
        type : 'POST',
        url : '{{ route('tutupBuku.datatable_tutupbuku') }}',
        data : function (e) {
          e.tgl = $('#tgl').val();
          e.jenisJurnal = $('#jenisJurnal').val();
        }
      },
      columns: [
          { data: 'id', name: 'id' },
          { data: 'tgl_awal', name: 'tgl_awal' },
          { data: 'tgl_akhir', name: 'tgl_akhir' },
          { data: 'labarugi', name: 'labarugi' },
          { data: 'status', name: 'status' },
        ],
    });

    



  </script>
@endpush