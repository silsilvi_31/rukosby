@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Detail Gaji</h2>
            <ul class="nav navbar-right panel_toolbox">
              <button class="btn btn-sm btn-success" onclick="excel_gaji()"><i class="fa fa-download"></i></button>
              <button class="btn btn-sm btn-primary" onclick="buat_jurnal()">Buat Jurnal</button>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <input type="text" id="tglAwal" name="tgl_awal" value="{{ $tgl_awal }}" readonly>
                {{-- <input type="text" id="tglAkhir" name="tgl_akhir" value="" readonly> --}}
                  <table id="tb_gajiMingguan" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>kodep</th>
                        <th>nama</th>
                        <th>Jumat</th>
                        <th>Sabtu</th>
                        <th>Minggu</th>
                        <th>Senin</th>
                        <th>Selasa</th>
                        <th>Rabu</th>
                        <th>Kamis</th>
                        <th>tt</th>
                        <th>Lembur</th>
                        <th>Pot</th>
                        <th>Total</th>
                        {{-- <th>Opsi</th> --}}
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">  
    var tb_gajiMingguan = '';

    tb_gajiMingguan = $('#tb_gajiMingguan').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
          type : 'POST',
          url : '{{ route('gaji.datatable_gaji_mingguan') }}',
          data: function (e) {
                e._tglAwal = $('#tglAwal').val();
                // e._tglAkhir = $('#tglAkhir').val();
              } 
      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'kodep', name: 'kodep' },
        { data: 'nama', name: 'nama' },
        { data: 'jumat', name: 'jumat' },
        { data: 'sabtu', name: 'sabtu' },
        { data: 'minggu', name: 'minggu' },
        { data: 'senin', name: 'senin' },
        { data: 'selasa', name: 'selasa' },
        { data: 'rabu', name: 'rabu' },
        { data: 'kamis', name: 'kamis' },
        { data: 'total_gaji_mingguan', name:'total_gaji_mingguan'},
        { data: 'lembur', name: 'lembur' },
        { data: 'total_potongan', name:'total_potongan'},
        { data: 'total', name:'total'},
        // { data: 'opsi', name: 'opsi' }
      ]
    });

    function excel_gaji() {
      var tgl_m = $('#tglAwal').val();
      // var tgl_a = $('#tglAkhir').val();

      // var tgl_gabung = tgl_m+'&'+tgl_a;

      var url = '{{ route('gaji.excel_gaji', ['.p']) }}';
      var url_fix = url.replaceAll('.p',tgl_m);
      
      window.location.href = url_fix;
    }

    function buat_jurnal() {
      var tgl_m = $('#tglAwal').val();

      $.ajax({
      type  : 'POST',
      url   : '{{ route('gaji.buat_jurnal') }}',
      data  : {
                '_tglM' : tgl_m
              },
      success : function (e) {
        notif(e.code, e.msg);
      }
      });
    }
    
  </script>
@endpush