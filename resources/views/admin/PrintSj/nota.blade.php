@extends('master.print')
    
@section('content')
<style type="text/css">
  body {
    /* border: 1px solid #000 !important; */
    font-size : 1.3em !important;
  }
  .table-bordered, .table th {
    border: 1px solid #000 !important;
    padding: .3rem;
  }
  .x_panel {
    border : none !important;
  }
  .table td {
    border: none !important;
    padding: .3rem;
  }
  .nominal {
    float: right;
  }
  tr .total_gf {
    border: 1px solid #000 !important;
  }
</style>
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

        <!-- page content -->
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">

                    <section class="content invoice">
                      <center><h1>Nota</h1></center>
                       <!-- title row -->
                        <div class="row">
                          <div class="col-md-2">
                           <strong>No.</strong> 
                          </div>
                          <div class="col-md-5">
                             {{ $id }}
                          </div>
                          <div class="col-md-2">
                           <strong>Kepada</strong> 
                          </div>
                          <div class="col-md-3">
                            {{ $nama }}
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-2">
                           <strong>Tanggal.</strong> 
                          </div>
                          <div class="col-md-5">
                             {{ $tgl }}
                          </div>
                          <div class="col-md-2">
                           {{-- <strong></strong>  --}}
                          </div>
                          <div class="col-md-3">
                            {{ $alamat }}
                          </div>
                        </div>

                      
                      <!-- /.row -->
                      <br>
                      <!-- Table row -->
                      <div class="row">
                        <div class="table">
                          <table class="table tabelku" id="tb_nota">
                            <thead>
                              <tr id="header_nota">
                                <th><center>No.</center></th>
                                <th>Nama Barang</th>
                                <th>Ketr</th>
                                <th>Satuan</th>
                                <th><center>Qty</center></th>
                                <th><span class="nominal">Harga</span></th>
                                <th><span class="nominal">Subtotal</span></th>
                              </tr>
                            </thead>
                            <tbody>
                              @php
                                  $no = 1;
                              @endphp
                                @foreach ($detail_barang as $item)
                                    <tr>
                                      <td> <center> {{ $no++ }} </center> </td>
                                      <td> {{ $item->nama_brg }} </td>
                                      <td> {{ $item->ketr}} </td>
                                      <td> {{ $item->satuan }} </td>
                                      <td> <center>{{ $item->qty }} </center> </td>
                                      <td> <span class="nominal"> {{ number_format($item->harga, 0, ',', '.') }} </span></td>
                                      <td> <span class="nominal"> {{ number_format((($item->qty * $item->harga)), 0,',','.') }} </span> </td>
                                    </tr>
                                    @if ((isset($item->potongan)) && ($item->potongan !=0))
                                    <tr>
                                      <td></td>
                                      <td colspan="3">&nbsp;&nbsp;&nbsp;Potongan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                      <td> <center>{{ $item->qty }} </center> </td>
                                      <td> <span class="nominal"> {{ number_format($item->potongan, 0, ',', '.') }} </span></td>
                                      <td> <span class="nominal"> (- {{ number_format((($item->qty * $item->potongan)), 0,',','.') }})</span> </td>
{{--                                       
                                      <td></td>
                                      <td colspan="3">&nbsp;&nbsp;&nbsp;Potongan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{  - $item->potongan }}</td>
                                      <td><center>{{ $item->qty }}</center></td>
                                      <td class="nominal">{{ $item->potongan }}</td>
                                      <td class="nominal">{{ $item->potongan * $item->qty }}</td> --}}
                                      {{-- <td class="nominal">{{ $item->qty * $item->potongan }}</td> --}}
                                    </tr> 
                                    @endif
                                    
                                @endforeach
                                <tr style="border-top:1px solid #000">
                                  <td colspan="6" ><b style="float:right">Total</b></td>
                                  <td id="grandtotal" colspan="7"> <span class="nominal">{{ number_format($total, 0,',','.') }} </span></td>
                                </tr>
                                
                                  @if ( $ongkir != 0 ) 
                                    <tr>
                                      <td colspan="6" ><b style="float:right">Ongkir</b></td>
                                      <td id="ongkir" colspan="7" ><span class="nominal"> {{ number_format($ongkir, 0,',','.') }}</span></td>
                                    </tr>
                                    <tr>
                                      <td colspan="6"><b style="float:right">Grand Total</b></td>
                                      <td id="grandtotal" colspan="7" ><span class="nominal">{{ number_format($ongkir+$total, 0,',','.') }}</span></td>
                                    </tr>
                                  @else 

                                  @endif
                                
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-md-6">
                          @if ($pembayaran == 'Transfer' ) 
                              <p class="lead">Pembayaran: {{ $pembayaran }} ke 
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                   No. Rek : <strong>{{ $no_rek }}</strong>
                                  <br> Nama : <strong>{{ $bank }}</strong> 
                                </p>
                          @else 
                              <p class="lead">Pembayaran: {{ $pembayaran }}</p>
                          @endif 
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                          <p class="lead"> Cek</p>
                          <table>
                            <tr></tr>
                            <tr></tr>
                            <tr>
                              <br>
                              <br>
                              {{-- <br> --}}
                              {{-- <td><strong>{{ $cek_nota }}</strong></td> --}}
                            </tr>
                          </table>
                        </div>

                        <div class="col-md-3">
                          <p class="lead"> Hormat Kami,</p>
                          <table>
                            <tr></tr>
                            <tr></tr>
                            <tr>
                              <br>
                              <br>
                              {{-- <br> --}}
                              <td><strong>{{ $mengetahui }}</strong></td>
                            </tr>
                          </table>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- this row will not appear when printing -->
                      {{-- <div class="row no-print">
                        <div class=" ">
                          <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                          <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>
                          <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                        </div>
                      </div> --}}
                    </section>
                  </div>
                </div>
              </div>
            </div>
        <!-- /page content -->

@endsection

@push('js')
  <script type="text/javascript">
    $(function(){
      window.print();
    }); 
  </script>
@endpush