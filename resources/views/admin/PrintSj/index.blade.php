@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    {{-- Modal Cek Nota --}}
    <div id="modal_ceknota" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Cek Nota</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- Kiri --}}
            <div class="col-md-6 col-sm-6">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="hidden" id="formSj" name="" class="form-control" readonly>
                  <input type="text" id="" name="id" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="tgl" name="tgl" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kepada
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="id_pelanggan" name="id_pelanggan" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Catatan Khusus
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="catatan" class="form-control" readonly>
                </div>
              </div>
            </div>

            {{-- Kanan --}}
            <div class="col-md-6 col-sm-6">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pembayaran
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="pembayaran" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pengiriman
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="pengiriman" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Mobil
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="id_kendaraan" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Sopir
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="id_karyawan" class="form-control" readonly>
                </div>
              </div>
            </div>
            </form>
            <div class="clearfix"></div>
            <div class="ln_solid"></div>
            <table class="table table-striped table-bordered" id="tb_ceknota" style="width:100%">
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Ketr</th>
                      <th>Satuan</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Potongan</th>
                      <th>Subtotal</th>
                      <th>Opsi</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Approve</button>
          </div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Print Surat Jalan Triplek</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('PrintSj.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <!-- <label class="col-form-label col-md-3 label-align" for="first-name">Tanggal</label>
                <div class="col-md-2">
                  <input type="text" id="tglExport_m" name="tanggal_export" class="form-control" autocomplete="off">
                </div>
                <div class="col-md-2">
                  <input type="text" id="tglExport_a" name="tanggal_export" class="form-control" autocomplete="off">
                </div>
                  <button type="button" onclick="export_excel()" class="btn btn-primary">Excel</button>
                  <button type="button" onclick="delete_tgl()" class="btn btn-danger">Hapus</button>
                </div> -->
                
                <div class="card-box table-responsive">
                  <table id="tbSuratJalan" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No. Nota</th>
                        <th>tgl</th>
                        <th>Kepada</th>
                        <th>Pembayaran</th>
                        <th>Pengiriman</th>
                        <th>Sopir</th>
                        <th>Mobil</th>
                        <th>Total</th>
                        <th>Cek Nota</th>
                        {{-- <th>Cek Sj</th> --}}
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
      $('#tglExport_m').datetimepicker({
        format: 'YYYY-MM-DD'
      });

      $('#tglExport_a').datetimepicker({
        format: 'YYYY-MM-DD'
      });
    // console.log('uyeee');
    var datatableSj = $('#tbSuratJalan').DataTable({
        processing: true,
        serverSide: true,
        order: false,
        ajax: {
            type: 'GET',
            url: '{{ route('PrintSj.datatable') }}', 
        },
        columns: [
          { data: 'id', name: 'id' },
          { data: 'tgl', name: 'tgl' },
          { data: 'nama', name: 'nama' },
          { data: 'pembayaran', name: 'pembayaran' },
          { data: 'pengiriman', name: 'pengiriman' },
          { data: 'sopir', name: 'sopir' },
          { data: 'kendaraan', name: 'kendaraan' },
          { data: 'total', name: 'total' },
          { data: 'is_cek_nota', name: 'is_cek_nota' },
          // { data: 'cek_sj', name: 'cek_sj' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_suratjalan(id) {
      $.ajax({
        type    : 'GET',
        url     : '{{ route('PrintSj.delete') }}',
        data    : { '_idSj' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableSj.draw(); 
        }
      });
    }

    $(document).on('click', '.print_sj', function() {
      var id_sj = $(this).attr('id');
      var jenis = 'print_sj';
      $.ajax({
        type    : 'POST',
        url     : '{{ route('PrintSj.print_sj_count') }}',
        data    : { '_idSj' : id_sj,
                    '_jenis' : jenis },
        success : function (e) {
          notif(e.code, e.msg);
          datatableSj.ajax.reload();
          window.location.href = 'print_sj/sj/'+id_sj;
        }
      });
    });

    $(document).on('click', '.print_nota', function() {
      var id_sj = $(this).attr('id');
      var jenis = 'print_nota';
      $.ajax({
        type    : 'POST',
        url     : '{{ route('PrintSj.print_sj_count') }}',
        data    : { '_idSj' : id_sj,
                    '_jenis' : jenis },
        success : function (e) {
          notif(e.code, e.msg);
          datatableSj.ajax.reload(); 
            window.location.href = 'print_sj/nota/'+id_sj;
        }
      });
    });

    function export_excel() {
      var tgl_m = $('#tglExport_m').val();
      var tgl_a = $('#tglExport_a').val();
      window.location.href = '/dashboard/PrintSj/excel_sj/'+tgl_m+'&'+tgl_a;
    }

    function delete_tgl() {
      var tgl_m = $('#tglExport_m').val();
      var tgl_a = $('#tglExport_a').val();
      $.ajax({

        
        type    : 'POST',
        url     : '{{ route('PrintSj.delete_tgl') }}',
        data    : { '_tglM' : tgl_m,
                    '_tglA' : tgl_a,
                  },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableSj.draw(); 
        }
      })
    }

  </script>
@endpush