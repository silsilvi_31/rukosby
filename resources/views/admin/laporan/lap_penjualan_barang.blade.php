@extends('master.ella')
    
@section('content')

<style type="text/css">
  body {
    /* border: 1px solid #000 !important; */
  }
  .table-bordered, .table th {
    /* border-bottom: 1px solid #000 !important; */
    border: none !important;
    padding: .3rem;
  }
  .x_panel {
    border : none !important;
  }
  .table td {
    border-bottom: none !important;
    padding: .3rem;
  }
  .nominal {
    float: right;
  }
  tr .total_gf {
    border: 1px solid #000 !important;
  }

  .no {
    width : 10%;
  }

  .akun {
    width: 45%;
  }

  .angka {
    width: 15%; 
  }
</style>

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Laporan Penjualan per Produk</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tgl <span class="required">*</span>
                  </label>
                  <div class="col-md-2 col-sm-2 ">
                    <input type="text" id="tglAwal" name="tgl_awal" class="form-control" autocomplete="off">
                  </div>
                  <div class="col-md-2 col-sm-2 ">
                    <input type="text" id="tglAkhir" name="tgl_akhir" class="form-control" autocomplete="off">
                  </div>
                  <div class="col-md-4 col-sm-4 ">
                    <button type="button" class="btn btn-info" onclick="cari_penjualan_brg()">Cari</button>
                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                      <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle"
                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Export
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                          <button class="dropdown-item" onclick="excel_labarugi()">Excel</button>
                          <a class="dropdown-item" href="#">Pdf</a>
                          <a class="dropdown-item" href="#">Csv</a>
                          <a class="dropdown-item" href="#">Print</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-box table-responsive">

                  <br>
                  <div class="card-box table-responsive">
                    <table id="tb_penjualan_brg" class="table table-striped jambo_table bulk_action" style="width:100%">
                      <thead>
                        <tr>
                          <th>No.</th>
                          {{-- <th>Kode Barang</th> --}}
                          <th>Nama</th>
                          <th>Satuan</th>
                          <th>Qty Terjual</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody id="body_penjualan_brg">
                        
                      </tbody>
                      <tfoot>
                        <td colspan="3" style="text-align:right"> <strong>Total:</strong> </td>
                        <td></td>
                        <td></td>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    $('#tglAwal').datetimepicker({
      format: 'DD-MM-YYYY'
    });

    $('#tglAkhir').datetimepicker({
      format: 'DD-MM-YYYY'
    });

    function num_format(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    var datatable_penjualan_brg = '';

    var datatable_penjualan_brg = $('#tb_penjualan_brg').DataTable({
      processing: true,
      serverSide: true,
      order : false,
      ajax : {
        type : 'POST',
        url : '{{ route('laporan.table_penjualan_barang') }}',
        data : function (e) {
          e._tglAwal = $('#tglAwal').val();
          e._tglAkhir = $('#tglAkhir').val();
        }
      },
      columns: [
          { data: 'no', name: 'no' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'satuan', name: 'satuan' },
          { data: 'qty_terjual', name: 'qty_terjual' },
          { data: 'total_nilai', name: 'total_nilai',className: 'text-right' },
        ],
        
        "footerCallback": function( tfoot, data, start, end, display ) {
          var api = this.api();
          var intVal = function ( i ) {
 
            return typeof i === 'string' ?
              i.replace(/[\$,.]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
          };

          var total_qty = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

          var total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                  var total =  intVal(a) + intVal(b);
                  var total_format =  num_format(total);
                  return total_format;
                }, 0 );

          $( api.column( 3 ).footer() ).html(total_qty);
          $( api.column( 4 ).footer() ).html(total);

        }
    });

    function cari_penjualan_brg() {
      datatable_penjualan_brg.draw();
    }

  </script>
@endpush