@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_content">

            <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Ringkasan Bisnis</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Penjualan</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Pembelian</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="row">
                  <!-- Jurnal element -->
                <div class="col-md-3 col-sm-3 ">
                  <div class="pricing">
                    <a href="{{ route('jurnal.index') }}" >
                    <div class="title">
                      <h2>Laporan</h2>
                      <h1>Jurnal</h1>
                    </div>
                  </a>
                  </div>
                </div>
                <!-- Jurnal element -->

                <!-- Neraca element -->
                <div class="col-md-3 col-sm-3  ">
                  <div class="pricing" >
                    <a href="{{ route('bukubesar.index') }}" >
                      <div class="title" style="background:#ff9f43;">
                        <h2>Laporan</h2>
                        <h1>Buku Besar</h1>
                      </div>
                    </a>
                  </div>
                </div>
                <!-- Neraca element -->

                <!-- Neraca element -->
                <div class="col-md-3 col-sm-3  ">
                  <div class="pricing" >
                    <a href="{{ route('neraca.index') }}" >
                      <div class="title" style="background:#2980b9;">
                        <h2>Laporan</h2>
                        <h1>Neraca</h1>
                      </div>
                    </a>
                  </div>
                </div>
                <!-- Neraca element -->

                 <!-- Laba Rugi element -->
                 <div class="col-md-3 col-sm-3 ">
                  <div class="pricing">
                    <a href="{{ route('labarugi.index') }}" >
                      <div class="title" style="background:#9b59b6;">
                        <h2>Laporan</h2>
                        <h1>Laba Rugi</h1>
                      </div>
                  </a>
                  </div>
                </div>
                <!-- Laba Rugi element -->
                </div>
              </div>

              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                 <!-- Penjualan element -->
                 <div class="col-md-3 col-sm-3 ">
                  <div class="pricing">
                    <a href="{{ route('laporan.lap_penjualan') }}" >
                      <div class="title" style="background:#34ace0;">
                        <h2>Laporan</h2>
                        <br>
                        <span>Penjualan</span>
                      </div>
                  </a>
                  </div>
                </div>
                <!-- Penjualan element -->

                <!-- Penjualan per produk element -->
                <div class="col-md-3 col-sm-3 ">
                  <div class="pricing">
                    <a href="{{ route('laporan.lap_penjualan_barang') }}" >
                      <div class="title" style="background:#84817a;">
                        <h2>Laporan</h2>
                        <br>
                        <span>Penjualan per Produk</span>
                      </div>
                    </a>
                  </div>
                </div>
                <!-- Penjualan per produk element -->

                 <!-- Penjualan per Pelanggan element -->
                 <div class="col-md-3 col-sm-3 ">
                  <div class="pricing">
                    <a href="{{ route('laporan.lap_penjualan_pelanggan') }}" >
                      <div class="title" style="background:#33d9b2;">
                        <h2>Laporan</h2>
                        <br>
                        <span>Penjualan per Pelanggan</span>
                      </div>
                  </a>
                  </div>
                </div>
                <!-- Penjualan per Pelanggan element -->

                <!-- Piutang Pelanggan element -->
                <div class="col-md-3 col-sm-3 ">
                  <a href="{{ route('laporan.lap_piutang_pelanggan') }}" >
                    <div class="pricing">
                        <div class="title" style="background:#706fd3;">
                          <h2>Laporan</h2>
                          <br>
                          <span>Piutang Pelanggan</span>
                        </div>
                    </div>
                  </a>
                </div>
                <!-- Piutang Pelanggan element -->
              </div>

              <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <!-- Pembelian element -->
                <div class="col-md-3 col-sm-3 ">
                  <div class="pricing">
                    <a href="{{ route('laporan.lap_pembelian') }}" >
                      <div class="title" style="background:#ffb8b8;">
                        <h2>Laporan</h2>
                        <br>
                        <span>Pembelian</span>
                      </div>
                  </a>
                  </div>
                </div>
                <!-- Pembelian element -->

                <!-- pembelian per produk element -->
                <div class="col-md-3 col-sm-3 ">
                  <div class="pricing">
                    <a href="{{ route('laporan.lap_pembelian_barang') }}" >
                      <div class="title" style="background:#ff3838;">
                        <h2>Laporan</h2>
                        <br>
                        <span>Pembelian per Produk</span>
                      </div>
                    </a>
                  </div>
                </div>
                <!-- pembelian per produk element -->

                 <!-- pembelian per Suplier element -->
                 <div class="col-md-3 col-sm-3 ">
                  <div class="pricing">
                    <a href="{{ route('laporan.lap_pembelian_suplier') }}" >
                      <div class="title" style="background:#ff9f1a;">
                        <h2>Laporan</h2>
                        <br>
                        <span>pembelian per Suplier</span>
                      </div>
                    </a>
                  </div>
                </div>
                <!-- pembelian per Suplier element -->

                <!-- Hutang element -->
                <div class="col-md-3 col-sm-3 ">
                  <a href="{{ route('laporan.lap_pembelian_hutang') }}" >
                    <div class="pricing">
                        <div class="title" style="background: #18dcff;">
                          <h2>Laporan</h2>
                          <br>
                          <span>Hutang</span>
                        </div>
                    </div>
                  </a>
                </div>
                <!-- Piutang element -->
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
  </script>
@endpush