{{-- @extends('master.print')
    
@section('content') --}}
<table>
        <tr>
            <th>Tgl</th>
            <th>Jurnal</th>
            <th>No Akun</th>
            <th>Ref</th>
            <th>Nama</th>
            <th>Keterangan</th>
            <th>Map</th>
            <th>Hit</th>
            <th>Qty</th>
            <th>M3</th>
            <th>Harga</th>
        </tr>
        @foreach ($data as $item)
            <tr>
                <td>{{ date('d-m-Y', strtotime($item->tgl)) }}</td>
                <td>{{ $item->jurnal }}</td>
                <td>{{ $item->no_akun }}</td>
                <td>{{ isset($item->bk) ? $item->bk : $item->jenis_jurnal.' '.$item->ref }}</td>
                <td>{{ strtolower($item->nama) }}</td>
                <td>{{ $item->keterangan }}</td>
                <td>{{ $item->map }}</td>
                <td>{{ $item->hit }}</td>
                <td>{{ $item->qty }}</td>
                <td>{{ $item->m3 }}</td>
                <td>{{ $item->harga }}</td>
            </tr>
        @endforeach
    </table>
{{-- @endsection --}}