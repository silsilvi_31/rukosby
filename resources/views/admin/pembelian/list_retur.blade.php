@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

     <!-- Awal Modal Detail Retur -->
     <div class="modal fade bs-example-modal-lg" id="modal_detail_item" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Detail Retur</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <input type="text" id="idSj" name="id_sj" hidden>
                <table class="table table-striped table-bordered" id="tb_detail_item" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Tgl.</th>
                              <th>Nama Barang</th>
                              <th>Satuan</th>
                              <th>Qty</th>
                              <th>Harga</th>
                              <th>Potongan</th>
                              <th>Subtotal</th>
                              {{-- <th>Ketr</th> --}}
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Detail Retur -->

    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Retur Penjualan</h2>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <table class="table table-sm table-striped table-bordered" id="tb_retur" style="width:100%">
                <thead>
                    <tr>
                      <th>No.</th>
                      <th>ID Beli</th>
                      <th>Tgl.</th>
                      <th>Opsi</th>
                      {{-- <th>Nama</th> --}}
                      {{-- <th>Barang</th> --}}
                      {{-- <th>Total</th> --}}
                    </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>                                   

            <div class="clearfix"></div>
            <div class="ln_solid"></div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    var tb_retur = $('#tb_retur').DataTable({
        processing: true,
        serverSide: true,
        order: false,
        ajax: {
            type: 'GET',
            url: '{{ route('beliretur.datatable_retur') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'id_sj', name: 'id_sj' },
          { data: 'tgl_retur', name: 'tgl_retur' },
          { data: 'opsi', name: 'opsi' }
          // { data: 'nama', name: 'nama' },
          // { data: 'nama_brg', name: 'nama_brg' },
          // { data: 'total', name: 'total' }
        ]
    });

    $('#modal_detail_item').on('shown.bs.modal', function (event) {
     var $button = $(event.relatedTarget);
     var id = $button.data('id');
     
     $('input[name=id_sj]').val(id);
     tb_detail_item.draw();

   });

    var tb_detail_item = $('#tb_detail_item').DataTable({
      processing: true,
      serverSide: true,
      ajax      : {
        type    : 'POST',
        url     : '{{ route('beliretur.datatable_detail_item') }}',
        data : function (e) {
          e._id = $('input[name=id_sj]').val();
        }
      },
      columns   : [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'tgl_retur', name: 'tgl_retur' },
        { data: 'nama_brg', name: 'nama_brg' },
        { data: 'nama', name: 'nama' },
        { data: 'qty_retur', name: 'qty_retur' },
        { data: 'harga', name: 'harga' },
        { data: 'potongan', name: 'potongan' },
        { data: 'subtotal', name: 'subtotal' },
        // { data: 'ketr', name: 'ketr' },
        { data: 'opsi', name: 'opsi' },
      ]
    });

    function delete_detail_item(id_sj, id_item) {
      var txt;
      var r = confirm('Apakah Anda yakin ingin menghapus?')

      if(r == true) {
        $.ajax({
          type    : 'POST',
          url     : '{{ route('beliretur.delete_detail_item') }}',
          data    : { 
                      '_idSj'   : id_sj,
                      '_idItem' : id_item
                    },
          success : function (e) {
            notif(e.response.code, e.response.msg);
            $('#modal_detail_item').modal('hide');
            tb_detail_item.draw(); 
            tb_retur.draw();
          }
        });
      } else {
        txt = 'n';
      }
    }
  </script>
@endpush