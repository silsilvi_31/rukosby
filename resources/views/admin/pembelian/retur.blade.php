@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Retur Penjualan</h2>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="idBeli" name="id_beli" required="required" class="form-control" readonly value="{{ !empty($id) ? $id : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tgl Jual
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" class="form-control" readonly value="{{ !empty($tgl) ? date('d-m-Y', strtotime($tgl)) : date('d-m-Y') }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tgl Retur <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="tglRetur" name="tgl_retur" required='required' class="form-control" autocomplete="off">
                    </div>
                  </div>
                </div>
              </form>

              <table class="table table-sm table-striped table-bordered" id="tb_detail_retur" style="width:100%">
                <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Qty</th>
                      <th>Qty Retur</th>
                      <th>Satuan</th>
                      {{-- <th>Harga</th> --}}
                      <th>Hpp</th>
                      <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>                                   

            <div class="clearfix"></div>
            <div class="ln_solid"></div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <div class="offset-md-3">
                  <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                  <button type="button" class="btn btn-sm btn-success" onclick="simpan_retur()">Submit</button>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    $('#tglRetur').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    var tb_detail_retur = $('#tb_detail_retur').DataTable({
      processing : true,
      serverSide : true,
      scrollY: true,
      paging: false,
      searching: false,
      ajax : {
        type : 'POST',
        url : '{{ route('beliretur.datatable_detail_retur') }}',
        data : function (e) {
          e._id = $('input[name=id_beli]').val();
        }
      },
      columns : [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'nama_brg', name:'nama_brg' },
            { data: 'qty', name:'qty' },
            { data: 'qty_retur', name:'qty_retur' },
            { data: 'satuan', name:'satuan' },
            // { data: 'harga', name:'harga' },
            { data: 'hpp', name:'hpp' },
            { data: 'subtotal', name:'subtotal' },
      ]
    }); 

    $(document).on('keyup', '.qty_retur', function(){    
      var id = $(this).attr('id');
      get_subtotal(id);

    });


    function get_subtotal(id) {
      var idd = id.slice(1);
      var qty_retur = $('#'+id).val();
      var hpp = $('#hpp'+idd).text();
      var qty = $('#qty'+idd).text();

      var msg = false;

      if (Number(qty_retur) > Number(qty)) {
        msg = confirm('Qty Retur Melebihi Qty Jual');
      }

      var subtotal = qty_retur * hpp;
      if (msg == true) {
        subtotal = '';
        $('#'+id).val('');
        $('#s'+idd).text('');
      }

      $('#s'+idd).text(subtotal);
      console.log(msg);
    }

    function simpan_retur() {
      var id_beli = $('#idBeli').val();
      var tgl_retur = $('#tglRetur').val();
      var dt = [];

      $('.qty_retur').each(function () {
        var $this = $(this).attr('id');
        var qty_retur = $('#'+$this).val();
        var id = $this.slice(1);
        var subtotal = $('#s'+id).text();
        var hpp = $('#hpp'+id).text();


        input = { 
          'id'        : $this,
          'id_item'    : id,
          'qty_retur' : qty_retur,
          'hpp'       : hpp,
          'subtotal'  : subtotal
        }
        dt.push(input);
      });

      $.ajax({
        type: 'POST',
        url: '{{ route('beliretur.save')}}',
        data : { 
                '_idBeli'     : id_beli,
                '_tglRetur'   : tgl_retur,
                '_dtQty'      : dt,
                },

        success: function (e) {
          console.log(e);
          notif(e.response.code, e.response.msg);
        }
      });
    }

  </script>
@endpush