@extends('master.ella')
    
@section('content')

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Buku Besar</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <label class="col-form-label col-md-4 label-align" for="first-name">Tanggal</label>
                <div class="col-md-2">
                  <input type="text" id="Tgl" name="tgl" class="form-control" autocomplete="off">
                </div>
                <div class="col-md-2">
                  <input type="text" id="tglDua" name="tgl_dua" class="form-control" autocomplete="off">
                </div>
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                  <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle"
                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Export
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                      <button class="dropdown-item" onclick="excel_bukubesar()">Excel</button>
                      <a class="dropdown-item" href="#">Pdf</a>
                      <a class="dropdown-item" href="#">Csv</a>
                      <a class="dropdown-item" href="#">Print</a>
                    </div>
                  </div>
                </div>
              <div class="card-box table-responsive">
                <br>
                <table id="table_bukubesar" class="table table-sm table-striped jambo_table bulk_action" style="width:100%">
                  <thead>
                    <tr>
                      <th>Akun/Tgl</th>
                      <th>Transaksi</th>
                      <th>Nama</th>
                      <th>Keterangan</th>
                      <th>Ref</th>
                      <th class="angka text-right">Debit</th>
                      <th class="angka text-right">Kredit</th>
                      <th class="angka text-right">Saldo</th>
                    </tr>
                  </thead>
                  <tbody id="body_bukubesar">
                    
                  </tbody>
                </table>
              </div>
            </div> 
  
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
  
    $(document).ready(function() {
      $('.expandable').nextAll('tr').each(function() { // hide all at start
        if (!($(this).is('.expandable')))
          $(this).hide();
      });

      $('.expandable input[type=button]').click(function() { // toggle single by click
        var trElem = $(this).closest("tr");
        trElem.nextAll('tr').each(function() {
          if ($(this).is('.expandable')) {
            return false;
          }
          $(this).toggle();
        });
      });
    });

    var tt_pendapatan;
    var tt_hpp;
    var tt_beban;
    var hasil;

    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('#tglDua').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $("#Tgl").on("dp.change", function (e) {
        var tgl = $(this).val();
        var tgl_dua = $('#tglDua').val();
        tampil_bukubesar(tgl, tgl_dua);
    });

    $("#tglDua").on("dp.change", function (e) {
        var tgl = $('#Tgl').val();
        var tgl_dua = $(this).val();
        tampil_bukubesar(tgl, tgl_dua);
    });

    tampil_bukubesar();

    var tipe = 0;

    function tampil_bukubesar(tgl, tglDua) {
      var tr = "";
      tipe = 0;
      if (tgl && tglDua) {
        tipe = 1;
      }
      $.ajax({
        type : 'POST',
        url : '{{ route('bukubesar.datatable') }}',
        data: { 
                '_tgl' : tgl,
                '_tglDua' : tglDua,
               },
        success: function (e) {
          $('#body_bukubesar').empty(); 
          $.each(e.data, function (key, value) { 
            if (value.tipe == "parent") {
              tr += '<tr>';
              tr += '<td class="'+value.no_akun+'">' + value.nama_akun + '</td>';
              tr += '<td>' + value.jenis_jurnal + '</td>';
              tr += '<td>' + value.nama + '</td>';
              tr += '<td>' + value.keterangan + '</td>';
              tr += '<td>' + value.ref + '</td>';
              tr += '<td class="text-right">' + value.debit + '</td>';
              tr += '<td class="text-right">' + value.kredit + '</td>';
              tr += '<td class="text-right">' + value.total + '</td>';
              tr += '</tr>';
            } else if (value.tipe == "child") {
              tr += '<tr class="'+value.no_akun+'c" hidden>';
              tr += '<td>' + value.nama_akun + '</td>';
              tr += '<td>' + value.jenis_jurnal + '</td>';
              tr += '<td>' + value.nama + '</td>';
              tr += '<td>' + value.keterangan + '</td>';
              tr += '<td>' + value.ref + '</td>';
              tr += '<td class="text-right">' + value.debit + '</td>';
              tr += '<td class="text-right">' + value.kredit + '</td>';
              tr += '<td class="text-right">' + value.total + '</td>';
              tr += '</tr>';
            }
          });    
          
          $('#body_bukubesar').append(tr);
        }
      });
    }

    function numberformat(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }  

    function excel_bukubesar() {
      var tgl_m = $('#Tgl').val();
      var tgl_a = $('#tglDua').val();

      var tgl_gabung = tgl_m+'&'+tgl_a;

      var url = '{{ route('bukubesar.excel_bukubesar', ['.p']) }}';
      var url_fix = url.replaceAll('.p',tgl_gabung);
      // console.log(url_fix);
      window.location.href = url_fix;
    }

    function show_all(no_akun) {
      var id = $('#'+no_akun).val();
      if (tipe == 0) {
        notif(400, 'Tarik tanggal dulu Sist :) ')
      }

      if (id == 0) {
        $('.'+no_akun+'c').removeAttr('hidden');
        $('#'+no_akun).val(1);
      } else {
        $('.'+no_akun+'c').attr('hidden', true);
        $('#'+no_akun).val(0);
      }
    }
  </script>
@endpush