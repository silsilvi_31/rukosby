@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
     <!-- Awal Modal Setor -->
     <div class="modal fade bs-example-modal-md" id="modal_setor" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Setor</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control" value="{{ date('d-m-Y') }}" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Saldo
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Saldo" name="saldo" class="form-control" value= {{ $saldo }} readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="offset-md-3 pull-right">
                      {{-- <button type="button" class="btn btn-sm btn-primary">Cancel</button> --}}
                      {{-- <button type="reset" class="btn btn-sm btn-primary">Reset</button> --}}
                      <button type="button" class="btn btn-sm btn-success" onclick="simpan_setor()"> Submit</button>
                    </div>
                  </div>
                </form>
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Setor -->

    <div class="row">
      <div class="col-md-12 col-sm-12">
          <div class="tile_count">
            <div class="tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Saldo</span>
              <div class="count">Rp. {{ $saldo }}
                <button type="button" class="btn btn-md btn-info" data-toggle="modal" data-target="#modal_setor" >Setor</button>
              </div>
              {{-- <span class="count_bottom"><i class="green">Per </i> Hari Ini</span> --}}
            </div>
          </div>

          <div class="card-box table-responsive">
            <table id="tb_setor" class="table table-sm table-striped table-bordered" style="width:100%">
              <thead>
                <tr>
                  <th>No. </th>
                  <th>Tgl Setor</th>
                  <th>Uang Masuk</th>
                  <th>Uang Keluar</th>
                  <th>Saldo</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
          </div>

      </div>
    </div>
    
  </div>
</div>  
  @push('js')
  <script type="text/javascript">
      $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

     var tb_setor = $('#tb_setor').DataTable({
        processing: true,
        serverSide: true,
        order: false,
        ajax: {
            type: 'GET',
            url: '{{ route('setor.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'tgl', name: 'tgl' },
          { data: 'uang_masuk', name: 'uang_masuk' },
          { data: 'uang_keluar', data: 'uang_keluar' },
          { data: 'saldo', name: 'saldo' },
          { data: 'status', name: 'status' },
        ]
    });

    function simpan_setor() {
      var tgl = $('#Tgl').val();
      var saldo = $('#Saldo').val();

      $.ajax({
        type    : 'POST',
        url     : '{{ route('setor.simpan') }}',
        data    : {
                    '_tgl'    : tgl,
                    '_saldo'  : saldo
                  },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          tb_setor.draw();
        }
      });
    }

  </script>
@endpush
@endsection

