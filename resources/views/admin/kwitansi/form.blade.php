@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <!-- Awal Modal Akun -->
    <div class="modal fade bs-example-modal-lg" id="modal_akun" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Akun</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tb_akun" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>No. Akun</th>
                              <th>Nama</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Akun -->

    <!-- Awal Modal Barang -->
    <div class="modal fade bs-example-modal-lg" id="modal_brg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tabel_brg" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Nama</th>
                              <th>Harga</th>
                              <th>Satuan</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Awal Modal barang -->

    {{-- modal tambah barang --}}
    {{-- <div class="modal fade bs-example-modal-lg" id="modal_add_barang" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Barang <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="namaBrgMd" name="nama_brg_md" class="form-control">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> p x l x t
                    </label>
                    <div class="col-md-2 col-sm-6 ">
                      <input type="text" id="panjangmd" name="panjang_md" class="form-control">
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                      <input type="text" id="lebarmd" name="lebar_md" class="form-control">
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                      <input type="text" id="tebalmd" name="tebal_md" class="form-control">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Harga <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="hargaMd" name="harga_md" class="form-control">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="idSatuanMd"  name="id_satuan_md" class="form-control">
                        <option value="">-</option>
                        @foreach ($satuan as $item)
                          <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Jenis Barang
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="jenisBrgMd" name="jenis_brg_md" class="form-control">
                        <option value="">-</option>
                        @foreach ($jenis_barang as $item)
                          <option value="{{ $item->id }}">{{ $item->jenis_brg }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Akun
                    </label>
                    <div class="col-md-6 ">
                      <div class="col-md-6 col-sm-6 ">
                        <select id="noAkunMd" name="no_akun_md" class="form-control">
                          <option value="">-</option>
                          @foreach ($akun as $item)
                            <option value="{{ $item->no_akun }}">{{ $item->no_akun }} - {{ $item->akun }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                  <div class="ln_solid"></div>
              
                <div class="form-group">
                  <div class="offset-md-3">
                    <button type="button" class="btn btn-sm btn-primary">Cancel</button>
                    <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                    <button type="button" class="btn btn-sm btn-success" onclick="add_barang()"> Submit</button>
                  </div>
                </div>
                </div>
          </div>
      </div>
    </div> --}}
    {{-- //end modal tambah barang --}}

    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Kwitansi</h2>
              <ul class="nav navbar-right panel_toolbox">
                <a href=" {{ route('kwitansi.form') }} " class="btn btn-sm btn-success">New</a>
                <a href=" {{  route('kwitansi.index') }} " class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back </a>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Kwitansi <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 ">
                      <input type="hidden" id="Form" name="form" required="required" class="form-control" readonly value="{{ !empty($form) ? $form : '' }}">
                      <input type="text" id="noKwi" name="no_kwi" required="required" class="form-control" readonly value="{{ !empty($no_kwi) ? $no_kwi : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control" value="{{ !empty($tgl) ? $tgl : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Dari <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Dari" name="dari" required="required" class="form-control" value="{{ !empty($dari) ? $dari : ''}}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Penerima <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <input type="text" id="Penerima" name="penerima" required='required' class="form-control" value="{{ !empty($penerima) ? $penerima : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Keterangan
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="Ketr" name="ketr" class="form-control" value="{{ !empty($ketr) ? $ketr : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Jumlah
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="Jumlah" name="jumlah" class="form-control" value="{{ !empty($jumlah) ? $jumlah : '' }}">
                    </div>
                  </div>
                </div>

                @php
                  $jenis_kwi = !empty($jenis_kwitansi) ? $jenis_kwitansi : ''; 
                  $akun_debit = !empty($akun_debit) ? $akun_debit : ''; 
                  $akun_kredit = !empty($akun_kredit) ? $akun_kredit : '';    
                @endphp
                {{-- Kanan --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Jenis Kwitansi <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select class="form-control" name="jenis_kwitansi" id="jenisKwitansi" autocomplete="off" >
                        <option></option>
                        <option value="Masuk" @if($jenis_kwi == "Masuk") selected @endif>Pemasukan</option>                              
                        <option value="Keluar" @if($jenis_kwi == "Keluar") selected @endif>Pengeluaran</option>                              
                      </select>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Akun Debit <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="akunDebit" name="akun_debit" class="form-control" autocomplete="off">
                        <option value="">-</option>
                        @foreach ($akun as $item)
                        <option value="{{ $item->no_akun }}" @if($item->no_akun == $akun_debit) selected @endif>{{ $item->no_akun }} - {{ $item->akun }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Akun Kredit <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="akunKredit" name="akun_kredit" class="form-control" autocomplete="off">
                        <option value="">-</option>
                        @foreach ($akun as $item)
                          <option value="{{ $item->no_akun }}" @if($item->no_akun == $akun_kredit) selected @endif>{{ $item->no_akun }} - {{ $item->akun }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      {{-- <button type="button" class="btn btn-primary">Cancel</button> --}}
                      <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="save_kwitansi()">Submit</button>
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>

              <div class="clearfix"></div>
                <div class="ln_solid"></div>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    var form = $('#Form').val();
    if (form == 'edit') {

    } else {
      no_urut();
    }

    function no_urut() {
      $.ajax({
              type : 'GET',
              url : '{{ route('no_urut_kwi') }}',
              success : function (e) {
                console.log(e);
                $('#noKwi').val(e);
              }
      });
    }

    function save_kwitansi() {
      var no_kwi = $('#noKwi').val();
      var tgl = $('#Tgl').val();
      var dari = $('#Dari').val();
      var penerima = $('#Penerima').val();
      var Keterangan = $('#Ketr').val();
      var jumlah = $('#Jumlah').val();
      var akun_debit = $('#akunDebit').val();
      var akun_kredit = $('#akunKredit').val();
      var jenis_kwitansi = $('#jenisKwitansi').val();
      
      $.ajax({
        type  : 'POST',
        url   : '{{ route('kwitansi.save') }}',
        data  : {
                  '_noKwi' : no_kwi,
                  '_tgl' : tgl,
                  '_dari' : dari,
                  '_penerima' : penerima,
                  '_ketr' : Keterangan,
                  '_jumlah' : jumlah,
                  '_akunDebit' : akun_debit,
                  '_akunKredit' : akun_kredit,
                  '_jenisKwitansi' : jenis_kwitansi
                },
        success  : function (e) {
          // notif(e.code,e.msg);
          clear_akun();
          window.location.href = '{{ route('kwitansi.index') }}';
        }
      });
    }

    function clear_akun() {
      $('#Tgl').val('');
      $('#Dari').val('');
      $('#Penerima').val('');
      $('#Ketr').val('');
      $('#Jumlah').val('');
    }

    $(document).on('click', '.noAkunQ', function(){  
      $('#modal_akun').modal('show');  
    });

    $(document).on('click', '.brgQ', function(){  
      $('#modal_brg').modal('show');  
    });

    var tb_akun = '';

    function select_akun(no_akun, akun) {
      $('#modal_akun').modal('hide');
      $('#noAkunKredit').val(no_akun);
      $('#namaAkunKredit').val(akun);
    }

    function select_brg(kode_brg,nama_brg,harga, id_satuan, satuan, no_akun) {
      $('#modal_brg').modal('hide');
      $('#idBrg').val(kode_brg);
      $('#namaBrg').val(nama_brg);
      $('#Harga').val(harga);
      $('#idSatuan').val(id_satuan);
      $('#Satuan').val(satuan);
      $('#noAkunDebit').val(no_akun)
      clear_barang2();
    }   

    function clear_barang2() {
      $('#Qty').val('');
      $('#Ketr').val('');
      $('#Subtotal').val('');
      $('#idSatuan').val('');
    }

    var tb_barang = $('#tabel_brg').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('pembelian.datatable_brg') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'harga', name: 'harga' },
          { data: 'satuan', name: 'satuan' },
          { data: 'opsi', name: 'opsi' },
        ]
      });

      function hitung_subtotal(harga, qty) {
        var subtotal = ( harga * qty );
        $('#Subtotal').val(subtotal);
      }

      $(document).on('keyup', '.qtyQ', function(){
        var qty = $('#Qty').val();
        var harga = $('#Harga').val();
        hitung_subtotal(harga, qty);
      });

      $(document).on('click', '.barangQ', function(){  
        $('#modal_add_barang').modal('show');  
      }); 

      var tb_detail = '';
      tb_detail = $('#tb_detail').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'POST',
            url: '{{ route('pembelian.datatable_detail') }}', 
            data : function (e) {
              e._idBeli = $('#idBeli').val();
            }
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'harga', name: 'harga' },
          { data: 'satuan', name: 'satuan' },
          { data: 'qty', name: 'qty' },
          { data: 'subtotal', name: 'subtotal' },
          { data: 'ketr', name: 'ketr' },
          { data: 'opsi', name: 'opsi' },
        ]
      });

      function delete_item(id) {
      $.ajax({
        type : 'POST',
        url : '{{ route('pembelian.delete_item') }}',
        data : { '_idItem' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          tb_detail.draw();
          }
        });
      }

      function add_barang() {
        var nama_brgMd = $('#namaBrgMd').val();
        var harga_brgMd = $('#hargaMd').val();
        var id_satuanMd = $('#idSatuanMd').val();
        var jenis_brgMd = $('#jenisBrgMd').val();
        var no_akunMd = $('#noAkunMd').val();

        $.ajax({
        type  : 'POST',
        url   : '{{ route('add_brg.save') }}',
        data  : {
                  '_namaBrg' : nama_brgMd,
                  '_harga' : harga_brgMd,
                  '_idSatuan' : id_satuanMd,
                  '_jenisBrg' : jenis_brgMd,
                  '_noAkun' : no_akunMd
                },
        success  : function (e) {
          console.log(e);
          notif(e.code, e.msg);
          $('#modal_add_barang').modal('hide');
          tb_barang.draw();
          $('#idBrg').val(e.kode);
          $('#namaBrg').val(e.nama_brg);
          $('#Harga').val(e.harga);
          $('#idSatuan').val(e.id_satuan);
          $('#noAkunDebit').val(e.no_akun);
          $('#namaBrgMd').val('');
          $('#hargaMd').val('');
          $('#idSatuanMd').val('');
          $('#jenisBrgMd').val('');
          $('#noAkunMd').val('');
        }
      });



      }
  </script>
@endpush