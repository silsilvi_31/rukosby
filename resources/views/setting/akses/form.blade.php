@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="modal fade" id="modal_akses" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Akses</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="card-box table-responsive">
                <table id="tbAkses" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Akses</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal_fitur" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Fitur</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="card-box table-responsive">
                <table id="tbFiturA" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Fitur</th>
                      <th>Parent</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Form Akses</h2>
            <ul class="nav navbar-right panel_toolbox">
              {{-- <a href="{{ route('menu.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a> --}}
              <a href="{{ route('akses.index') }}" class="btn btn-sm btn-info">Data</a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-6">
                <form action="#" method="#" class="form-horizontal form-label-left">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Nama <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="hidden" name="id" id="idAkses" class="form-control" value="{{ isset($id_akses) ? $id_akses : NULL }}" readonly autocomplete="off">
                      <input type="text" name="akses" id="nmAkses" class="form-control " autocomplete="off" value="{{ isset($akses) ? $akses : NULL }}">
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                      <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_akses"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
    
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Fitur <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="hidden" name="id_fitur" id="idFitur" class="form-control" autocomplete="off" value="" readonly>
                      <input type="text" name="nm_fitur" id="nmFitur" class="form-control" autocomplete="off" value="" readonly>
                    </div>
                    <div class="col-md-4">
                      <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_fitur"><i class="fa fa-search"></i></button>
                      <button type="button" class="btn btn-sm btn-success" onclick="add_fitur()"><i class="fa fa-plus"></i></button>
                      <button type="button" class="btn btn-sm btn-danger" onclick="clear_fitur()"><i class="fa fa-eraser"></i></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            
            <br>
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbFiturB" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Fitur</th>
                        <th>Parent</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    console.log('uyeee');

    var akses = $('#tbAkses').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('list_akses.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama', name: 'nama' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    var dataFitur = $('#tbFiturA').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('list_fitur.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama', name: 'nama' },
          { data: 'parent', name: 'parent' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    var dataFiturB = $('#tbFiturB').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type : 'POST',
            url  : '{{ route('item_fitur.datatable') }}', 
            data : function (e) {
              e._idAkses = $('#idAkses').val();
            }
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama', name: 'nama' },
          { data: 'parent', name: 'parent' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function select_akses(id, nama) {
      $('#idAkses').val(id);
      $('#nmAkses').val(nama);
      dataFiturB.draw();
      clear_fitur();
      $('#modal_akses').modal('hide');
    }

    function select_fitur(id, nm) {
      $('#idFitur').val(id);
      $('#nmFitur').val(nm);
      
      $('#modal_fitur').modal('hide');
    }

    function clear_fitur() {
      $('#idFitur').val('');
      $('#nmFitur').val('');
    }

    function add_fitur() {
      var id_akses = $('#idAkses').val();
      var nm_akses = $('#nmAkses').val();
      var id_fitur = $('#idFitur').val();

      $.ajax({
        type    : 'POST',
        url     : '{{ route('akses.save') }}',
        data    : { 
                    '_idAkses' : id_akses,
                    '_nmAkses' : nm_akses,
                    '_idFitur' : id_fitur
                  },
        success : function (e) {
          notif(e.code, e.msg);
          $('#idAkses').val(e.id_akses);
          akses.draw();
          dataFiturB.draw();
        }
      });
    }

    function delete_fitur(id) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('item_fitur.delete') }}',
        data    : { '_id' : id },
        success : function (e) {
          notif(e.code, e.msg);
          dataFiturB.draw();          
        }
      });
    }

  </script>
@endpush