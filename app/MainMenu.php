<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainMenu extends Model
{
    protected $table = 'menu';

    public function parent()
    {
        return $this->hasMany('App\MainMenu', 'parent_id');
    }

    public function child_menu()
    {
        return $this->hasMany('App\MainMenu', 'parent_id')->with('parent');
    }
}
