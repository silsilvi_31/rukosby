<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use \PDF;
use App\Exports\LabarugiExport;

class LabarugiController extends Controller
{
    private $sj;
    private $beli;
    private $kwitansi;
    private $ju;
    private $opname;

    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");
        $this->sj = DB::table('suratjalan')
                        ->where('is_cek_nota', 1)
                        ->select('id', 'is_batal')
                        ->get();

        $this->beli = DB::table('beli')
                        ->where('is_cek_beli', 1)
                        ->select('id_beli','user_batal')
                        ->get();

        $this->kwitansi = DB::table('kwitansi')
                        ->where('is_cek_kwi', 1)
                        ->select('no_kwi')
                        ->get();

        $this->ju = DB::table('jurnal_umum')
                        ->where('is_cek_jurnal', 1)
                        ->select('id_ju')
                        ->get();

        $this->opname = DB::table('log_opname_stok')
                        ->where('is_cek_opname', 1)
                        ->select('id')
                        ->get();
    }

    public function index()
    {
        return view('admin.labaRugi.index');
    }

    public function total($data)
    {
        $total = 0;
        foreach ($data as $value) {
            $total += $value->total;            
        }
        return $total;
    }

    public function get_akun_child($akun_child, $parent_id)
    {
        $dty = array_filter($akun_child->toArray(), function ($value) use ($parent_id) {
            return $value->parent_id == $parent_id;
        });

        return $dty;
    }

    public function datatable_pendapatan(Request $req)
    {
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $tgl_dua = date('Y-m-d', strtotime($req->_tglDua));

        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';

        $jurnalQ = DB::table('jurnal')
                        ->whereDate('tgl', '<=', $tgl_akhir)
                        ->get();

        $id_jurnalQ = [];
        foreach ($jurnalQ as $value) {
            $id_jurnalQ[] = $value->id;
        }

        $pendapatan = DB::table('akun')
                            ->whereIn('kel', [4, 5, 6])
                            ->whereNotIn('id', $id_jurnalQ)
                            ->orderBy('no_akun')
                            ->get();

        $akun_child = DB::table('akun')
                            ->get();

        $dt = [];
        $total_d = 0;
        $total_k = 0;
        $total_all = 0;
        foreach ($pendapatan as $v) {
            $dt[] = (object) [
                     'no_akun'      => $v->no_akun,
                     'akun'         => $v->akun,
                     'akun_child'   => $this->get_akun_child($akun_child, $v->kel)
            ];
        }

        $dt_akun = [];
        $total_all = 0;

        $penjualan = $this->sj->map(function ($val) {
            return $val->id;
        });

        $pembelian = $this->beli->map(function ($val) {
            return $val->id_beli;
        });

        $batal_pembelian = $this->beli->filter(function ($val) {
            return isset($val->user_batal);
        })->map(function ($v) {
            return $v->id_beli;
        });

        $batal_penjualan = $this->sj->filter(function ($val) {
            return isset($val->is_batal);
        })->map(function ($v) {
            return $v->id;
        });

        $kwitansi_ = $this->kwitansi->map(function ($val) {
            return $val->no_kwi;
        });

        $jurnal_umum = $this->ju->map(function ($val) {
            return $val->id_ju;
        });

        $opname = $this->opname->map(function ($val) {
            return $val->id;
        });

        foreach ($dt as $x) {
            $dt_akun[] = (object) [
                        'no_akun'       => $x->no_akun,
                        'akun'          => $x->akun,
                        'debit'         => 0,
                        'kredit'        => 0,
                        'total'         => 0,
                        'format_total'  => 0,
                        'tipe'          => 'parent'
            ];

            $total_akun = 0;

            foreach ($x->akun_child as $y) {
                //debit
                $sql_data_debit_penjualan = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $penjualan)
                                                ->where('jenis_jurnal', 'ina')
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();

                $sql_data_debit_pembelian = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('bm', $pembelian)
                                                ->where('jenis_jurnal', 'beli')
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();

                $sql_data_debit_kwitansi = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $kwitansi_)
                                                ->where('jenis_jurnal', 'kwitansi')
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();

                $sql_data_debit_ju = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $jurnal_umum)
                                                ->where('jenis_jurnal', 'ju')
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();

                $sql_data_debit_opname = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $opname)
                                                ->where('jenis_jurnal', 'opname')
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();

                $sql_data_debit_lain = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('jenis_jurnal', ['beli-batal','ina-batal','beli-retur', 'ina-retur', 'ina-dp', 'lr', 'gaji', 'gaji-mingguan','awalan' ])
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();
                
                //kredit
                $sql_data_kredit_penjualan = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $penjualan)
                                                ->where('jenis_jurnal', 'ina')
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();

                $sql_data_kredit_pembelian = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('bm', $pembelian)
                                                ->where('jenis_jurnal', 'beli')
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();

                $sql_data_kredit_kwitansi = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $kwitansi_)
                                                ->where('jenis_jurnal', 'kwitansi')
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();

                $sql_data_kredit_ju = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $jurnal_umum)
                                                ->where('jenis_jurnal', 'ju')
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();

                $sql_data_kredit_opname = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $opname)
                                                ->where('jenis_jurnal', 'opname')
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();

                $sql_data_kredit_lain = DB::table('jurnal')
                                                ->where('no_akun', $y->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('jenis_jurnal', ['beli-batal','ina-batal','beli-retur', 'ina-retur', 'ina-dp', 'lr', 'gaji', 'gaji-mingguan','awalan' ])
                                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                                ->whereNotIn('id', $id_jurnalQ)
                                                ->where('status', null)
                                                ->get();
                                
                $sql_data_debit = $sql_data_debit_penjualan->merge($sql_data_debit_pembelian)
                                                ->merge($sql_data_debit_kwitansi)
                                                ->merge($sql_data_debit_ju)
                                                ->merge($sql_data_debit_opname)
                                                ->merge($sql_data_debit_lain);   
                                                
                $sql_data_kredit = $sql_data_kredit_penjualan->merge($sql_data_kredit_pembelian)
                                                ->merge($sql_data_kredit_kwitansi)
                                                ->merge($sql_data_kredit_ju)
                                                ->merge($sql_data_kredit_opname)
                                                ->merge($sql_data_kredit_lain);   
                                                
                // $sql_data_debit = DB::table('jurnal')
                //                         ->where('no_akun', $y->no_akun)
                //                         ->whereBetween('tgl', [$tgl, $tgl_dua])
                //                         ->whereNotIn('id', $id_jurnalQ)
                //                         ->where('map', 'd')
                //                         ->get();
    
                // $sql_data_kredit = DB::table('jurnal')
                //                         ->where('no_akun', $y->no_akun)
                //                         ->whereBetween('tgl', [$tgl, $tgl_dua])
                //                         ->where('map', 'k')
                //                         ->whereNotIn('id', $id_jurnalQ)
                //                         ->get();

                $debit =  $this->total($sql_data_debit);
                $kredit = $this->total($sql_data_kredit); 
                
                $total = $debit - $kredit;
                $format_total = ($total) < 0 ? '('.str_replace('-', '', number_format($total, 0, ',', '.')).')' : number_format($total, 0, ',', '.');

                $dt_akun[] = (object) [
                        'no_akun'       => $y->no_akun,
                        'akun'          => $y->akun,
                        'debit'         => $debit,
                        'kredit'        => $kredit,
                        'total'         => $total,
                        'format_total'  => $format_total,
                        'tipe'          => 'child'
                ];

                $total_all += $total;
                $total_akun += $total;
            }
            $format_total_akun = ($total_akun) < 0 ? '('.str_replace('-', '', number_format($total_akun, 0, ',', '.')).')' : number_format($total_akun, 0, ',', '.');
            $dt_akun[] = (object) [
                'no_akun'       => '',
                'akun'          => '',
                'debit'         => '',
                'kredit'        => '',
                'total'         => $format_total_akun,
                'format_total'  => '',
                'tipe'          => 'child2'
    ];

        }

        $format_total_all = number_format(abs($total_all), 0, ',', '.');

        $dataQ['data'] = $dt_akun;
        $dataQ['total_all'] = $total_all;
        
        $dataQ['format_total_all'] = $format_total_all;
        // dd($dataQ);
        return response()->json($dataQ);
    }

    public function excel_labarugi($tgl)
    {
        $pecah = explode('&', $tgl);
          $tgl_m = $pecah[0];
            $tgl_a = $pecah[1];  

            $tgl_m_format = date('Y-m-d', strtotime($tgl_m));
            $tgl_a_format = date('Y-m-d', strtotime($tgl_a));

            // AWAL PENDAPATAN 
            $pendapatan = DB::table('akun')
                            ->where('parent_id', 4)
                            ->orWhereNull('parent_id')
                            ->where('kel', 4)
                            ->orderBy('no_akun')
                            ->get();

            $dt_pendapatan = [];
            $total_d_pendapatan = 0;
            $total_k_pendapatan = 0;
            $total_all_pendapatan = 0;
            foreach ($pendapatan as $v) {
                $data_debit_pendapatan = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'd')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
    
                $data_kredit_pendapatan = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'k')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
                    
                $debit_pendapatan =  $this->total($data_debit_pendapatan);
                $kredit_pendapatan = $this->total($data_kredit_pendapatan);
    
                $total_pendapatan = $debit_pendapatan - $kredit_pendapatan;
    
                $total_fix_pendapatan = ($total_pendapatan) < 0 ? '('.str_replace('-', '', $total_pendapatan).')' : $total_pendapatan;
                
                $dt_pendapatan[] = (object) [
                            'no_akun_pendapatan' => $v->no_akun,
                            'akun_pendapatan' => $v->akun,
                            'parent_id_pendapatan' => $v->parent_id,
                            'debit_pendapatan' => $debit_pendapatan,
                            'kredit_pendapatan' => $kredit_pendapatan,
                            'total_pendapatan' => $total_fix_pendapatan,
                ];
    
                $total_d_pendapatan += $debit_pendapatan;
                $total_k_pendapatan += $kredit_pendapatan;
            }
            $total_all_pendapatan = $total_d_pendapatan - ($total_k_pendapatan);
            $total_all_fix_pendapatan = ($total_all_pendapatan) < 0 ? '('.str_replace('-', '', $total_all_pendapatan).')' : $total_all_pendapatan;
            // AKHIR PENDAPATAN

            // AWAL HPP
            $hpp = DB::table('akun')
                            ->where('parent_id', 5)
                            ->orWhereNull('parent_id')
                            ->where('kel', 5)
                            ->orderBy('no_akun')
                            ->get();
            
            $dt_hpp = [];
            $total_d_hpp = 0;
            $total_k_hpp = 0;
            $total_all_hpp = 0;
            foreach ($hpp as $v) {
                $data_debit_hpp = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'd')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
    
                $data_kredit_hpp = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'k')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
                    
                $debit_hpp =  $this->total($data_debit_hpp);
                $kredit_hpp = $this->total($data_kredit_hpp);
    
                $total_hpp = $debit_hpp - $kredit_hpp;
    
                $total_fix_hpp = ($total_hpp) < 0 ? '('.str_replace('-', '', $total_hpp).')' : $total_hpp;
                
                $dt_hpp[] = (object) [
                            'no_akun_hpp' => $v->no_akun,
                            'akun_hpp' => $v->akun,
                            'parent_id_hpp' => $v->parent_id,
                            'debit_hpp' => $debit_hpp,
                            'kredit_hpp' => $kredit_hpp,
                            'total_hpp' => $total_fix_hpp,
                ];
    
                $total_d_hpp += $debit_hpp;
                $total_k_hpp += $kredit_hpp;
            }
            $total_all_hpp = $total_d_hpp - ($total_k_hpp);
            $total_all_fix_hpp = ($total_all_hpp) < 0 ? '('.str_replace('-', '', $total_all_hpp).')' : $total_all_hpp;
            // AKHIR HPP

            // AWAL BEBAN
            $beban = DB::table('akun')
                            ->where('parent_id', 6)
                            ->orWhereNull('parent_id')
                            ->where('kel', 6)
                            ->orderBy('no_akun')
                            ->get();
            
            $dt_beban = [];
            $total_d_beban = 0;
            $total_k_beban = 0;
            $total_all_beban = 0;
            foreach ($beban as $v) {
                $data_debit_beban = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'd')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
    
                $data_kredit_beban = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'k')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
                    
                $debit_beban =  $this->total($data_debit_beban);
                $kredit_beban = $this->total($data_kredit_beban);
    
                $total_beban = $debit_beban - $kredit_beban;
    
                $total_fix_beban = ($total_beban) < 0 ? '('.str_replace('-', '', $total_beban).')' : $total_beban;
                
                $dt_beban[] = (object) [
                            'no_akun_beban' => $v->no_akun,
                            'akun_beban' => $v->akun,
                            'parent_id_beban' => $v->parent_id,
                            'debit_beban' => $debit_beban,
                            'kredit_beban' => $kredit_beban,
                            'total_beban' => $total_fix_beban,
                ];
    
                $total_d_beban += $debit_beban;
                $total_k_beban += $kredit_beban;
            }
            $total_all_beban = $total_d_beban - ($total_k_beban);
            $total_all_fix_beban = ($total_all_beban) < 0 ? '('.str_replace('-', '', $total_all_beban).')' : $total_all_beban;

            // AKHIR BEBAN

            $dt['rekap_pendapatan'] = $dt_pendapatan;
            $dt['tt_debit_pendapatan'] = $total_d_pendapatan;
            $dt['tt_kredit_pendapatan'] = $total_k_pendapatan;
            $dt['tt_fix_pendapatan'] = $total_all_fix_pendapatan;
            $dt['rekap_hpp'] = $dt_hpp;
            $dt['tt_debit_hpp'] = $total_d_hpp;
            $dt['tt_kredit_hpp'] = $total_k_hpp;
            $dt['tt_fix_hpp'] = $total_all_fix_hpp;
            $dt['rekap_beban'] = $dt_beban;
            $dt['tt_debit_beban'] = $total_d_beban;
            $dt['tt_kredit_beban'] = $total_k_beban;
            $dt['tt_fix_beban'] = $total_all_fix_beban;
            $dt['tgl_m'] = $tgl_m;
            $dt['tgl_a'] = $tgl_a;
            $dt['total_akhir_fix'] = $total_all_pendapatan + $total_all_hpp - $total_all_beban;

        $nama_file = "Rekap Laba-Rugi ".$tgl_m."-".$tgl_a.".xlsx";
        return Excel::download(new LabarugiExport($dt), $nama_file); 
    }

    public function pdf_labarugi($tgl)
    {
        $pecah = explode('&', $tgl);
          $tgl_m = $pecah[0];
            $tgl_a = $pecah[1];  

            $tgl_m_format = date('Y-m-d', strtotime($tgl_m));
            $tgl_a_format = date('Y-m-d', strtotime($tgl_a));

            // AWAL PENDAPATAN 
            $pendapatan = DB::table('akun')
                            ->where('parent_id', 4)
                            ->orWhereNull('parent_id')
                            ->where('kel', 4)
                            ->orderBy('no_akun')
                            ->get();

            $dt_pendapatan = [];
            $total_d_pendapatan = 0;
            $total_k_pendapatan = 0;
            $total_all_pendapatan = 0;
            foreach ($pendapatan as $v) {
                $data_debit_pendapatan = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'd')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
    
                $data_kredit_pendapatan = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'k')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
                    
                $debit_pendapatan =  $this->total($data_debit_pendapatan);
                $kredit_pendapatan = $this->total($data_kredit_pendapatan);
    
                $total_pendapatan = $debit_pendapatan - $kredit_pendapatan;
    
                $total_fix_pendapatan = ($total_pendapatan) < 0 ? '('.str_replace('-', '', $total_pendapatan).')' : $total_pendapatan;
                
                $dt_pendapatan[] = (object) [
                            'no_akun_pendapatan' => $v->no_akun,
                            'akun_pendapatan' => $v->akun,
                            'parent_id_pendapatan' => $v->parent_id,
                            'debit_pendapatan' => $debit_pendapatan,
                            'kredit_pendapatan' => $kredit_pendapatan,
                            'total_pendapatan' => $total_fix_pendapatan,
                ];
    
                $total_d_pendapatan += $debit_pendapatan;
                $total_k_pendapatan += $kredit_pendapatan;
            }
            $total_all_pendapatan = $total_d_pendapatan - ($total_k_pendapatan);
            $total_all_fix_pendapatan = ($total_all_pendapatan) < 0 ? '('.str_replace('-', '', $total_all_pendapatan).')' : $total_all_pendapatan;
            // AKHIR PENDAPATAN

            // AWAL HPP
            $hpp = DB::table('akun')
                            ->where('parent_id', 5)
                            ->orWhereNull('parent_id')
                            ->where('kel', 5)
                            ->orderBy('no_akun')
                            ->get();
            
            $dt_hpp = [];
            $total_d_hpp = 0;
            $total_k_hpp = 0;
            $total_all_hpp = 0;
            foreach ($hpp as $v) {
                $data_debit_hpp = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'd')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
    
                $data_kredit_hpp = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'k')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
                    
                $debit_hpp =  $this->total($data_debit_hpp);
                $kredit_hpp = $this->total($data_kredit_hpp);
    
                $total_hpp = $debit_hpp - $kredit_hpp;
    
                $total_fix_hpp = ($total_hpp) < 0 ? '('.str_replace('-', '', $total_hpp).')' : $total_hpp;
                
                $dt_hpp[] = (object) [
                            'no_akun_hpp' => $v->no_akun,
                            'akun_hpp' => $v->akun,
                            'parent_id_hpp' => $v->parent_id,
                            'debit_hpp' => $debit_hpp,
                            'kredit_hpp' => $kredit_hpp,
                            'total_hpp' => $total_fix_hpp,
                ];
    
                $total_d_hpp += $debit_hpp;
                $total_k_hpp += $kredit_hpp;
            }
            $total_all_hpp = $total_d_hpp - ($total_k_hpp);
            $total_all_fix_hpp = ($total_all_hpp) < 0 ? '('.str_replace('-', '', $total_all_hpp).')' : $total_all_hpp;
            // AKHIR HPP

            // AWAL BEBAN
            $beban = DB::table('akun')
                            ->where('parent_id', 6)
                            ->orWhereNull('parent_id')
                            ->where('kel', 6)
                            ->orderBy('no_akun')
                            ->get();
            
            $dt_beban = [];
            $total_d_beban = 0;
            $total_k_beban = 0;
            $total_all_beban = 0;
            foreach ($beban as $v) {
                $data_debit_beban = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'd')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
    
                $data_kredit_beban = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'k')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
                    
                $debit_beban =  $this->total($data_debit_beban);
                $kredit_beban = $this->total($data_kredit_beban);
    
                $total_beban = $debit_beban - $kredit_beban;
    
                $total_fix_beban = ($total_beban) < 0 ? '('.str_replace('-', '', $total_beban).')' : $total_beban;
                
                $dt_beban[] = (object) [
                            'no_akun_beban' => $v->no_akun,
                            'akun_beban' => $v->akun,
                            'parent_id_beban' => $v->parent_id,
                            'debit_beban' => $debit_beban,
                            'kredit_beban' => $kredit_beban,
                            'total_beban' => $total_fix_beban,
                ];
    
                $total_d_beban += $debit_beban;
                $total_k_beban += $kredit_beban;
            }
            $total_all_beban = $total_d_beban - ($total_k_beban);
            $total_all_fix_beban = ($total_all_beban) < 0 ? '('.str_replace('-', '', $total_all_beban).')' : $total_all_beban;

            // AKHIR BEBAN

            $dt['rekap_pendapatan'] = $dt_pendapatan;
            $dt['tt_debit_pendapatan'] = $total_d_pendapatan;
            $dt['tt_kredit_pendapatan'] = $total_k_pendapatan;
            $dt['tt_fix_pendapatan'] = $total_all_fix_pendapatan;
            $dt['rekap_hpp'] = $dt_hpp;
            $dt['tt_debit_hpp'] = $total_d_hpp;
            $dt['tt_kredit_hpp'] = $total_k_hpp;
            $dt['tt_fix_hpp'] = $total_all_fix_hpp;
            $dt['rekap_beban'] = $dt_beban;
            $dt['tt_debit_beban'] = $total_d_beban;
            $dt['tt_kredit_beban'] = $total_k_beban;
            $dt['tt_fix_beban'] = $total_all_fix_beban;
            $dt['tgl_m'] = $tgl_m;
            $dt['tgl_a'] = $tgl_a;
            $dt['total_akhir_fix'] = $total_all_pendapatan + $total_all_hpp - $total_all_beban;

            $nama_file = "Rekap Laba-Rugi ".$tgl_m."-".$tgl_a.".xlsx";
            return PDF::download(new LabarugiExport($dt), $nama_file); 
    }

}