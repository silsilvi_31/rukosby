<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class AbsenController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/jakarta");
    }

    public function index()
    {
        return view('admin.absen.index');
    }

    public function total_claim($tgl, $kodep)
    {
        $total_absen = DB::table('absen')->where('tgl', $tgl)->count('id');
        $total_claim = DB::table('claimabsen')->where('tgl', $tgl)->count('id');

        $dt = [
            'total_absen' => $total_absen,
            'total_claim' => $total_claim
        ];

        return $dt;
    }

    public function datatable()
    {
        $data = DB::table('absen')
                        ->select('kodep', 'tgl', DB::raw('count(*) as total'))
                        ->groupBy('tgl')
                        ->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('total_claim', function ($data)
        {
            $kodep = $data->kodep;
            $tgl = $data->tgl;
            return $this->total_claim($data->tgl,$data->kodep)['total_absen'].' / '.$this->total_claim($data->tgl,$data->kodep)['total_claim'];
        })
        ->addColumn('opsi', function ($data) {
            $edit = route('claimabsen.form_edit', [($data->tgl)]);
            $tgl = "'".$data->tgl."'";
            $total_absen = $this->total_claim($data->tgl,$data->kodep)['total_absen'];
            $total_claim = $this->total_claim($data->tgl,$data->kodep)['total_claim'];
            $status_edit = ($total_absen == $total_claim) ? 'btn-secondary disabled' : 'btn-primary';
            $status_hapus = ($total_absen == $total_claim) ? 'btn-secondary disabled' : 'btn-danger';

            return '<a href="'.$edit.'" class="btn btn-sm '.$status_edit.'"><i class="fa fa-edit"></i><a/>
                    <button type="button" class="btn btn-sm '.$status_hapus.'" onclick="delete_absen('.$tgl.')"><i class="fa fa-trash"></i></button>';
            // return 'opsi';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function get_nama(Request $req)
    {
        $kodep = $req->_kodep;
        $data_kar = DB::table('karyawan')
                        ->where('kode',  $kodep)
                        ->first();
            
        $nama = isset($data_kar) ? $data_kar->nama : null;
        $data['nama'] = $nama;
        return response()->json($data);
    }

    public function form()
    {
        return view('admin.absen.form');
    }

    public function jamkerja($jamhadir, $jampulang)
    {
        $jamkerja=floor(abs(strtotime($jampulang)-strtotime($jamhadir)) / (60 * 60));
		return $jamkerja;
    }

    function lembur($jamkerja) {
        if ($jamkerja>9) { 
            $lembur = $jamkerja - 9;
        } else {
            $lembur = null;
        }
		
		return $lembur;
    }

    public function add_absen(Request $req)
    {
        $id_user = session::get('id_user');
        $tgl = date("Y-m-d", strtotime($req->_tgl));
        $kodep = $req->_kodep;
        $jam_hadir = $req->_jamHadir;
        $jam_pulang = $req->_jamPulang;
        $izin = $req->_izin;
        $potongan = $req->_potongan;
        $ketr = $req->_ketr;

        $jamkerja = $this->jamkerja($jam_hadir, $jam_pulang);
        $lembur = $this->lembur($jamkerja);

        $data_absen = [
                        "tgl" => $tgl,
                        "kodep" => $kodep,
                        "jamhadir" => $jam_hadir,
                        "jampulang" => $jam_pulang,
                        "izin" => $izin,
                        "potongan" => $potongan,
                        "lembur" => $lembur,
                        "ketr" => $ketr,
                        "user_add" => $id_user,
                        "created_at" => date("Y-m-d H:i:s")
                    ];

        $cek_absen = DB::table('absen')
                                ->where('kodep', $kodep)
                                ->where('tgl', $tgl)
                                ->first();

        if (isset($cek_absen)) {
            $res = [
                'code' => 400,
                'msg' => 'Kode Karyawan Telah Terpakai'
            ];
        } else {
            $insert_absen = DB::table('absen')->insert($data_absen);

            if ($insert_absen) {
                $res = [
                        'code' => 300,
                        'msg' => 'Data Berhasil Disimpan'
                ];
            } else {
                $res = [
                        'code' => 400,
                        'msg' => 'Data Gagal Disimpan'
                ];
            }
        }

        return response()->json($res);
    }

    public function tampil_absen(Request $req)
    {
        $tgl = isset($req->_tgl) ? date('Y-m-d', strtotime($req->_tgl)) : null;
        $absen = DB::table('absen as a')
                        ->leftJoin('karyawan as b', 'a.kodep', '=', 'b.kode')
                        ->where('tgl', $tgl)
                        ->select('a.id','a.kodep', 'b.nama', 'a.tgl', 'a.jamhadir', 'a.jampulang', 
                                    'a.izin', 'a.potongan','a.lembur','a.ketr')
                        ->get();

        return response()->json($absen);
    }

    public function form_edit($tgl)
    {
        $data['tgl'] = date('d-m-Y', strtotime($tgl));
        return view('admin.absen.form')->with($data);
    }

    public function get_absen(Request $req)
    {
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $dt = [];

        $absen = DB::table('absen as a')
                                ->leftJoin('karyawan as b', 'a.kodep', '=', 'b.kode')
                                ->where('a.tgl', $tgl)
                                ->get();

        foreach ($absen as $key => $v) {
            $dt[] = [
                        'id' => $v->id,
                        'tgl' => $v->tgl,
                        'kodep' => $v->kodep,
                        'nama' => $v->nama,
                        'jamhadir' => $v->jamhadir,
                        'jampulang' => $v->jampulang,
                        'izin' => $v->izin,
                        'potongan' => $v->potongan,
                        'lembur' => $v->lembur,
                        'ketr' => $v->ketr
                    ];
        }
        // dd($dt);
        return response()->json($dt);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $tgl = date('Y-m-d', strtotime($req->_tgl));

        $res = [];
        $delete = DB::table('absen')->where('tgl', $tgl)->delete();

        if ($delete) {
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function delete_item_absen(Request $req)
    {
        $id = $req->_id;

        $delete = DB::table('absen')->where('id', $id)->delete();

        if($delete) {
            $res = [
                'code' => 300,
                'msg' => 'Berhasil Hapus Item Absen'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal Hapus Item Absen'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }
}
