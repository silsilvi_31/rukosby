<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class AkunController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.master.akun.index');
    }

    public function datatable()
    {
        $data = DB::table('akun')->get();
        // dd($data);
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('kelompok', function () {
            return 'kel';
        })
        ->addColumn('opsi', function ($data) {
            // $edit = route('jabatan.form_edit', [base64_encode($data->id)]);
            // $id_jabatan = "'".base64_encode($data->id)."'";
            return '<a href="#" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_jabatan()"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function akun_list()
    {
        $data = DB::table('akun')->get();
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('kelompok', function () {
            return 'kel';
        })
        ->addColumn('opsi', function ($data) {
            $id_akun = "'".base64_encode($data->id)."'";
            $nama_akun = $data->akun;
            return '<button type="button" class="btn btn-sm btn-danger" onclick="delete_jabatan()"><i class="fa fa-trash"></i></button>
                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form()
    {
        return view('admin.master.akun.form');
    }

    // AKUN BARU

    public function formQ()
    {
        return view('admin.master.akun.formQ');
    }

    public function saveQ(Request $req)
    {
        $id_user = session::get('id_user');
        $no_akun = $req->_noAkun;
        $nama_akun = $req->_namaAkun;
        $parent_id = substr($no_akun, 0,1);

        $data = [
            'no_akun' => $no_akun,
            'akun' => $nama_akun,
            'parent_id' => $parent_id,
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => $id_user
        ];

        $cek_akun = DB::table('akun')->where('no_akun', $no_akun)->first();

        $res = [];
        if (isset($cek_akun)) {
            $res = [
                    "code" => 400,
                    "msg" => "Nomor akun sudah Ada"
            ];
        } else {
            $insert_akun = DB::table('akun')->insert($data);
            if ($insert_akun) {
                $res = [
                    "code" => 300,
                    "msg" => "Data berhasil disimpan"
                ];      
            } else {
                $res = [
                    "code" => 400,
                    "msg" => "Data gagal disimpan"
                ];
            }
        }
        return response()->json($res);
    }

    public function akun_listQ()
    {
        $data = DB::table('akun')->get();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('kelompok', function () {
            return 'kel';
        })
        ->addColumn('opsi', function ($data) {
            $id_akun = "'".base64_encode($data->id)."'";
            $no_akun = "'".$data->no_akun."'";
            $nama_akun = "'".$data->akun."'";
            return '<button class="btn btn-sm btn-primary" onclick="select_akun('.$no_akun.','.$nama_akun.')">Pilih</button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    // public function save(Request $req)
    // {
    //     $id_user = session::get('id_user');
    //     $jabatan = $req->jabatan;
        
    //     $data_jabatan = [
    //         'jabatan' => $jabatan,
    //         'created_at' => date("Y-m-d H:i:s"),
    //         'user_add' => $id_user
    //     ];

    //     $insert_jabatan = DB::table('jabatan')->insert($data_jabatan);
    //     if ($insert_jabatan) {
    //         $res = [
    //             'code' => 200,
    //             'msg' => 'Berhasil disimpan'
    //         ];
    //     }else {
    //         $res = [
    //             'code' => 200,
    //             'msg' => 'Berhasil disimpan'
    //         ];
    //     }

    //     $data['response'] = $res;

    //     return redirect()->route('jabatan.index')->with($data);
    // }
    
    // public function form_edit($id)
    // {
    //     $id_jabatan = base64_decode($id);
    //     $jabatan = DB::table('jabatan')->where('id', $id_jabatan)->first();
        
    //     $data['id_jabatan'] = base64_encode($jabatan->id);
    //     $data['jabatan']    = $jabatan->jabatan;
        
    //     return view('admin.master.jabatan.form_edit')->with($data);
    // }

    // public function update(Request $req)
    // {
    //     $id_user    = session::get('id_user');
    //     $id_jabatan = base64_decode($req->id_jabatan);
    //     $jabatan    = $req->jabatan;

    //     $data_jabatan = [
    //         'jabatan' => $jabatan,
    //         'updated_at' => date("Y-m-d H:i:s"),
    //         'user_upd' => $id_user
    //     ];

    //     $update = DB::table('jabatan')->where('id', $id_jabatan)->update($data_jabatan);
    //     if ($update) {
    //         $res = [
    //             'code' => 201,
    //             'msg' => 'Data telah diupdate'
    //         ];
    //     }else {
    //         $res = [
    //             'code' => 400,
    //             'msg' => 'Gagal diupdate'
    //         ];
    //     }
    //     $data['response'] = $res;
    //     return redirect()->route('jabatan.index')->with($data);
    // }

    // public function delete(Request $req)
    // {
    //     $id_user    = session::get('id_user');
    //     $id_jabatan = base64_decode($req->_idJabatan);

    //     $data_jabatan =[
    //         'updated_at' => date("Y-m-d H:i:s"),
    //         'user_upd' => $id_user,
    //         'status' => 9
    //     ];
    //     $res = [];
    //     $update = DB::table('jabatan')->where('id', $id_jabatan)->update($data_jabatan);
    //     if ($update) {
    //         $res = [
    //             'code' => 300,
    //             'msg' => 'Data telah dihapus'
    //         ];
    //     }else {
    //         $res = [
    //             'code' => 400,
    //             'msg' => 'Gagal dihapus'
    //         ];
    //     }
    //     $data['response'] = $res;
    //     return response()->json($data);
    // }
}