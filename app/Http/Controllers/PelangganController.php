<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class PelangganController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/jakarta");
    }

    public function index()
    {
        return view('admin.master.pelanggan.index');
    }

    public function datatable()
    {
        $data = DB::table('pelanggan')->where('status', NULL)->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $edit = route('pelanggan.form_edit', [base64_encode($data->id)]);
            $id_pelanggan = "'".base64_encode($data->id)."'";
            return '<a href="'.$edit.'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i><a/>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_pelanggan('.$id_pelanggan.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form()
    {
        return view('admin.master.pelanggan.form');
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $nama_pelanggan = $req->nama_pelanggan;
        $alamat = $req->alamat;
        $no_telp = $req->no_telp;
        $email = $req->email;

        $data_pelanggan = [
                            'nama' => $nama_pelanggan,
                            'alamat' => $alamat,
                            'no_telp' => $no_telp,
                            'email' => $email,
                            "created_at" => date("Y-m-d H:i:s"),
                            "user_add" => $id_user
                            ];
        
        $insert_pelanggan = DB::table('pelanggan')->insert($data_pelanggan);
        if($insert_pelanggan) {
            $res = [
                'code' => 300,
                'msg' => 'Pelanggan Berhasil disimpan'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Pelanggan Gagal disimpan'
            ];
        }

        $data['response'] = $res;
        return redirect()->route('pelanggan.index')->with($data);
    }

    public function form_edit($id)
    {
        $id_pelanggan = base64_decode($id);
        $pelanggan = DB::table('pelanggan')->where('id', $id_pelanggan)->where('status', NULL)->first();

        $data['id_pelanggan'] = $pelanggan->id;
        $data['nama'] = $pelanggan->nama;
        $data['alamat'] = $pelanggan->alamat;
        $data['no_telp'] = $pelanggan->no_telp;
        $data['email'] = $pelanggan->email;

        return view('admin.master.pelanggan.form_edit')->with($data);
    }

    public function update(Request $req)
    {
        $id_user = session::get('id_user');
        $id_pelanggan = $req->id_pelanggan;
        $nama_pelanggan = $req->nama_pelanggan;
        $alamat = $req->alamat;
        $no_telp = $req->no_telp;
        $email = $req->email;

        $data_pelanggan = [
                            'nama' => $nama_pelanggan,
                            'alamat' => $alamat,
                            'no_telp' => $no_telp,
                            'email' => $email,
                            "updated_at" => date("Y-m-d H:i:s"),
                            "user_upd" => $id_user
                            ];

        

        $update = DB::table('pelanggan')->where('id', $id_pelanggan)->update($data_pelanggan);
        if ($update) {
            $res = [
                'code' => 201,
                'msg' => 'Data telah diupdate'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal diupdate'
            ];
        }
        $data['response'] = $res;
        return redirect()->route('pelanggan.index')->with($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id_pelanggan = base64_decode($req->_idPelanggan);

        $data_pelanggan = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];

        $res = [];
        $update = DB::table('pelanggan')->where('id', $id_pelanggan)->update($data_pelanggan);

        if ($update) {
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }
}
