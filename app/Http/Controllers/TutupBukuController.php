<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use Exception;
use App\Exports\NeracaExport;

class TutupBukuController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                        ->where('status', 'tutup')
                                        ->orderBy('created_at', 'DESC')
                                        ->first();
        
        $tgl_awalan = isset($parent_jurnal) ? date('d-m-Y', strtotime('+1 days', strtotime($parent_jurnal->tgl_akhir))) : '';

        $data['tgl_awalan'] = $tgl_awalan;
        return view('admin.tutupBuku.index')->with($data);
    }

    public function total($data)
    {
        $total = 0;
        foreach ($data as $value) {
            $total += $value->total;            
        }
        return $total;
    }

    public function no_urut()
    {
        $id_jurnalUmum = DB::table('jurnal')->where('jenis_jurnal', 'ju')->where('status',NULL)->max('ref');
        $no = $id_jurnalUmum;
        $no++;
        return response()->json($no);
    }

    public function set_akun_labarugi($tgl_akhir, $labarugi, $simpan_id)
    {
        $ref_labarugi = DB::table('jurnal')
                            ->where('jenis_jurnal', 'lr')
                            ->where('status',NULL)
                            ->max('ref');

        $no = isset($ref_labarugi) ? $ref_labarugi+1 : 1;
        
        $akun[0]['tgl'] = $tgl_akhir;
        $akun[0]['id_item'] = null;
        $akun[0]['no_akun'] = '330';
        $akun[0]['jenis_jurnal'] = 'lr';
        $akun[0]['ref'] = $simpan_id;
        $akun[0]['nama'] = null;
        $akun[0]['keterangan'] = null;
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = null;
        $akun[0]['grup'] = 1;
        $akun[0]['qty'] = null;
        $akun[0]['harga'] = $labarugi;
        $akun[0]['total'] = $labarugi;

        $akun[1]['tgl'] = $tgl_akhir;
        $akun[1]['id_item'] = null;
        $akun[1]['no_akun'] = '410';
        $akun[1]['jenis_jurnal'] = 'lr';
        $akun[1]['ref'] = $simpan_id;
        $akun[1]['nama'] = null;
        $akun[1]['keterangan'] = null;
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = null;
        $akun[1]['grup'] = 2;
        $akun[1]['qty'] = null;
        $akun[1]['harga'] =  $labarugi;
        $akun[1]['total'] = $labarugi;

        // dd($akun);

        $insert_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function simpan_tutupbuku(Request $req)
    {
        $id_user = session::get('id_user');
        $tgl_m_format = date('Y-m-d', strtotime($req->_tgl));
        $tgl_a_format = date('Y-m-d', strtotime($req->_tglDua));

        $labarugi = DB::table('akun')
                            ->whereIn('parent_id', [4, 5, 6])
                            ->orWhereNull('parent_id')
                            ->whereIn('kel', [4, 5, 6])
                            ->orderBy('no_akun')
                            ->get();

        $dt_labarugi = [];
        $total_d_labarugi = 0;
        $total_k_labarugi = 0;
        $total_all_labarugi = 0;

        foreach ($labarugi as $v) {
            $data_debit_labarugi = DB::table('jurnal')
                            ->where('no_akun', $v->no_akun)
                            ->where('map', 'd')
                            ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                            ->get();

            $data_kredit_labarugi = DB::table('jurnal')
                            ->where('no_akun', $v->no_akun)
                            ->where('map', 'k')
                            ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                            ->get();

            $debit_labarugi =  $this->total($data_debit_labarugi);
            $kredit_labarugi = $this->total($data_kredit_labarugi);

            $total_labarugi = $debit_labarugi - $kredit_labarugi;

            $dt_labarugi[] = (object) [
                    'no_akun_labarugi' => $v->no_akun,
                    'akun_labarugi' => $v->akun,
                    'parent_id_labarugi' => $v->parent_id,
                    'debit_labarugi' => $debit_labarugi,
                    'kredit_labarugi' => $kredit_labarugi,
            ];

            $total_d_labarugi += $debit_labarugi;
            $total_k_labarugi += $kredit_labarugi;
        }
        
        $total_all_labarugi = $total_d_labarugi - ($total_k_labarugi);

        DB::beginTransaction();

        try {
            $parent_jurnal = DB::table('parent_jurnal')->where('status', 'buka')->first();

            $get_id = $parent_jurnal->id;
            $get_tgl_m = strtotime($parent_jurnal->tgl_awal);
            $get_tgl_a = strtotime($tgl_a_format);

            if ($get_tgl_m <= $get_tgl_a) {
                $update_status = DB::table('parent_jurnal')
                                                ->where('status', 'buka')
                                                ->update([
                                                    'labarugi' => $total_all_labarugi,
                                                    'tgl_akhir' => $tgl_a_format,
                                                    'status' => 'tutup',
                                                    'created_at' => date('Y-m-d H:i:s'),
                                                    'user_add' => $id_user
                                                ]);

                $this->set_akun_labarugi($tgl_a_format, abs($total_all_labarugi), $get_id);

                DB::commit();
                $res = [
                    'code' => 200,
                    'msg' => 'Berhasil Disimpan'
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Tanggal Tidak Sesuai'
                ];
            }

           
        } catch (Exception $th) {
            DB::rollback();
            $res = [
                'code' => 400,
                'msg' => $th->getMessage()
            ];
        }
        
        $data['response'] = $res;
        return response()->json($data);
    }

    public function datatable_tutupbuku()
    {
        $data = DB::table('parent_jurnal')->get();

        return datatables::of($data)
        ->editColumn('labarugi', function ($data) {
            $labarugi = $data->labarugi;
            $format_labarugi = ($data->labarugi) < 0 ? str_replace('-', '', number_format($data->labarugi, 0, ',', '.')) : number_format($data->labarugi, 0, ',', '.');
            return ($labarugi < 0 ) ? $format_labarugi.' <span class="badge badge-success">Laba</span>' : $format_labarugi.' <span class="badge bg-warning">Rugi</span>';
        })
        ->addColumn('opsi', function ($data) {
            // $status = ($data->status == 'tutup') ? 'btn-secondary disabled' : 'btn-primary';
            return '<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal_tutupbuku" data-id="'.$data->id.'">Tutup Buku</button>';
        })
        ->rawColumns(['opsi','labarugi'])
        ->make(true);
    }

    public function bukabuku()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                        ->where('status', 'tutup')
                                        ->orderBy('created_at', 'DESC')
                                        ->first();

        $tgl_awalan = isset($parent_jurnal) ? date('d-m-Y', strtotime('+1 days', strtotime($parent_jurnal->tgl_akhir))) : date('d-m-Y');
        
        $data['tgl_awalan'] = $tgl_awalan;

        return view('admin.tutupBuku.bukabuku')->with($data);
    }

    public function datatable_bukabuku()
    {
        $data = DB::table('parent_jurnal')->where('status', 'buka')->get();

        return datatables::of($data)
        ->editColumn('labarugi', function ($data) {
            $labarugi = $data->labarugi;
            return ($labarugi < 0 ) ? $labarugi.' <span class="badge bg-success">Laba</span>' : $labarugi.' <span class="badge bg-warning">Rugi</span>';
        })
        ->rawColumns(['labarugi'])
        ->make(true);
    }

    public function get_awalan($tgl_buka)
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_awal = isset($parent_jurnal) ? $parent_jurnal->tgl_awal : NULL;
        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? $parent_jurnal->tgl_akhir : NULL;
        

        $dt_global = [];
        // MULAI AWALAN
        $awalan = DB::table('akun')
                ->whereIn('parent_id', [1, 2, 3])
                ->orderBy('no_akun')
                ->get();

        $dt_awalan = [];
        $total_d_awalan = 0;
        $total_k_awalan = 0;
        $total_all_awalan = 0;
        $tt_awalan = 0;
        
        foreach ($awalan as $v) {
            $data_debit_awalan = DB::table('jurnal')
                            ->where('no_akun', $v->no_akun)
                            ->where('map', 'd')
                            ->whereBetween('tgl', [$tgl_awal, $tgl_akhir])
                            ->get();

            $data_kredit_awalan = DB::table('jurnal')
                            ->where('no_akun', $v->no_akun)
                            ->where('map', 'k')
                            ->whereBetween('tgl', [$tgl_awal, $tgl_akhir])
                            ->get();

            $debit_awalan =  $this->total($data_debit_awalan);
            $kredit_awalan = $this->total($data_kredit_awalan);

            $total_awalan = $debit_awalan - $kredit_awalan;

            $dt_awalan[] = (object) [
                    'no_akun_awalan' => $v->no_akun,
                    'akun_awalan' => $v->akun,
                    'parent_id_awalan' => $v->parent_id,
                    'debit_awalan' => $debit_awalan,
                    'kredit_awalan' => $kredit_awalan,
            ];

            $total_d_awalan += $debit_awalan;
            $total_k_awalan += $kredit_awalan;
            
            $tt_awalan = $total_d_awalan - $total_k_awalan;
            $dt_global[] = [
                'tgl' => $tgl_buka,
                'no_akun' => $v->no_akun,
                'map' => 'd',
                'jenis_jurnal' => 'awalan',
                'total' => abs($total_awalan)
            ];
        }
        // AKHIR AWALAN

        return $dt_global;

    }

    public function simpan_bukabuku(Request $req)
    {
        $id_user = session::get('id_user');
        $tgl_m_format = date('Y-m-d', strtotime($req->_tgl));
        // $tgl_a_format = date('Y-m-d', strtotime($req->_tglDua));

        $jurnalQ = $this->get_awalan($tgl_m_format);

        $parent_jurnal = DB::table('parent_jurnal')
                                        ->where('status', 'buka')
                                        ->orderBy('created_at', 'DESC')
                                        ->first();

        DB::beginTransaction();

        try {
            if (!isset($parent_jurnal)) {
                $simpan_bukabuku = DB::table('parent_jurnal')
                                ->insert([
                                    'tgl_awal' => $tgl_m_format,
                                    'tgl_akhir' => NULL,
                                    'labarugi' => NULL,
                                    'status' => 'buka',
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'user_add' => $id_user
                                ]);

                $insert_jurnal = DB::table('jurnal')->insert($jurnalQ);

                DB::commit();
                $res = [
                    'code' => 200,
                    'msg' => 'Berhasil Disimpan'
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Gagal Simpan, <br>Pastikan Anda sudah Tutup buku sebelumnya'
                ]; 
            }
        } catch (\Throwable $th) {
            DB::rollback();
            $res = [
                'code' => 400,
                'msg' => $th->getMessage()
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

}