<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class CekJurnalUmumController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.CekJurnalUmum.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function datatable()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';

        $jurnalUmum = DB::table('jurnal')
                            ->whereDate('tgl', '<', $tgl_akhir)
                            ->where('jenis_jurnal', 'ju')
                            ->get();

        $no_ju = [];

        foreach ($jurnalUmum as $value) {
            $no_ju[] = $value->ref;
        }

        $data = DB::table('jurnal_umum')
                        ->whereNotIn('id', $no_ju)
                        ->orderBy('tgl', 'DESC')
                        // ->where('jenis_jurnal', 'ju')
                        ->get(); 

        return Datatables::of($data)
        ->editColumn('is_cek_jurnal', function ($data) {
            $is_cek_jurnal = $data->is_cek_jurnal;
            $nama = isset($is_cek_jurnal) ? $this->get_karyawan($data->cek_jurnal) : '-';
            
            if ($is_cek_jurnal == 1) {
                return '<i class="fa fa-check"></i>'.$nama;
            } else if ($is_cek_jurnal == 2) {
                return '<i class="fa fa-close"></i>'.$nama;
            } else if ($is_cek_jurnal == '') {
                return '<i class="fa fa-minus">';
            } else {
            
            }
        }) 
        ->addColumn('opsi', function ($data){
            $no_ju = $data->id_ju;
            $tgl = date("d-m-Y", strtotime($data->tgl));
            $nama = $data->nama;
            $total = $data->total;
            $cek_jurnal = ($data->is_cek_jurnal == 1) ? 'acc' : 'tidak acc';

            return '<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal_cekju" data-form="cek_ju" data-status="'.$cek_jurnal.'" data-id="'.$no_ju.'" data-tgl="'.$tgl.'" data-nama="'.$nama.'" data-total="'.$total.'">Cek Jurnal</button>';
        })
        ->rawColumns(['is_cek_jurnal', 'opsi'])
        ->make(true);
    }

    public function datatable_ju(Request $req)
    {
        $no_ju = $req->_noJu;

        $jurnal = DB::table('jurnal')->where('ref', $no_ju)->where('jenis_jurnal', 'ju')->get();

        return Datatables::of($jurnal)
        ->addIndexColumn()
        ->make(true);
    }

    public function acc_ju(Request $req)
    {
        $id_users = session::get('id_user');
        $no_ju = $req->_noJu;
        $btn = $req->_btn;
        $status = $req->_status;
        
        $data_acc_ju = [
            'is_cek_jurnal' => 1,
            'cek_jurnal' => $id_users,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $data_batal_ju = [
            'is_cek_jurnal' => 2,
            'cek_jurnal' => $id_users,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];
        
        if($status != 'acc') {
            if ($btn == 'setuju') {
                $update_ju = DB::table('jurnal_umum')
                                ->where('id_ju', $no_ju)
                                ->update($data_acc_ju);
                                
                if ($update_ju) {
                    $res = [
                        'code' => 201,
                        'msg' => 'Jurnal Umum Berhasil Di ACC'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Jurnal Umum gagal di ACC'
                    ];
                }
            } else if ($btn == 'batal') {
                $update_ju = DB::table('jurnal_umum')
                            ->where('id_ju', $no_ju)
                            ->update($data_batal_ju);
                            
                if ($update_ju) {
                    $res = [
                        'code' => 300,
                        'msg' => 'Jurnal Umum Batal di Acc'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Jurnal Umum gagal untuk dibatalkan'
                    ];
                }
            }
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Jurnal Umum sudah di ACC'
            ];
        }
        return response()->json($res);
    }
}
