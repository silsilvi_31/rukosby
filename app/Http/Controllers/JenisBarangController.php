<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class JenisBarangController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");    
    }

    public function index ()
    {
        return view('admin.master.jenisbarang.index');
    }
    
    public function datatable()
    {
        $jenis_barang = DB::table('jenis_barang')->where('status',NULL)->get();
        return Datatables::of($jenis_barang)
        ->addIndexColumn()
        ->addcolumn('opsi', function ($jenis_barang){
            $id = $jenis_barang->id;
            $edit = route('jenisbarang.form_edit', [base64_encode($id)]);
            return '<a href="'.$edit.'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_jenis_barang('.$id.')"><i class="fa fa-trash"></i></button>';
                    
        })
        ->rawcolumns(['opsi'])
        ->make(true);
    }
    
    public function form()
    {
        return view('admin.master.jenisbarang.form');
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $jenis_barang = $req->jenisbarang;
        $data_jenis_barang = [
                              'jenis_brg' => $jenis_barang,
                              'created_at' => date("Y-m-d H:i:s"),
                              'user_add' => $id_user
                            ];
        $insert_jenis_barang = DB::table('jenis_barang')->insert($data_jenis_barang);
       if ($insert_jenis_barang) {
           $res =[
               'code' => 200,
               'msg' => 'Berhasil Disimpan'
           ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal disimpan'
            ];
       }
       $data['response'] = $res;
       return redirect()->route('jenisbarang.index')->with($data);
    }

    public function form_edit($id)
    {
        $id_jenis_barang = base64_decode($id);
        $jenis_barang = DB::table('jenis_barang')->where('id',$id_jenis_barang)->first();
        // dd($jenis_barang);
        $data['id_jenis_barang'] = base64_encode($jenis_barang->id);
        $data['jenis_barang'] = $jenis_barang->jenis_brg;
        return view('admin.master.jenisbarang.form_edit')->with($data);
    }

    public function update(Request $req)
    {
        $id_user = session::get('id_user');
        $id_jenis_barang = base64_decode($req->id_jenis_barang);
        $jenis_barang = $req->jenis_barang;
        
        $data_jenis_barang = [
            'jenis_brg' => $jenis_barang,
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user
        ];
        $update = DB::table('jenis_barang')->where('id', $id_jenis_barang)->update($data_jenis_barang);
        if ($update) {
            $res = [
                'code' => 201,
                'msg' => 'Data telah diupdate'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal diupdate'
            ];
        }
        $data['response'] = $res;
        return redirect()->route('jenisbarang.index')->with($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id_jenis_barang= $req->_idJenisbarang;
        $data_jenis_barang =[
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];

        $res = [];
        $update = DB::table('jenis_barang')->where('id',$id_jenis_barang)->update($data_jenis_barang);
        if ($update){
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        }else {
            $res =[
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;     
        return response()->json($data);
    }
}

