<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class KwitansiController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/jakarta");
    }

    public function no_urut()
    {
        $no_kwi = DB::table('kwitansi')->max('no_kwi');
        $no = $no_kwi;
        $no++;
        return response()->json($no);
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function index()
    {
        return view('admin.kwitansi.index');
    }

    public function datatable()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';

        $kwitansi = DB::table('kwitansi')
                            ->whereDate('tgl', '<', $tgl_akhir)
                            ->get();

        $no_kwi = [];

        foreach ($kwitansi as $value) {
            $no_kwi[] = $value->no_kwi;
        }

        $data = DB::table('kwitansi')
                            ->whereNotIn('no_kwi', $no_kwi)
                            ->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->editColumn('total', function ($data){
            $total = number_format($data->total, 0,',','.');
            return $total;
        })
        ->editColumn('is_cek_kwi', function ($data) {
            $is_cek_kwi = $data->is_cek_kwi;
            $nama = isset($is_cek_kwi) ? $this->get_karyawan($data->cek_kwi) : '-';
            
            if ($is_cek_kwi == 1) {
                return '<center><i class="fa fa-check"></i>'.$nama.'</center>';
            } else if ($is_cek_kwi == 2) {
                return '<center><i class="fa fa-close"></i>'.$nama.'</center>';
            } else if ($is_cek_kwi == '') {
                return '<center><i class="fa fa-minus"></center>';
            } else {
            
            }
        }) 
        ->addColumn('opsi', function ($data) {
            $edit = route('kwitansi.form_edit', [base64_encode($data->no_kwi)]);
            $no_kwi = "'".base64_encode($data->no_kwi)."'";
            $status = ($data->is_cek_kwi == 1 ) ? 'btn-success' : 'btn-secondary disabled';
            $status_edit = ($data->is_cek_kwi != 1 ) ? 'btn-primary' : 'btn-secondary disabled';
            $status_hapus = ($data->is_cek_kwi != 1) ? 'btn-danger' : 'btn-secondary disabled';
            $status_bayar = ( !isset($data->is_cek_kwi) )? 'none' : '';

            return '<a href="'.$edit.'" class="btn btn-sm '.$status_edit.'"><i class="fa fa-edit"></i><a/>
                    <button type="button" class="btn btn-sm '.$status_hapus.'" onclick="delete_kwitansi('.$no_kwi.')"><i class="fa fa-trash"></i></button>
                    <a href="#" id="'.$no_kwi.'" class="print_kwi btn btn-sm '.$status.'"><i class="fa fa-print"></i></a>';
                    // <button type="button" class="btn btn-sm btn-info" style="display:'.$status_bayar.'" data-toggle="modal" data-target="#modal_bayar" data-id="'.$data->no_kwi.'" data-total="'.$data->total.'">Bayar</button>';
        })
        ->rawColumns(['opsi', 'is_cek_kwi'])
        ->make(true);
    }

    public function form()
    {
        $satuan = DB::table('satuan')->get();
        $jenis_brg = DB::table('jenis_barang')->get();
        $akun = DB::table('akun')->get();
        $suplier = DB::table('suplier')->get();

        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_brg;
        $data['akun'] = $akun;
        $data['suplier'] = $suplier;
        return view('admin.kwitansi.form')->with($data);
    }

    public function get_ket($id_beli)
    {
        $detail_beli = DB::table('beli_detail as a')
                                ->leftJoin('satuan as b', 'b.id', '=', 'a.id_satuan')
                                ->where('id_detail_beli', $id_beli)->get();

        $dt = [];
        foreach ($detail_beli as $value) {
            $dt[] = $value->nama_brg.' @'.$value->nama.' '.$value->ketr;
        }

        $hasil = implode(', ', $dt);
        return $hasil;
    }

    public function get_total($id_beli)
    {
        $total_d = DB::table('jurnal')->where('bm',$id_beli)
                            ->where('map', 'd')
                            ->where('jenis_jurnal', 'bl')
                            ->sum('total');
        
        return $total_d;
    }

    public function set_akun($kwitansi)
    {
        $akun[0]['tgl'] = $kwitansi['tgl'];
        $akun[0]['id_item'] = null;
        $akun[0]['no_akun'] = $kwitansi['akun_debit'];
        $akun[0]['jenis_jurnal'] = 'kwitansi';
        $akun[0]['ref'] = strtolower($kwitansi['no_kwi']);
        $akun[0]['nama'] = $kwitansi['penerima'];
        $akun[0]['keterangan'] = $kwitansi['ketr'];
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = null;
        $akun[0]['grup'] = 1;
        $akun[0]['qty'] = null;
        $akun[0]['harga'] = $kwitansi['total'];
        $akun[0]['total'] = $kwitansi['total'];

        $akun[1]['tgl'] = $kwitansi['tgl'];
        $akun[1]['id_item'] = null;
        $akun[1]['no_akun'] = $kwitansi['akun_kredit'];
        $akun[1]['jenis_jurnal'] = 'kwitansi';
        $akun[1]['ref'] = strtolower($kwitansi['no_kwi']);
        $akun[1]['nama'] = $kwitansi['penerima'];
        $akun[1]['keterangan'] = $kwitansi['ketr'];
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = null;
        $akun[1]['grup'] = 2;
        $akun[1]['qty'] = null;
        $akun[1]['harga'] =  $kwitansi['total'];
        $akun[1]['total'] = $kwitansi['total'];

        $insert_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function update_jurnal($kwitansi)
    {
        $jurnal = DB::table('jurnal')->where('ref', $kwitansi['no_kwi'])->get();
        dd($jurnal);
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $no_kwi = $req->_noKwi;
        $tgl = $req->_tgl;
        $dari = $req->_dari;
        $penerima = $req->_penerima;
        $ketr = $req->_ketr;
        $jumlah = $req->_jumlah;
        $akun_debit = $req->_akunDebit;
        $akun_kredit = $req->_akunKredit;
        $jenis_kwitansi = $req->_jenisKwitansi;

        $kwitansi = [
                        'no_kwi' => $no_kwi,
                        'jenis_kwitansi' => $jenis_kwitansi,
                        'tgl' => date('Y-m-d', strtotime($tgl)),
                        'dari' => $dari,
                        'penerima' => $penerima,
                        'ketr' => $ketr,
                        'total' => $jumlah,
                        'akun_debit' => $akun_debit,
                        'akun_kredit' => $akun_kredit,
                        "created_at" => date("Y-m-d H:i:s"),
                        "user_add" => $id_user
                    ];

        $cek_kwi = DB::table('kwitansi')->where('no_kwi', $no_kwi)->first();

        $parent_jurnal = Db::table('parent_jurnal')
                                        ->where('status', 'tutup')
                                        ->orderBy('created_at', 'DESC')
                                        ->first();

        $tgl_int = strtotime($tgl);
        $tgl_akhir_int = isset($parent_jurnal) ? strtotime($parent_jurnal->tgl_akhir) : NULL;


        if (!$tgl || !$dari || !$penerima) {
            $msg = [];
            $res = [
                'code' => 400,
                'msg' => 'Data Belum Lengkap'
            ];
            Session::put('code',400);
            Session::put('msg','Data Belum Lengkap');
        } elseif($tgl_int <= $tgl_akhir_int) {
            $res = [
                'code' => 400,
                'msg' => 'Tgl tidak sesuai'
            ];
            Session::put('code',400);
            Session::put('msg','Tgl tidak sesuai');
        } else {
            if (is_null($cek_kwi)) {
                 $insert_kwi = DB::table('kwitansi')->insertGetId($kwitansi);
                 $this->set_akun($kwitansi);

                 if($insert_kwi) {
                    $res = [
                        'code' => 300,
                        'msg' => 'Data Berhasil disimpan'
                    ];
                    Session::put('code',300);
                    Session::put('msg','Data Berhasil disimpan');
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Data Gagal disimpan'
                    ];
                    Session::put('code',400);
                    Session::put('msg','Data Gagal disimpan');
                }
            } else {
                    $update_kwi =  DB::table('kwitansi')->where('no_kwi', $no_kwi)->update($kwitansi);
                    $delete_jurnal = DB::table('jurnal')->where('ref', $no_kwi)->delete();
                    $this->set_akun($kwitansi);

                    if ($update_kwi) {
                        $res = [
                            'code' => 201,
                            'msg' => 'Data Berhasil Diupdate',
                        ];
                        Session::put('code',201);
                        Session::put('msg','Data Berhasil Diupdate');
                    }else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Data update SJ !'
                        ];
                        Session::put('code',400);
                        Session::put('msg','Data update SJ !');
                    }
                }
            }
        return response()->json($res);
    }

    public function simpan_bayar(Request $req)
    {
        $id_user = session::get('id_user');
        $id_beli = $req->_idBeli;
        $total_bayar = $req->_totalBayar;
        $tanggal = $req->_tanggal;
        $pembayaran = $req->_pembayaran;

        $dtt = [
            'tgl_bayar' => date("Y-m-d H:i:s", strtotime($tanggal)),
            'pembayaran' => $pembayaran,
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
        ];

        $beli = DB::table('beli as a')
                            ->leftJoin('suplier as b', 'a.suplier', '=', 'b.id')
                            ->where('a.id_beli', $id_beli)
                            ->first();

        $suplier = $beli->nama;

        $update = DB::table('beli')->where('id_beli', $id_beli)->update($dtt);

        if ($update) {
            $this->set_akun_bayar($id_beli, $total_bayar, $suplier, $dtt);
            $res = [
                'code' => 201,
                'msg' => 'Tanda Terima Berhasil Disimpan',
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Tanda Terima Gagal Disimpan',
            ];
        }
        return response()->json($res);

    }

    public function set_akun_bayar($id, $total_bayar, $suplier, $dtt)
    {
        // kas/bank
        $akun[0]['tgl'] = $dtt['tgl_bayar'];
        $akun[0]['id_item'] = NULL;
        $akun[0]['ref'] = $id;
        $akun[0]['hpp'] = NULL;
        $akun[0]['grup'] = 3;
        $akun[0]['jenis_jurnal'] = 'sj';
        $akun[0]['nama'] = $suplier; 
        $akun[0]['no_akun'] = '210';
        $akun[0]['keterangan'] = null;
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = '';
        $akun[0]['qty'] = NULL;
        $akun[0]['harga'] = $total_bayar;
        $akun[0]['total'] = $total_bayar;
        
        // piutang
        $akun[1]['tgl'] = $dtt['tgl_bayar'];
        $akun[1]['id_item'] = null;
        $akun[1]['ref'] =  $id;
        $akun[1]['hpp'] = 1;
        $akun[1]['grup'] = 3;
        $akun[1]['jenis_jurnal'] = 'sj';
        $akun[1]['nama'] = $suplier;
        $akun[1]['no_akun'] = $dtt['pembayaran'] == 'Transfer' ? 120 : 110;
        $akun[1]['keterangan'] = '';
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = 'b';
        $akun[1]['qty'] = null;
        $akun[1]['harga'] = $total_bayar;
        $akun[1]['total'] = $total_bayar;

        $simpan_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function hapus_jurnal_item($id_beli)
    {
        $update_jurnal = DB::table('jurnal')
                                ->where('bm', $id_beli)
                                ->where('jenis_jurnal', 'bl')
                                ->where('map', 'k')
                                ->where('status', NULL)
                                ->update([
                                    'keterangan' => $this->get_ket($id_beli),
                                    'harga' => $this->get_total($id_beli),
                                    'total' => $this->get_total($id_beli)
                                ]);
    }

    public function delete_item(Request $req) 
    {
        $id_user = session::get('id_user');
        $id = $req->_idItem;

        $beli_detail = DB::table('beli_detail')->where('id', $id)->first();
        $id_beli = $beli_detail->id_detail_beli;

        $data_beli_detail = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];
        $res = [];

        $delete_beli_detail = DB::table('beli_detail')->where('id', $id)->delete();
        if($delete_beli_detail) {
            $delete_item_jurnal = DB::table('jurnal')->where('id_item', $id)->delete();
            $jumlah = DB::table('beli_detail')->where('id_detail_beli',$id_beli)->where('status',NULL)->sum('subtotal');
            $update_total_detail_beli = DB::table('beli')->where('id_beli', $id_beli)->update([
                'total' => $jumlah
            ]);
            $this->hapus_jurnal_item($id_beli);
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function add_brg(Request $req)
    {
        $id_user = session::get('id_user');

        $nama_brg = $req->_namaBrg;
        $harga = $req->_harga;
        $jenis_brg = $req->_jenisBrg;
        $id_satuan = $req->_idSatuan;
        $no_akun = $req->_noAkun;

        $data_barang = [
            'nama_brg' => $nama_brg,
            'harga' => $harga,
            'jenis_brg' => $jenis_brg,
            'satuan' => $id_satuan,
            'no_akun' => $no_akun
        ];

        $return_id = DB::table('barang')->insertGetId($data_barang);

        $help_a = substr($no_akun, 0, 1);
        $help_b = substr($no_akun, 3, 1);

        $data_setAkun = [
            'kode' => $no_akun,
            'id_set' => $return_id,
            'help_a' => $help_a,
            'help_b' => $help_b,
            'help_c' => 'bl'
        ];

        if(isset($return_id)){
            // $insert_set_akun = DB::table('set_akun')->insert($data_setAkun);
            $res = [
                'code' => 200,
                'msg' => 'Barang berhasil ditambahkan',
                'kode' => $return_id,
                'nama_brg' => $nama_brg,
                'harga' => $harga,
                'jenis_brg' => $jenis_brg,
                'id_satuan' => $id_satuan,
                'no_akun' => $no_akun,
                'created_at' => date("Y-m-d H:i:s"),
                'user_add' => $id_user

            ];
        }else{
            $res = [
                'code' => 400,
                'msg' => 'Kepada Gagal ditambahkan !',
                'kode' => NULL,
                'nama_brg' => NULL,
                'harga' => NULL,      
                'jenis_brg' => NULL,
                'id_satuan' => NULL,
                'no_akun' => NULL,
                'created_at' => NULL,
                'user_add' => NULL
            ];
        }
        return response()->json($res);
    }

    public function form_edit($no_kwi)
    {
        $no_kwi = base64_decode($no_kwi);
        $kwitansi = DB::table('kwitansi')
                        ->where('no_kwi', $no_kwi)
                        ->first();

        $satuan = DB::table('satuan')->get();
        $jenis_brg = DB::table('jenis_barang')->get();
        $akun = DB::table('akun')->get();

        $suplier = DB::table('suplier')->get();

        $data['no_kwi'] = $kwitansi->no_kwi;
        $data['tgl'] = date('d-m-Y', strtotime($kwitansi->tgl));
        $data['dari'] = $kwitansi->dari;
        $data['penerima'] = $kwitansi->penerima;
        $data['ketr'] = $kwitansi->ketr;
        $data['jumlah'] = $kwitansi->total;
        $data['akun'] = $akun;
        $data['akun_debit'] = $kwitansi->akun_debit;
        $data['akun_kredit'] = $kwitansi->akun_kredit;
        $data['jenis_kwitansi'] = $kwitansi->jenis_kwitansi;
        $data['form'] = 'edit';
        // dd($data);
        return view('admin.kwitansi.form')->with($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $no_kwi = base64_decode($req->_noKwi);

        $res = [];

        $delete = DB::table('kwitansi')->where('no_kwi', $no_kwi)->delete();

        if($delete) {
            $delete_jurnal = DB::table('jurnal')->where('ref', $no_kwi)->delete();
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    public function print($no_kwi)
    {
        $no_kwitansi = base64_decode($no_kwi);
        $kwitansi = DB::table('kwitansi')
                            ->where('no_kwi', $no_kwitansi)
                            ->first();

        //Inisialisasi 
        $data['no_kwi'] = $kwitansi->no_kwi; 
        $data['tgl'] = $kwitansi->tgl;
        $data['dari'] = $kwitansi->dari;
        $data['penerima'] = $kwitansi->penerima;
        $data['ketr'] = $kwitansi->ketr;
        $data['total'] = $kwitansi->total;
        $data['pembuat'] = $this->get_karyawan($kwitansi->user_add);

        return view('admin.kwitansi.print')->with($data);

    }

    public function print_kwi_count(Request $req)
    {
        $no_kwi = $req->_noKwi;
        $jenis = $req->_jenis;

        $update_print = DB::table('kwitansi')->where('no_kwi', $no_kwi)->update(['print_kwi' => 1]);
        
        $res = [];

        if ($update_print) {
            $res = [
                'kode' => 200,
                'msg' => 'Berhasil Update Data'
            ];
        } else {
            $res = [
                'kode' => 400,
                'msg' => 'Gagal Update Data'
            ];            
        }

        return response()->json($res);
    }

}
