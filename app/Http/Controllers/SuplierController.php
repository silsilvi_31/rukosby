<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
// use Excel;
// use App\User;
// use App\Exports\UserExport;
// use App\Exports\PresensiExport;

class SuplierController extends Controller
{
    public function index()
    {
        return view('admin.master.suplier.index');
    }

    public function datatable()
    {
        $data = DB::table('suplier')->get()->where('status', NULL);
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $edit = route('suplier.form_edit', [base64_encode($data->id)]);
            return '<a href="'.$edit.'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                    <button class="btn btn-sm btn-danger" onclick="delete_suplier('.$data->id.')"><i class="fa fa-trash-o"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form()
    {
        return view('admin.master.suplier.form');
    }

    public function save(Request $req)
    {
        $nama = $req->suplier;
        $no_telp = $req->no_telp;
        $alamat = $req->alamat;
        $ket = $req->ket;
        
        $suplier = [
            'nama' => $nama,
            'alamat' => $alamat,
            'no_telp' => $no_telp,
            'ket' => $ket
        ];

        $insert_suplier = DB::table('suplier')->insert($suplier);
        if ($insert_suplier) {
            $data = [
                'code' => 200,
                'msg' => 'Berhasil disimpan'
            ];
        }else {
            $data = [
                'code' => 400,
                'msg' => 'Gagal disimpan'
            ];
        }
        return redirect()->route('suplier.index')->with($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id= $req->_idSuplier;

        $data_suplier = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];
        $res = [];
        $update_suplier = DB::table('suplier')->where('id',$id)->update($data_suplier);
        if ($update_suplier) {
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function form_edit($id)
    {
        $id_suplier = base64_decode($id);
        $suplier = DB::table('suplier')->where('id',$id_suplier)->first();

        $data['id_suplier'] = base64_encode($suplier->id);
        $data['nama'] = $suplier->nama;
        $data['no_telp'] = $suplier->no_telp;
        $data['alamat'] = $suplier->alamat;
        $data['ket'] = $suplier->ket;

        return view('admin.master.suplier.form_edit')->with($data);
    }

    public function update(Request $req)
    {
        $id_user = session::get('id_user');
        $id_suplier = base64_decode($req->id_suplier);
        $suplier = $req->suplier;
        $no_telp = $req->no_telp;
        $alamat = $req->alamat;
        $ket = $req->ket;
       
        $data_suplier = [
            'nama' => $suplier,
            'alamat' => $alamat,
            'no_telp' => $no_telp,
            'ket' => $ket,
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user
        ];

        $update = DB::table('suplier')->where('id', $id_suplier)->update($data_suplier);
        if ($update) {
            $res = [
                'code' => 201,
                'msg' => 'Data telah diupdate'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal diupdate'
            ];
        }
        $data['response'] = $res;
        return redirect()->route('suplier.index')->with($data);
        // return "update";
    }
}