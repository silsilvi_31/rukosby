<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use Exception;

class TransaksiController extends Controller
{
    private $transaksi;

    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");  
        $this->transaksi = DB::table('transaksi')->get();  
    }

    public function index()
    {
        return view('admin.transaksi.index');
    }

    public function datatable()
    {
        $transaksi = DB::table('transaksi')->get();

        return datatables::of($transaksi)
        ->addIndexColumn()
        ->editColumn('id_sj', function ($transaksi) {
            return 'Sj '.$transaksi->id_sj;
        })
        ->editColumn('tgl', function ($transaksi) {
            return date('d-m-Y', strtotime($transaksi->tgl));
        })
        ->make(true);
    }    

    public function form()
    {
        return view('admin.transaksi.form');
    }

    public function get_transaksi($transaksi, $id_sj)
    {
        $dty = array_filter($transaksi->toArray(), function ($value) use ($id_sj) {
            return $value->id_sj == $id_sj;
        });

        $bayar = null;
        $jenis_byr = '';
        foreach ($dty as $value) {
            $bayar += $value->total;
            if ($value->jenis_byr == 'Transfer') {
                $jenis_byr = '<br> Transfer : ';
            }
        }

        $data['bayar'] = $bayar;
        $data['jenis_byr'] = $jenis_byr;
        return $data;
    }

    public function datatable_sj()
    {
        $sj = DB::table('suratjalan as a')
                            ->leftJoin('pelanggan as b', 'a.member', '=', 'b.id')
                            ->where(function ($query) {
                                $query->whereColumn('total', '>', 'bayar')
                                      ->orWhere('bayar', NULL);
                            })
                            ->select('a.id as id_sj', 'a.tgl', 'b.nama', 'a.total', 'a.bayar')
                            ->get();
        $transaksi = $this->transaksi;

        $dt = [];
        foreach ($sj as $v) {
            $total = $v->total;
            $bayar = $v->bayar;
            $pelunasan = $this->get_transaksi($transaksi, $v->id_sj)['bayar'];
            $sisa = $total - ($bayar + $pelunasan);

            if ($sisa > 0) {
                $dt[] = (object) [
                    'id_sj' => $v->id_sj,
                    'tgl' => $v->tgl,
                    'nama' => $v->nama,
                    'total' => $total,
                    'bayar' => $bayar,
                    'pelunasan' => $pelunasan,
                    'sisa' => $sisa
                ];
            }
        }

        return datatables::of($dt)
        ->addIndexColumn()
        ->addColumn('opsi', function ($dt){
            $id_sj = $dt->id_sj;
            $sisa = $dt->sisa;
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_sj('.$id_sj.','.$sisa.')">Pilih</button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }
    
    public function set_akun_pelunasan($transaksi)
    {
        $pelanggan = DB::table('suratjalan as a')
                                    ->leftJoin('pelanggan as b', 'a.member', '=', 'b.id')
                                    ->where('a.id', $transaksi['id_sj'])
                                    ->select('nama')
                                    ->first();
        // kas/bank
        $akun[0]['tgl'] = $transaksi['tgl'];
        $akun[0]['id_item'] = NULL;
        $akun[0]['ref'] = $transaksi['id_sj'];
        $akun[0]['hpp'] = NULL;
        $akun[0]['grup'] = 4;
        $akun[0]['jenis_jurnal'] = 'ina';
        $akun[0]['nama'] = $pelanggan->nama; 
        $akun[0]['no_akun'] = $transaksi['jenis_byr'] == 'Transfer' ? 120 : 110;
        $akun[0]['keterangan'] = null;
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = '';
        $akun[0]['qty'] = NULL;
        $akun[0]['harga'] = $transaksi['total'];
        $akun[0]['total'] = $transaksi['total'];
        
        // piutang
        $akun[1]['tgl'] = $transaksi['tgl'];
        $akun[1]['id_item'] = null;
        $akun[1]['ref'] =  $transaksi['id_sj'];
        $akun[1]['hpp'] = 1;
        $akun[1]['grup'] = 4;
        $akun[1]['jenis_jurnal'] = 'ina';
        $akun[1]['nama'] = $pelanggan->nama;
        $akun[1]['no_akun'] = '130';
        $akun[1]['keterangan'] = '';
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = 'b';
        $akun[1]['qty'] = null;
        $akun[1]['harga'] = $transaksi['total'];
        $akun[1]['total'] = $transaksi['total'];
        
        // dd($akun);
        $simpan_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $id_sj = $req->_idSj;
        $tgl = $req->_tgl;
        $jenis_byr = $req->_jenisByr;
        $total = $req->_total;
        $ketr = $req->_ketr;

        $transaksi = [
                    'id_sj' => $id_sj,
                    'tgl' => date('Y-m-d', strtotime($tgl)),
                    'jenis_byr' => $jenis_byr,
                    'total' => $total,
                    'keterangan' => $ketr,
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_add' => $id_user
                ];
        
        DB::beginTransaction();
        
        try {
            $insert = DB::table('transaksi')->insert($transaksi);
            $this->set_akun_pelunasan($transaksi);

            Db::commit();
            $res = [
                'code' => 200,
                'msg' => 'Data berhasil Disimpan'
            ];
            Session::put('code',200);
            Session::put('msg','Data Berhasil Disimpan');
        } catch (Exception $th) {
            DB::rollback();
            $res = [
                'code' => 400,
                'msg' => $th->getMessage()
            ];
            Session::put('code',400);
            Session::put('msg','Data Gagal Disimpan');
        }
        $data['response'] = $res;
        return response()->json($data);
    }

}
