<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\SjExport;

class StokOpnameController extends Controller
{
    private $url;
    private $beli;
    private $jual;
    private $retur_beli;
    private $retur_jual;
    private $jurnal_umum_msk;
    private $jurnal_umum_klr;

    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
        // $this->url = 'http://adm.wijayaplywoodsindonesia.com/api/ina/stok';
        // $this->url = 'http://192.168.5.9:8080/api/ina/stok';
        // $this->apina = json_decode(file_get_contents($this->url), true);
        $this->beli = DB::table('jurnal')
                            ->where('jenis_jurnal', 'beli')
                            // ->where('bm', 1)
                            // ->whereIn('bm', [1,2])
                            ->where('map', 'd')
                            ->groupBy('bm')
                            ->get();

        $this->jual = DB::table('jurnal')
                            ->where('jenis_jurnal', 'ina')
                            // ->where('ref', 1)
                            // ->whereIn('ref', [1,2])
                            ->where('map', 'k')
                            ->groupBy('ref')
                            ->get();

        $this->retur_beli = DB::table('jurnal')
                            ->where('jenis_jurnal', 'beli-retur')
                            // ->where('bm', 1)
                            // ->whereIn('bm', [1,2])
                            ->where('map', 'k')
                            ->groupBy('bm')
                            ->get();

        $this->retur_jual = DB::table('jurnal')
                            ->where('jenis_jurnal', 'ina-retur')
                            // ->where('ref', 1)
                            // ->whereIn('ref', [1,2])
                            ->where('map', 'd')
                            ->groupBy('ref')
                            ->get();

        $this->jurnal_umum_msk = DB::table('jurnal')
                            ->where('jenis_jurnal', 'ju')
                            // ->where('ref', 1)
                            // ->whereIn('ref', [1,2])
                            ->where('map', 'd')
                            ->whereIn('no_akun', ['140', '150', '160', '170'])
                            ->groupby('ref')
                            ->get();

        $this->jurnal_umum_klr = DB::table('jurnal')
                            ->where('jenis_jurnal', 'ju') 
                            // ->where('ref', 1)
                            // ->whereIn('ref', [1,2])
                            ->where('map', 'k')
                            ->whereIn('no_akun', ['140', '150', '160', '170'])
                            ->groupBy('ref')
                            ->get();
    }

    public function index()
    {
        return view('admin.StokOpname.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function get_sisa($data, $id_brg, $opname_time)
    {
        $cari = [
            'id_brg'    => $id_brg,
            'tgl_opname'  => $opname_time
        ];

        $dty = array_filter($data, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id_brg'] && $value['tgl_time'] >= $cari['tgl_opname'];
        });

        if (is_null($opname_time)) {
            $dty = array_filter($data, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id_brg'];
            });
        }

        $total_qty = 0;
        foreach ($dty as $val) {
            $total_qty += $val['qty'];
        }

        return $total_qty;
    }

    public function tampil_barang($id, $method)
    {
        if($method == 'beli') {
            $hasil = DB::table('beli_detail as a')
                            ->leftJoin('beli as b', 'a.id_detail_beli', '=', 'b.id_beli')
                            ->where('a.id_detail_beli', $id)
                            ->whereNotNull('b.is_cek_beli')
                            ->whereNull('b.user_batal')
                            ->get();
        } elseif($method == 'jual') {
            $hasil = DB::table('suratjalan_detail as a')
                            ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                            ->where('a.id_sj', $id)
                            ->whereNotNull('b.is_cek_nota')
                            ->whereNull('b.is_batal')
                            ->get();
        }  elseif($method == 'beli-retur') {
            $hasil = DB::table('retur_brg')->where('id_sj', $id)->where('jenis', 'beli')->get();
        } elseif($method == 'ina-retur') {
            $hasil = DB::table('retur_brg')->where('id_sj', $id)->where('jenis', 'jual')->get();
        } elseif($method == 'jurnal-masuk') {
            $hasil = DB::table('jurnal_umum_detail as a')
                            ->leftJoin('jurnal_umum as b', 'a.id_ju', '=', 'b.id_ju')
                            ->where('a.id_ju', $id)
                            ->whereNotNull('b.is_cek_jurnal')
                            ->get();
        } elseif($method == 'jurnal-keluar') {
            $hasil = DB::table('jurnal_umum_detail as a')
                                ->leftJoin('jurnal_umum as b', 'a.id_ju', '=', 'b.id_ju')
                                ->where('a.id_ju', $id)
                                ->whereNotNull('b.is_cek_jurnal')
                                ->get();
        }

        return $hasil;
    }

    public function closestnumber($cari, $tgl) {
        $akhir = null;
        foreach ($tgl as $c) {
            if ($c < $cari) {
                $akhir = $c;
            } else if ($c == $cari) {
                return $cari;
            } else if ($c > $cari) {
                return $akhir;
            }
        }
        return $akhir;
    }

    public function get_sisa_detail($data, $id_brg, $tgl)
    {
        $cari = [
            'id_brg'    => $id_brg,
            'tgl'       => $tgl
        ];

        $dty = array_filter($data, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id_brg'] && $value['tgl'] == $cari['tgl'];
        });

        $total_qty = 0;
        foreach ($dty as $val) {
            $total_qty += $val['qty'];
        }

        return $total_qty;
    }

    function get_tgl_terakhir($stok, $max){
        $dataku = array_filter($stok, function ($vv) use ($max){
            return $vv['tgl_time'] == $max;
        });

        $sisa = 0;
        foreach ($dataku as $key => $value) {
            $sisa = $value['stok_awal'];

        }
        
        return $sisa;
    }

    public function stok_tgl(Request $req)
    {
        $tgl = $req->_tgl;
        $id_brg = $req->_idBrg;
        $data_brg = DB::table('barang')->where('kode', $id_brg)->first();

        $opname = DB::table('log_opname_stok')
                        // ->where('tgl', $tgl_)
                        ->where('id_brg', $data_brg->kode)
                        ->orderBy('created_at', 'DESC')
                        ->select('tgl', 'id_brg', 'nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
                        ->groupBy('tgl', 'id_brg', 'nama_brg', 'stok_sesudah')
                        ->get();

        $dt_beli = [];
        $dt_jual = [];
        $dt_beli_retur = [];
        $dt_jual_retur = [];
        $dt_msk_jurnal_umum = [];
        $dt_klr_jurnal_umum = [];
        $dt_opname = [];

        $tmp_beli = [];
        $tmp_jual = [];
        $tmp_beli_retur = [];
        $tmp_jual_retur = [];
        $tmp_msk_ju = [];
        $tmp_klr_ju = [];
        $tmp_opname = [];

        $stokQ = [];

        $tmp_tgl_fix = [];

        // AWAL JURNAL
        foreach ($this->beli as $value) {
            $dt_beli[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->bm, 'beli')
            ];
        }

        foreach ($this->jual as $value) {
            $dt_jual[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jual')
            ];
        }

        foreach ($this->retur_beli as $value) {
            $dt_beli_retur[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->bm, 'beli-retur')
            ];
        }

        foreach ($this->retur_jual as $value) {
            $dt_jual_retur[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'ina-retur')
            ];
        }
        foreach ($this->jurnal_umum_msk as $value) {
            $dt_msk_jurnal_umum[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jurnal-masuk')
            ];
        }

        foreach ($this->jurnal_umum_klr as $value) {
            $dt_klr_jurnal_umum[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jurnal-keluar')
            ];
        }
        // AKHIR JURNAL

        foreach ($dt_beli as $v) {
            foreach ($v->barang as $x) {
               $tmp_beli[] = [
                'tgl'           => $v->tgl,
                'tgl_time'      => strtotime($v->tgl),
                'id_brg'        => $x->id_brg,
                'nama_brg'      => $x->nama_brg,
                'qty'           => $x->qty,
                'harga'         => $x->harga
               ];
            }
        }

        foreach ($dt_jual as $v) {
            foreach ($v->barang as $x) {
               $tmp_jual[] = [
                'tgl'           => $v->tgl,
                'tgl_time'      => strtotime($v->tgl),
                'id_brg'        => $x->id_brg,
                'nama_brg'      => $x->nama_brg,
                'qty'           => $x->qty,
                'harga'         => $x->harga
               ];
            }
        }

        foreach ($dt_beli_retur as $v) {
            foreach ($v->barang as $x) {
                $tmp_beli_retur[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty_retur,
                    'harga'         => $x->hpp
                ];
            }
        }

        foreach ($dt_jual_retur as $v) {
            foreach ($v->barang as $x) {
                $tmp_jual_retur[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty_retur,
                    'harga'         => $x->hpp
                ];
            }
        }

        foreach ($dt_msk_jurnal_umum as $v) {
            foreach ($v->barang as $x) {
                $tmp_msk_ju[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty,
                    'harga'         => $x->harga
                ];
            }
        }

        foreach ($dt_klr_jurnal_umum as $v) {
            foreach ($v->barang as $x) {
                $tmp_klr_ju[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty,
                    'harga'         => $x->harga
                ];
            }
        }

        foreach ($opname as $v) {
            $tmp_opname[] = [
                'tgl'           => $v->tgl,
                'id_brg'        => $v->id_brg,
                'nama_brg'      => $v->nama_brg,
                'qty'           => $v->stok_sesudah
            ];
        }

        $tmp_tgl_fix = [];
        $tmp_tgl_fase1 = array_merge($tmp_beli, $tmp_jual, $tmp_beli_retur, $tmp_jual_retur, $tmp_msk_ju, $tmp_klr_ju, $tmp_opname);

        foreach ($tmp_tgl_fase1 as $e) {
            $tmp_tgl_fix[$e['tgl']] = $e['tgl'];
        }
        $tglQ = [];
        foreach (array_sort($tmp_tgl_fix) as $t) {
            $tglQ[] = strtotime($t);
        }

        $cari_tgl = array_filter($tglQ, function ($v) use ($tgl) {
            return $v == strtotime($tgl);
        });

        $saklar = 'off';

        if(empty($cari_tgl)) {
            $saklar = 'on';
        }

        $tgl_dekat = (!empty($tmp_tgl_fix)) ? $this->closestnumber( strtotime($tgl), array_values($tglQ) ): null; 
 
        $hasil = 0;
        $awal = 0;

        foreach (array_sort($tmp_tgl_fix) as $v) {
            $stok_beli = $this->get_sisa_detail($tmp_beli, $data_brg->kode, $v);
            $stok_jual = $this->get_sisa_detail($tmp_jual, $data_brg->kode, $v);
            $stok_beli_retur = $this->get_sisa_detail($tmp_beli_retur, $data_brg->kode, $v);
            $stok_jual_retur = $this->get_sisa_detail($tmp_jual_retur, $data_brg->kode, $v);
            $stok_msk_ju = $this->get_sisa_detail($tmp_msk_ju, $data_brg->kode, $v);
            $stok_klr_ju = $this->get_sisa_detail($tmp_klr_ju, $data_brg->kode, $v);
            
            $log_opname = DB::table('log_opname_stok')
                                ->where('tgl', $v)
                                ->where('id_brg', $data_brg->kode)
                                ->orderBy('created_at', 'DESC')
                                ->select('tgl', 'id_brg', 'nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
                                ->groupBy('tgl', 'id_brg' ,'nama_brg', 'stok_sesudah')
                                ->first();

            if (isset($log_opname)) {
                $awal = $log_opname->stok_sesudah;
                $ketr = '<span class="badge bg-green">opname</span>';
            } else {
                $ketr = '';
            }

            $stok_masuk = ($stok_beli + $stok_msk_ju);
            $stok_keluar = ($stok_jual + $stok_klr_ju);

            $hasil = $awal + ($stok_masuk + $stok_jual_retur ) - ($stok_keluar + $stok_beli_retur);

            $stokQ[] = [
                'tgl'               => $v,
                'tgl_time'          => strtotime($v),
                'id_brg'            => $data_brg->kode,
                'nama_brg'          => $data_brg->nama_brg,
                'stok_beli'         => $stok_beli,
                'stok_jual'         => $stok_jual,
                'stok_beli_retur'   => $stok_beli_retur,
                'stok_jual_retur'   => $stok_jual_retur,
                'stok_msk_ju'       => $stok_msk_ju,
                'stok_klr_ju'       => $stok_klr_ju,
                'stok_awal'         => $awal,
                'stok_masuk'        => $stok_masuk,
                'stok_keluar'       => $stok_keluar,
                'sisa'              => $hasil
            ];
            $awal = $hasil;
        }
        
        // $dty = array_filter($stokQ, function ($value) use ($tgl_dekat) {
        //     return $value['tgl_time'] == $tgl_dekat;  
        // });

        // $sty = [];

        // if (!empty($dty)) {
        //     foreach ($dty as $s) {
        //         $sty = [
        //             'tgl'             => $s['tgl_time'],
        //             'stok_awl'        => $s['stok_awal'],
        //             'stok_msk'        => $s['stok_masuk'],
        //             'stok_klr'        => $s['stok_keluar'],
        //             'stok_bl_retur'   => $s['stok_beli_retur'],
        //             'stok_jl_retur'   => $s['stok_jual_retur'],
        //             'sisaa'           => $s['sisa']
        //         ];

        //         if ($saklar == 'on') {
        //             $sty = [
        //                 'tgl'             => $s['tgl_time'],
        //                 'stok_awl'        => $s['sisa'],
        //                 'stok_msk'        => NULL,
        //                 'stok_klr'        => NULL,
        //                 'stok_bl_retur'   => NULL,
        //                 'stok_jl_retur'   => NULL,
        //                 'sisaa'           => NULL
        //             ];
        //         }
        //     }
        // } else {
        //     $sty = [
        //         'tgl'             => $s['tgl_time'],
        //         'stok_awl'        => NULL,
        //         'stok_msk'        => NULL,
        //         'stok_klr'        => NULL,
        //         'stok_bl_retur'   => NULL,
        //         'stok_jl_retur'   => NULL,
        //         'sisaa'           => NULL
        //     ];
        // }
        $stok_akhir = $this->get_tgl_terakhir($stokQ, $tgl_dekat);
        return response()->json($stok_akhir);
    }

    public function datatable()
    {
        // $tgl_now = date("Y-m-d");
        $data_brg = DB::table('barang as a')
                        ->where('a.status',NULL)
                        ->where('jenis_brg', 5)
                        ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                        ->select('a.kode', 'a.nama_brg', 'a.hpp', 'a.harga', 'a.satuan as id_satuan','b.nama as satuan', 'a.jenis_brg','a.stok','a.tgl_opname')
                        ->get();

        $dt_beli = [];
        $dt_jual = [];
        $dt_beli_retur = [];
        $dt_jual_retur = [];
        $dt_msk_jurnal_umum = [];
        $dt_klr_jurnal_umum = [];

        $stok_brg_beli = [];
        $stok_brg_jual = [];
        $stok_brg_beli_retur = [];
        $stok_brg_jual_retur = [];
        $stok_brg_msk_ju = [];
        $stok_brg_klr_ju = [];

        $stokQ = [];
        
        // AWAL JURNAL
        foreach ($this->beli as $value) {
            $dt_beli[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->bm, 'beli')
            ];
        }

        foreach ($this->jual as $value) {
            $dt_jual[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jual')
            ];
        }

        foreach ($this->retur_beli as $value) {
            $dt_beli_retur[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->bm, 'beli-retur')
            ];
        }

        foreach ($this->retur_jual as $value) {
            $dt_jual_retur[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'ina-retur')
            ];
        }

        foreach ($this->jurnal_umum_msk as $value) {
            $dt_msk_jurnal_umum[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jurnal-masuk')
            ];
        }

        foreach ($this->jurnal_umum_klr as $value) {
            $dt_klr_jurnal_umum[] = (object) [
                "tgl"            => $value->tgl,
                "no_akun"        => $value->no_akun,
                "hpp"            => $value->hpp,
                "jenis_jurnal"   => $value->jenis_jurnal,
                "ref"            => $value->ref,
                "bm"             => $value->bm,
                "map"            => $value->map,
                "qty"            => $value->qty,
                "harga"          => $value->harga,
                "total"          => $value->total,
                "barang"         => $this->tampil_barang($value->ref, 'jurnal-keluar')
            ];
        }

        // AKHIR JURNAL
        foreach ($dt_beli as $v) {
            foreach ($v->barang as $x) {
               $stok_brg_beli[] = [
                'tgl'           => $v->tgl,
                'tgl_time'      => strtotime($v->tgl),
                'id_brg'        => $x->id_brg,
                'nama_brg'      => $x->nama_brg,
                'qty'           => $x->qty,
                'harga'         => $x->harga
               ];
            }
        }

        foreach ($dt_jual as $v) {
            foreach ($v->barang as $x) {
               $stok_brg_jual[] = [
                'tgl'           => $v->tgl,
                'tgl_time'      => strtotime($v->tgl),
                'id_brg'        => $x->id_brg,
                'nama_brg'      => $x->nama_brg,
                'qty'           => $x->qty,
                'harga'         => $x->harga
               ];
            }
        }

        foreach ($dt_beli_retur as $v) {
            foreach ($v->barang as $x) {
                $stok_brg_beli_retur[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty_retur,
                    'harga'         => $x->hpp
                ];
            }
        }

        foreach ($dt_jual_retur as $v) {
            foreach ($v->barang as $x) {
                $stok_brg_jual_retur[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty_retur,
                    'harga'         => $x->hpp
                ];
            }
        }

        foreach ($dt_msk_jurnal_umum as $v) {
            foreach ($v->barang as $x) {
                $stok_brg_msk_ju[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty,
                    'harga'         => $x->harga
                ];
            }
        }

        foreach ($dt_klr_jurnal_umum as $v) {
            foreach ($v->barang as $x) {
                $stok_brg_klr_ju[] = [
                    'tgl'           => $v->tgl,
                    'tgl_time'      => strtotime($v->tgl),
                    'id_brg'        => $x->id_brg,
                    'nama_brg'      => $x->nama_brg,
                    'qty'           => $x->qty,
                    'harga'         => $x->harga
                ];
            }
        }

        $sisa = 0;
        $stok_awal = 0;

        $dt = [];
        $awal = 0;
        $hasil = 0;
        $hasil_masuk = 0;

        foreach ($data_brg as $b) {
            $opname_time = isset($b->tgl_opname) ? (string)strtotime($b->tgl_opname) : null;
            $stok_awal = isset($b->stok) ? $b->stok : null;

            $stok_beli = $this->get_sisa($stok_brg_beli, $b->kode, $opname_time);
            $stok_jual = $this->get_sisa($stok_brg_jual, $b->kode, $opname_time);
            $stok_beli_retur = $this->get_sisa($stok_brg_beli_retur, $b->kode, $opname_time);
            $stok_jual_retur = $this->get_sisa($stok_brg_jual_retur, $b->kode, $opname_time);
            $stok_msk_jurnal = $this->get_sisa($stok_brg_msk_ju, $b->kode, $opname_time);
            $stok_klr_jurnal = $this->get_sisa($stok_brg_klr_ju, $b->kode, $opname_time);

            $stok_masuk = $stok_beli + $stok_jual_retur + $stok_msk_jurnal;
            $stok_keluar = $stok_jual + $stok_beli_retur + $stok_klr_jurnal;
            $sisa = $stok_awal + $stok_masuk - $stok_keluar;

            $dt[] = (object) [
                'id_brg'          => $b->kode,
                'nama_brg'        => $b->nama_brg,
                'hpp'             => $b->hpp,
                'harga'           => $b->harga,
                'id_satuan'       => $b->id_satuan,
                'satuan'          => $b->satuan,
                'stok_beli'       => $stok_beli,
                'stok_jual'       => $stok_jual,
                'stok_beli_retur' => $stok_beli_retur,
                'stok_jual_retur' => $stok_jual_retur,
                'stok_msk_jurnal' => $stok_msk_jurnal,
                'stok_klr_jurnal' => $stok_klr_jurnal,
                'stok_awal'       => $stok_awal,
                'stok_masuk'      => $stok_masuk,
                'stok_keluar'     => $stok_keluar,
                'sisa'            => $sisa
            ];
        }


        return datatables::of($dt)
        ->addIndexColumn()
        ->addColumn('stok_awal', function ($dt){
            return $dt->stok_awal;
        })
        ->addColumn('stok_masuk', function ($dt){
            return $dt->stok_masuk;
        })
        ->addColumn('stok_keluar', function ($dt){
            return $dt->stok_keluar;
        })
        ->addColumn('sisa', function ($dt){
            return $dt->sisa;
        })
        ->addColumn('opsi', function ($dt){
            $link_detail = route('stok.detail', [base64_encode($dt->id_brg)]);
            // $tgl = date('d-m-Y');
            return '<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modal_opname" data-kode="'.$dt->id_brg.'" data-namabrg="'.$dt->nama_brg.'">Opname</button>
                    <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_detail_opname" data-kode="'.$dt->id_brg.'" data-namabrg="'.$dt->nama_brg.'">Detail</button>';
        })
        ->rawColumns(['stok_awal','stok_masuk', 'stok_keluar', 'sisa', 'opsi'])
        ->make(true);
    }

    public function set_akun_opname($data_opname, $id_opname)
    {
        $barang = DB::table('barang')->where('kode', $data_opname['id_brg'])->first();

            $akun_debit = $data_opname['ketr'] == 'kelebihan' ? '140' : '640';
            $akun_kredit = $data_opname['ketr'] == 'kehilangan' ? '140' : '310';

            $akun[0]['tgl'] = $data_opname['tgl'];
            $akun[0]['id_item'] = NULL;
            $akun[0]['ref'] = $id_opname;
            $akun[0]['hpp'] = null;
            $akun[0]['grup'] = 1;
            $akun[0]['jenis_jurnal'] = 'opname';
            $akun[0]['nama'] = 'catatan perubahan kualitas'; 
            $akun[0]['no_akun'] = $akun_debit;
            $akun[0]['keterangan'] = $data_opname['nama_brg'].' // '.$data_opname['ketr'];
            $akun[0]['map'] = 'd';
            $akun[0]['hit'] = '';
            $akun[0]['qty'] = $data_opname['selisih'];
            $akun[0]['harga'] = $data_opname['selisih'] * $barang->hpp;
            $akun[0]['total'] = $data_opname['selisih'] * $barang->hpp;

            $akun[1]['tgl'] = $data_opname['tgl'];
            $akun[1]['id_item'] = NULL;
            $akun[1]['ref'] = $id_opname;
            $akun[1]['hpp'] = null;
            $akun[1]['grup'] = 2;
            $akun[1]['jenis_jurnal'] = 'opname';
            $akun[1]['nama'] = 'catatan perubahan kualitas'; 
            $akun[1]['no_akun'] = $akun_kredit;
            $akun[1]['keterangan'] = $data_opname['nama_brg'].' // '.$data_opname['ketr'];
            $akun[1]['map'] = 'k';
            $akun[1]['hit'] = '';
            $akun[1]['qty'] = $data_opname['selisih'];
            $akun[1]['harga'] = $data_opname['selisih'] * $barang->hpp;
            $akun[1]['total'] = $data_opname['selisih'] * $barang->hpp;

            $insert_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $kode = $req->_kode;
        $barang = DB::table('barang')->where('kode', $kode)->first();
        $nama_brg = $barang->nama_brg;
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $stok_sblm = $req->_stokSblm;
        $stok_sesudah = $req->_stokSesudah;
        $selisih = $req->_selisih;
        $ketr = $req->_ketr;

        $data_opname =  [
                            'id_brg' => $kode,
                            'nama_brg' => $nama_brg,
                            'tgl' => $tgl,
                            'stok_sblm' => $stok_sblm,
                            'stok_sesudah' => $stok_sesudah,
                            'selisih' => $selisih,
                            'ketr' => $ketr,
                            'created_at' => date("Y-m-d H:i:s"),
                            'user_add' => $id_user
                        ];

        $cek_opname = DB::table('log_opname_stok')
                                ->where('id_brg', $kode)
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($cek_opname) ? strtotime($cek_opname->tgl) : '';
        $tgl_int = strtotime($tgl);

        if ($tgl_int <= $tgl_akhir) {
            $res = [
                'code' => 400,
                'msg' => 'Tidak bisa opname ditanggal tersebut',
            ];
        } else {
            $insert_opname = DB::table('log_opname_stok')->insertGetId($data_opname);

            if($insert_opname) {
                // $update_stok = DB::table('barang')
                //                     ->where('kode', $kode)
                //                     ->update([
                //                         'stok'          => $stok_sesudah,
                //                         'tgl_opname'    => $tgl
                //                     ]);
                $this->set_akun_opname($data_opname, $insert_opname);
                
                $res = [
                    'code' => 200,
                    'msg' => 'Berhasil Disimpan',
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Gagal Disimpan'
                ];
            }
        }
        
        return response()->json($res);
    }

    public function datatable_detail(Request $req)
    {
        $kode_brg = $req->_detKodeBrg;

        $dt = [];
        $log_opname = DB::table('log_opname_stok')
                                ->where('id_brg', $kode_brg)
                                // ->where('is_cek_opname',1)
                                ->get();

        return datatables::of($log_opname)
        ->addIndexColumn()
        ->addColumn('opsi', function ($log_opname){
            $btn = '<button type="button" class="btn btn-sm btn-danger" onclick="batal_opname('.$log_opname->id.','.$log_opname->id_brg.')">Batal</button>';
            if (isset($log_opname->is_cek_opname)) {
                $btn = '<span class="badge bg-green">approved</span>';
            }
            
            return $btn;
            // return 'opsi';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function batal_opname(Request $req)
    {
        $id = $req->_id;
        $id_brg = $req->_idBrg;

        $delete_opname = DB::table('log_opname_stok')->where('id', $id)->delete();

        if (isset($delete_opname)) {
            $delete_jurnal = DB::table('jurnal')->where('ref', $id)->delete();
            
            $res = [
                'code' => 200,
                'msg' => 'Opname Berhasil Dibatalkan'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Opname Gagal Dibatalkan'
            ];
        }
        return response()->json($res);
    }

}
