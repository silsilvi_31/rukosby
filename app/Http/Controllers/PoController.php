<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use Exception;

class PoController extends Controller
{
    private $transaksi_po;

    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
        $this->transaksi_po = DB::table('transaksi')->where('is_po', 1)->get();
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return isset($data) ? $data->nama : 'No Name';
    }

    public function index()
    {
        return view('admin.po.index');
    }

    public function form()
    {
        $kendaraan = DB::table('kendaraan')
                            // ->where('status', NULL)
                            ->where('penyusutan', '<>', NULL)
                            ->get();
        $sopir = DB::table('karyawan')
                                ->where('id_jabatan',4)
                                ->get();
        $pelanggan = DB::table('pelanggan')->where('status', NULL)->get();
                                               
        $data['kendaraan'] = $kendaraan;
        $data['sopir'] = $sopir;
        $data['pelanggan'] = $pelanggan;
        
        return view('admin.po.form')->with($data);
    }

    public function get_qty_beli($data_beli, $id_brg, $tgl)
    {
        $cari = [
                    'id_brg' => (string) $id_brg,
                    'tgl' => (string) $tgl
        ];

        $dty = array_filter($data_beli, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id_brg'] && $value['tgl'] >= $cari['tgl'];
        });

        $tmp = [];
        $hasil = 0;
        foreach ($dty as $k => $v) {
            $hasil += $v['qty'];
        }

        return $hasil;
    }

    public function get_brg_jual($sj_detail, $id_brg, $tgl)
    {
        $cari = [
            'id' => $id_brg,
            'tgl_time' => $tgl
        ];
        
        $dty = array_filter($sj_detail, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id'] && $value['tgl'] >= $cari['tgl_time'];
        });

        if (is_null($tgl)) {
            $dty = array_filter($sj_detail, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id'];
            });
        }

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v['qty'];
            }
        }
        return $hasil;
    }

    public function get_brg_masuk($apiIna, $id_brg, $tgl)
    {
        $id = $id_brg;
        $cari = [
            'id' => $id_brg,
            'tgl_time' => $tgl
        ];

        $dty = array_filter($apiIna, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id'] && $value['tgl'] >= $cari['tgl_time'];
        });

        if (is_null($tgl)) {
            $dty = array_filter($apiIna, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id'];
            });
        }

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v['qty'];
            }
        }
        
        return $hasil;
    }

    public function datatable_brg()
    {
        $tgl_now = date("Y-m-d");
        $data = DB::table('barang as a')
                ->where('a.status',NULL)
                ->where('jenis_brg', 5)
                ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                ->select('a.kode', 'a.nama_brg', 'a.hpp', 'a.harga', 'a.satuan as id_satuan','b.nama as satuan', 'a.jenis_brg','a.stok','a.tgl_opname')
                ->get();
        
        // $rUrl =  $this->url;
        // $apiIna = json_decode(file_get_contents($rUrl), true);

        $brg_sj = [];
        // $brg_masuk = [];
        $brg_beli = [];
        $brg_retur_jual = [];
        $brg_retur_beli = [];

        // foreach ($apiIna as $y => $e) {
        //     $brg_masuk[] = [
        //         'id_sj' => $e['id_sj'],
        //         'tgl' => (string)strtotime($e['tgl']),
        //         'tgl_s' => $e['tgl'],
        //         'id_brg' => $e['id_brg'],
        //         'nama_brg' => $e['nama_brg'],
        //         'qty' => $e['qty'],
        //         'harga' => $e['harga'],
        //         'harga_new' => $e['harga_new']
        //     ];
        // }

        $sj_detail = DB::table('suratjalan_detail as a')
                                ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                                // ->whereNotNull('b.bayar')
                                ->whereNotNull('b.is_cek_nota')
                                ->select('a.id_brg','a.qty','b.tgl')
                                ->get();

        foreach ($sj_detail as $t => $d) {
            $brg_sj[] = [
                'id_brg' => $d->id_brg,
                'qty' => $d->qty,
                'tgl' => strtotime($d->tgl)
            ];
        }

        $beli = DB::table('beli as a')
                            ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'id_detail_beli')
                            ->whereNotNull('a.is_cek_beli')
                            ->whereNull('a.user_batal')
                            ->get();

        foreach ($beli as $key => $value) {
            $brg_beli[] = [
                'id_brg' => $value->id_brg,
                'qty' => $value->qty,
                'tgl' => strtotime($value->tgl)
            ];
        }

        $retur_jual = DB::table('retur_brg')
                        ->where('jenis', 'jual')
                        ->get();

        foreach ($retur_jual as $y) {
            $brg_retur_jual[] = [
                'id_brg'   => $y->id_brg,
                'qty'      => $y->qty_retur,
                'tgl'      => strtotime($y->tgl_retur)
            ];  
        }

        $retur_beli = DB::table('retur_brg')
                            ->where('jenis', 'beli')
                            ->get();

        foreach ($retur_beli as $y) {
            $brg_retur_beli[] = [
                'id_brg'   => $y->id_brg,
                'qty'      => $y->qty_retur,
                'tgl'      => strtotime($y->tgl_retur)
            ];  
        }

        $dt = [];
        $awal = 0;
        $hasil = 0;
        $hasil_masuk = 0;

        foreach ($data as $v) {
            $opname_time = isset($v->tgl_opname) ? (string)strtotime($v->tgl_opname) : null; 

            $stok_b = $this->get_qty_beli($brg_beli, $v->kode, $opname_time);
            $stok_k = $this->get_brg_jual($brg_sj, $v->kode, $opname_time);
            // $stok_m = $this->get_brg_masuk($brg_masuk, $v->kode, $opname_time) + $stok_b;
            $stok_r_jual = $this->get_brg_masuk($brg_retur_jual, $v->kode, $opname_time);
            $stok_r_beli = $this->get_brg_masuk($brg_retur_beli, $v->kode, $opname_time);

            $hasil = $v->stok + ($stok_b + $stok_r_jual) - ($stok_k + $stok_r_beli);

            $dt[] = (object) [
                'kode' => $v->kode,
                'nama_brg' => $v->nama_brg,
                'hpp' => $v->hpp,
                'harga' => $v->harga,
                'id_satuan' => $v->id_satuan,
                'satuan' => $v->satuan,
                'stok_awal' => $awal,
                'stok_masuk' => $v->stok + $stok_b + $stok_r_jual,
                'stok_keluar' => $stok_k + $stok_r_beli,
                'sisa' => $hasil,
                'tgl_opname' => $opname_time,
                'list---------' => $stok_b,
                'list_keluar' => $stok_k
            ];
        }

        // dd($dt);
        return Datatables::of($dt)
        ->addIndexColumn()
        ->addColumn('opsi', function ($dt){
            $kode_barang = $dt->kode;
            $nama_brg = "'".$dt->nama_brg."'";
            $hpp = $dt->hpp;
            $harga = $dt->harga;
            $id_satuan = $dt->id_satuan;
            $satuan = "'".$dt->satuan."'";
            $sisa = $dt->sisa;
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_brg('.$kode_barang.','.$nama_brg.','.$hpp.','.$harga.','.$id_satuan.','.$satuan.','.$sisa.')">Pilih</button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function no_urut()
    {
        $id_suratjalan = DB::table('po')->max('id');
        $no = $id_suratjalan;
        $no++;
        return response()->json($no);
    }

    public function get_transaksi($transaksi, $id_sj)
    {
        $dty = array_filter($transaksi->toArray(), function ($value) use ($id_sj) {
            return $value->id_sj == $id_sj;
        });

        $bayar = null;
        foreach ($dty as $value) {
            $bayar += $value->total;
        }

        $data['bayar'] = $bayar;
        return $data;
    }

    public function datatable()
    {
        $data = DB::table('po as sj')
                    ->where('sj.status',5)
                    // ->whereNotIn('sj.id', $id_sj)
                    ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                    ->leftJoin('pelanggan as pl', 'sj.member', '=', 'pl.id')
                    ->select('sj.id','sj.tgl','sj.pelanggan','pl.nama','sj.member','sj.catatan','sj.total','sj.bayar','sj.tgl','sj.opsi','sj.ongkir','sj.pembayaran',
                                'sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj','sj.is_batal','sj.batal',
                                    'kar.nama as sopir')
                    ->orderBy('sj.tgl', 'DESC')
                    ->orderBy('sj.id', 'DESC')
                    ->get(); 

        $transaksi = $this->transaksi_po;
        
        return datatables::of($data)
        ->addColumn('total_bayar', function ($data)  use ($transaksi){
            $total_bayar = $this->get_transaksi($transaksi, $data->id)['bayar'];
            return $total_bayar;
        })
        ->addColumn('sisa_tagihan', function ($data) use ($transaksi) {
            $total = $data->total;
            $total_bayar = $this->get_transaksi($transaksi, $data->id)['bayar'];
            $sisa_tagihan = $total - $total_bayar;
            return ($sisa_tagihan <= 0) ? '<span class="badge badge-success">Lunas</span>' : $sisa_tagihan;
        })
        ->addColumn('status', function ($data) {
            return 'PO';
        })
        ->editColumn('cek_batal', function ($data) {
            $user_btl = isset($data->is_batal) ? $this->get_karyawan($data->is_batal) : null;
            $ket_btl = isset($data->is_batal) ? $data->batal : null;
            return $ket_btl.' ('.$user_btl.')';
        })
        ->addColumn('opsi',  function ($data) use ($transaksi) {
            $total = $data->total;
            $total_bayar = $this->get_transaksi($transaksi, $data->id)['bayar'];

            $status_lunas = ($total <= $total_bayar) ? 'btn-secondary disabled' : 'btn-primary' ;
            $status_lns =  ($total <= $total_bayar) ? 'disabled' : '' ;
            $status_tampil = ($total <= $total_bayar) ? 'none' : '';
            return '<button type="button" class="btn btn-sm '.$status_lunas.'" '.$status_lns.' data-toggle="modal" data-target="#modal_pelunasan" data-id="'.$data->id.'">Pelunasan</button>
                    <button type="button" class="btn btn-sm btn-danger" style="display:'.$status_tampil.'" onclick="delete_po('.$data->id.')"><i class="fa fa-trash"></i></button>
                    <button type="button" class="btn btn-sm btn-danger" style="display:'.$status_tampil.'" data-toggle="modal" data-target="#modal_btl_po" data-id="'.$data->id.'">Batal</button>';
        })
        ->rawColumns(['opsi', 'sisa_tagihan'])
        ->make(true);
    }

    public function set_akun_dp($no_po, $jenis_byr, $dp)
    {
        $po = DB::table('po as a')
                    ->leftJoin('pelanggan as b', 'a.member', '=', 'b.id')
                    ->where('a.id', $no_po)
                    ->first();

        // kas/bank
        $akun[0]['tgl'] = date('Y-m-d');
        $akun[0]['id_item'] = NULL;
        $akun[0]['ref'] = $no_po;
        $akun[0]['hpp'] = NULL;
        $akun[0]['grup'] = 5;
        $akun[0]['jenis_jurnal'] = 'ina-dp';
        $akun[0]['nama'] = $po->nama; 
        $akun[0]['no_akun'] = $jenis_byr == 'Transfer' ? 120 : 110;
        $akun[0]['keterangan'] = 'DP No. PO '.$no_po;
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = NULL;
        $akun[0]['qty'] = NULL;
        $akun[0]['harga'] = $dp;
        $akun[0]['total'] = $dp;
        
        // piutang
        $akun[1]['tgl'] = date('Y-m-d');
        $akun[1]['id_item'] = NULL;
        $akun[1]['ref'] =  $no_po;
        $akun[1]['hpp'] = NULL;
        $akun[1]['grup'] = 5;
        $akun[1]['jenis_jurnal'] = 'ina-dp';
        $akun[1]['nama'] = $po->nama;
        $akun[1]['no_akun'] = '210';
        $akun[1]['keterangan'] = 'DP No. PO '.$no_po;
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = NULL;
        $akun[1]['qty'] = NULL;
        $akun[1]['harga'] = $dp;
        $akun[1]['total'] = $dp;

        $simpan_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function add_dp(Request $req)
    {
        $user_add = session::get('id_user');
        $no_po = $req->_noPo;
        $dp = $req->_dp;
        $jenis_byr = $req->_jenisByr;
        $tgl = date('Y-m-d');

        $dt_dp = [
            'id_sj'             => $no_po,
            'tgl'               => $tgl,
            'jenis_byr'         => $jenis_byr,
            'jenis_transaksi'   => 1,
            'total'             => $dp,
            'is_po'             => 1,
            'created_at'        => date('Y-m-d H:i:s'),
            'user_add'          => $user_add
        ];

        DB::beginTransaction();
        $insert = DB::table('transaksi')->insert($dt_dp);
        $this->set_akun_dp($no_po, $jenis_byr, $dp);

        try {
           
            $res = [
                'code'  => 200,
                'msg'   => 'Data Berhasil Disimpan',
                'dp'    => $dp
            ];
            DB::commit();
        } catch (\Exception $th) {
            DB::rollback();
            $res = [
                'code'  => 400,
                'msg'   => 'Data Gagal Disimpan',
                'dp'    => null
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    public function save(Request $req)
    {
       //inisialiasi surat jalan
        $id_user = session::get('id_user');
        $no_nota = $req->_noNota;
        $tgl = date("Y-m-d", strtotime($req->_tgl)) ;
        // $pelanggan = $req->_pelanggan;
        $member = $req->_idPelanggan;
        $catatan = $req->_catatan;
        $ongkir = $req->_ongkir;
        $is_cek_nota = NULL;

        //inisialiasi surat jalan detail
        $id_brg = $req->_idBrg;
        $nama_brg = $req->_namaBrg;
        $stok = $req->_stok;
        $satuan = $req->_idSatuan;
        $harga_new = $req->_hargaNew;
        $hpp = $req->_hpp;
        $harga = $req->_harga ;
        $ketr_ganti_harga = $req->_ketrGantiHarga;
        $qty = $req->_qty;
        $potongan = $req->_potongan;
        $ketr = $req->_ketr;
        $ketr_tambahan = $req->_ketrTambahan;
        $subtotal = ($qty * (isset($harga_new) ? $harga_new : $harga)) - ($qty * $potongan);
        $id_item = $req->_idItem;
        
        $data_sj = [
            'id' => $no_nota,
            'tgl' => $tgl,
            // 'pelanggan' => $pelanggan,
            'member' => $member,
            'catatan' => $catatan,
            'ongkir' => $ongkir,
            'status' => '5', // STATUS PO = 5
            'is_cek_nota' => $is_cek_nota,
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => $id_user
        ];

        $data_sj_detail = [
            'id_sj' => $no_nota,
            'id_brg' => $id_brg,
            'nama_brg' => $nama_brg,
            'satuan' => $satuan,
            'hpp' => $hpp,
            'harga' => $harga,
            'harga_new' => $harga_new,
            'ketr_ganti_harga' => $ketr_ganti_harga,
            'qty' => $qty,
            'potongan' => $potongan,
            'subtotal' => $subtotal,
            'ketr' => $ketr,
            'ketr_tambahan' => $ketr_tambahan,
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => $id_user
        ]; 

        $cek_sj = DB::table('po')->where('id',$no_nota)->first();
        $parent_jurnal = DB::table('parent_jurnal')
                                        ->where('status', 'tutup')
                                        ->orderBy('created_at', 'DESC')
                                        ->first();

        $tgl_int = strtotime($tgl);
        $tgl_akhir_int = isset($parent_jurnal) ? strtotime($parent_jurnal->tgl_akhir) : null;                                        
        
        if (!$tgl || (!$member && !$member) || (!$cek_sj && !$nama_brg)) {
            $msg = [];
            $res = [
                'code' => 400,
                'msg' => 'Data Belum Lengkap'
            ];
        } 
        elseif ($qty > $stok){
            $msg = [];
            $res = [
                'code' => 400,
                'msg' => 'Stok Tidak mencukupi'
            ];
        } elseif ($tgl_int <= $tgl_akhir_int) {
            $res = [
                'code' => 400,
                'msg' => 'Sudah tutup buku'
            ];
        } else {
            if (is_null($cek_sj)) {
                $insert_sj = DB::table('po')->insert($data_sj);
                $insert_sj_detail = DB::table('po_detail')->insertGetId($data_sj_detail);

                if ($insert_sj) {
                    $jumlah = DB::table('po_detail')->where('id_sj',$no_nota)->where('status',NULL)->sum('subtotal');
                    $update_total_sj = DB::table('po')->where('id', $no_nota)->update([
                        'total' => $jumlah
                    ]);
                    $res = [
                        'code' => 200,
                        'msg' => 'Berhasil Disimpan',
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Gagal Disimpan'
                    ];
                }
                
            } elseif (!is_null($cek_sj)) {
                if (isset($id_item)) {
                    $sj_detail = DB::table('po_detail')->where('id', $id_item)->first();

                    $update_item = DB::table('po_detail')->where('id',$id_item)->update($data_sj_detail);

                    if ($update_item) {
                        $jumlah = DB::table('po_detail')->where('id_sj',$no_nota)->where('status',NULL)->sum('subtotal');
                        $update_total_sj = DB::table('po')->where('id', $no_nota)->update([
                            'total' => $jumlah,
                            'is_cek_nota' => $is_cek_nota
                        ]);
                        $res = [
                            'code' => 200,
                            'msg' => 'Data Barang Berhasil Diupdate',
                        ];
                    } else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Data Barang Gagal Diupdate'
                        ];
                    }
                } else {
                    if (isset($nama_brg)) {
                        $insert_sj_detail = DB::table('po_detail')->insertGetId($data_sj_detail);
                        if (isset($insert_sj_detail)) {
                            $jumlah = DB::table('po_detail')->where('id_sj',$no_nota)->where('status',NULL)->sum('subtotal');
                            $update_total_sj = DB::table('po')->where('id', $no_nota)->update([
                                'total' => $jumlah,
                                'is_cek_nota' => $is_cek_nota
                            ]);
                            $res = [
                                'code' => 200,
                                'msg' => 'Data Barang Berhasil Disimpan',
                            ];
                        } else {
                            $res = [
                                'code' => 400,
                                'msg' => 'Data Barang Gagal Disimpan'
                            ];
                        }
                    }

                    $update_sj =  DB::table('po')->where('id', $no_nota)->update($data_sj);
                    
                    if ($update_sj) {
                        $res = [
                            'code' => 201,
                            'msg' => 'SJ Berhasil Diupdate',
                        ];
                    }else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Gagal update SJ !'
                        ];
                    }
                }
            }
        }
        return response()->json($res);
    }

    public function datatable_brg_detail(Request $req)
    {
        $id = $req->_idBeli;

        $data = DB::table('po_detail as a')
                    ->where('a.status', NULL)
                    ->where('a.id_sj', $id)
                    ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                    ->leftJoin('barang as c', 'a.nama_brg', '=', 'c.nama_brg')
                    ->select('a.id','a.nama_brg','a.ketr','a.ketr_tambahan','a.satuan as id_satuan','a.qty','a.harga','a.harga_new','a.potongan','a.ketr_ganti_harga',
                                'b.nama as satuan',
                                'c.kode')
                    ->get();
        return Datatables::of($data)
        ->addIndexColumn()
        ->editColumn('harga', function ($data) {
            $harga = isset($data->harga_new) ? $data->harga_new : $data->harga;
            return $harga;
        })
        ->addColumn('subtotal', function ($data) {
            $qty = $data->qty;
            $harga_item = isset($data->harga_new) ? $data->harga_new : $data->harga ;
            $potongan = $data->potongan;
            $subtotal = ($qty * $harga_item) - ($qty * $potongan);
            return number_format($subtotal, 0,',', '.');
        })
        ->addColumn('opsi', function ($data) {
            $id_item = $data->id;
            $nama_item = "'".$data->nama_brg."'";
            $harga_item = $data->harga;
            $kode_brg = $data->kode;

            $harga_new = isset($data->harga_new) ? $data->harga_new : NULL;
            $ketr_ganti_harga = isset($data->ketr_ganti_harga) ? "'".$data->ketr_ganti_harga."'" : "'"."'";
            $id_satuan = $data->id_satuan;
            $satuan = "'".$data->satuan."'";
            $qty = !is_null($data->qty) ? $data->qty : 0 ;
            $potongan = !is_null($data->potongan) ? $data->potongan : 0;
            $ketr = !is_null($data->ketr) ?  "'".$data->ketr."'" : "'"."'";
            $ketr_tambahan = !is_null($data->ketr_tambahan) ?  "'".$data->ketr_tambahan."'" : "'"."'";
            $subtotal = ($qty * $harga_item) - ($qty * $potongan);
            // return '<button type="button" class="btn btn-sm btn-primary" onclick="select_item('.$id_item.','.$kode_brg.','.$nama_item.','.$id_satuan.','.$satuan.','.$harga_item.','.$qty.','.$potongan.','.$ketr.','.$ketr_tambahan.','.$subtotal.','.$ketr_ganti_harga.','.$harga_new.')"><i class="fa fa-edit"></i></a>
            //         <button type="button" class="btn btn-sm btn-danger" onclick="delete_item('.$id_item.')"><i class="fa fa-trash"></i></button>';
                    return '<button type="button" class="btn btn-sm btn-danger" onclick="delete_item('.$id_item.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function delete_item(Request $req) 
    {
        $id_user = session::get('id_user');
        $id = $req->_idItem;

        $sj_detail = DB::table('po_detail')->where('id', $id)->first();
        // $id_detail_sj = $sj_detail->id;
        $id_sj = $sj_detail->id_sj;
        $nama_barang = $sj_detail->nama_brg;
        $ketr_barang = $sj_detail->ketr;

        $barang = DB::table('barang')->where('nama_brg', $nama_barang)->first();
        $id_barang = $barang->kode;

        $sj = DB::table('po')->where('id', $id_sj)->first();
        $opsi = $sj->opsi;

        $data_sj_detail = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];
        $res = [];

        $delete_sj = DB::table('po_detail')->where('id', $id)->delete();
        if($delete_sj) {
            $jumlah = DB::table('po_detail')->where('id_sj',$id_sj)->where('status',NULL)->sum('subtotal');
            $update_total_sj = DB::table('po')->where('id', $id_sj)->update([
                'total' => $jumlah
            ]);
            // $this->hapus_jurnal_item($id_sj, $id_barang, $ketr_barang, $id);
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function datatable_lunas(Request $req)
    {
        $id = $req->_id;

        $data = DB::table('transaksi')->where('id_sj', $id)->where('is_po', 1)->get();

        return DataTables::of($data)
        ->addIndexColumn()
        ->editColumn('jenis_byr', function ($data) {
            $jenis_byr = $data->jenis_byr;
            $urut_byr = $data->jenis_transaksi;

            return 'pembayaran ke-'.$urut_byr.' ('.$jenis_byr.')'; 
        })
        ->make(true);
    }

    public function hitung_total(Request $req)
    {
        $id = $req->_id;
        $sj = DB::table('po')->where('id', $id)->first();
        $ongkir = isset($sj) ? $sj->ongkir : NULL;
        $total = isset($sj) ? $sj->total : NULL;
        $grandtotal = $ongkir + $total;
        $data['ongkir'] = number_format($ongkir,0,',','.');
        $data['total'] = number_format($total, 0, ',', '.');
        $data['grandtotal'] = number_format($grandtotal, 0, ',', '.');

        return response()->json($data);
    }

    public function total(Request $req)
    {
        $id = $req->_id;
        $po = DB::table('po')->where('id', $id)->first();
        $transaksi = DB::table('transaksi')->where('id_sj', $id)->where('is_po', 1)->sum('total');

        $data['total_byr'] = $po->total;
        $data['total'] = $transaksi;
        $data['sisa_tagihan'] = $po->total - $transaksi;

        return response()->json($data);
    }

    public function get_ket($id_sj)
    {
        $detail_sj = DB::table('suratjalan_detail')->where('id_sj', $id_sj)->get();

        $dt = [];
        foreach ($detail_sj as $value) {
            $dt[] = $value->nama_brg;
        }

        $hasil = implode(', ', $dt);
        return $hasil;
    }

    public function set_akun($sj, $detail_sj, $jenis_byr)
    {
        $pelanggan = DB::table('pelanggan')->where('id', $sj['member'])->first();
        $a = 0;
        // HPP Triplek
        $akun[$a]['tgl'] = $sj['tgl'];
        $akun[$a]['id_item'] = NULL;
        $akun[$a]['ref'] = $sj['id'];
        $akun[$a]['hpp'] = NULL;
        $akun[$a]['grup'] = 1;
        $akun[$a]['jenis_jurnal'] = 'ina';
        $akun[$a]['nama'] = $pelanggan->nama; 
        $akun[$a]['no_akun'] = '510';
        $akun[$a]['keterangan'] = null;
        $akun[$a]['map'] = 'd';
        $akun[$a]['hit'] = '';
        $akun[$a]['qty'] = NULL;
        $akun[$a]['harga'] = 0;
        $akun[$a]['total'] = 0;

        $fase_1 = $a + 1;
        $aa = 0;

        $total_hpp = 0;
        $total_qty = 0;
        foreach ($detail_sj as $v) {
            $aa = $fase_1++;
            $subtotal_hpp = $v['qty'] * $v['hpp'];
            
            $akun[$aa]['tgl'] = $sj['tgl'];
            $akun[$aa]['id_item'] = NULL;
            $akun[$aa]['ref'] =  $sj['id'];
            $akun[$aa]['hpp'] = 1;
            $akun[$aa]['grup'] = 1;
            $akun[$aa]['jenis_jurnal'] = 'ina';
            $akun[$aa]['nama'] = $pelanggan->nama;
            $akun[$aa]['no_akun'] = '140';
            $akun[$aa]['keterangan'] = $v['nama_brg'];
            $akun[$aa]['map'] = 'k';
            $akun[$aa]['hit'] = 'b';
            $akun[$aa]['qty'] = $v['qty'];
            $akun[$aa]['harga'] =  $v['hpp'];
            $akun[$aa]['total'] = $subtotal_hpp;

            $total_hpp += $subtotal_hpp;
            $total_qty += $v['qty'];
        }

        $fase_2 = $aa + 1;

        $akun[$fase_2]['tgl'] = $sj['tgl'];
        $akun[$fase_2]['id_item'] = NULL;
        $akun[$fase_2]['ref'] = $sj['id'];
        $akun[$fase_2]['hpp'] = NULL;
        $akun[$fase_2]['grup'] = 2;
        $akun[$fase_2]['jenis_jurnal'] = 'ina';
        $akun[$fase_2]['nama'] = $pelanggan->nama;
        $akun[$fase_2]['no_akun'] = '210';
        $akun[$fase_2]['keterangan'] = NULL;
        $akun[$fase_2]['map'] = 'd';
        $akun[$fase_2]['hit'] = '';
        $akun[$fase_2]['qty'] = null;
        $akun[$fase_2]['harga'] = $v['harga'];
        $akun[$fase_2]['total'] = $v['qty'] * $v['harga'];

        $fase_3 = $fase_2 + 1;
        $b = 0;

        $total_harga = 0;
        $total_byk = 0;
        foreach ($detail_sj as $v) {
            $b = $fase_3++;
            $subtotal_harga = $v['qty'] * $v['harga'];
            
            $akun[$b]['tgl'] = $sj['tgl'];
            $akun[$b]['id_item'] = NULL;
            $akun[$b]['ref'] =  $sj['id'];
            $akun[$b]['hpp'] = 2;
            $akun[$b]['grup'] = 2;
            $akun[$b]['jenis_jurnal'] = 'ina';
            $akun[$b]['nama'] = $pelanggan->nama;
            $akun[$b]['no_akun'] = '410';
            $akun[$b]['keterangan'] = $v['nama_brg'];
            $akun[$b]['map'] = 'k';
            $akun[$b]['hit'] = 'b';
            $akun[$b]['qty'] = $v['qty'];
            $akun[$b]['harga'] =  $v['harga'];
            $akun[$b]['total'] = $subtotal_harga;

            $total_harga += $subtotal_harga;
            $total_byk += $v['qty'];
        }

        $insert= DB::table('jurnal')->insert($akun);
        
        $update_akun = DB::table('jurnal')
                            ->where('no_akun', '210')
                            ->where('ref',  $sj['id'])
                            ->where('status', NULL)
                            ->update([
                                'harga' => $total_harga,
                                'total' => $total_harga
                            ]);   

        $update_hpp = DB::table('jurnal')
                            ->where('no_akun', '510')
                            ->where('ref',  $sj['id'])
                            ->where('status', '=', NULL)
                            ->update([
                                'harga' => $total_hpp,
                                'total' => $total_hpp
                            ]);
        
    }

    public function simpan_pelunasan(Request $req)
    {
        $id_user = session::get('id_user');
        $id = $req->_id;
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $total_byr = $req->_totalByr;
        $jenis_byr = $req->_jenisByr;

        $no_byr = DB::table('transaksi')
                            ->where('id_sj', $id)
                            ->where('is_po', 1)
                            ->max('jenis_transaksi');

        $dt_transaksi = [
            'id_sj'             => $id,
            'tgl'               => $tgl,
            'total'             => $total_byr,
            'jenis_byr'         => $jenis_byr,
            'jenis_transaksi'   => $no_byr + 1,
            'is_po'             => 1
        ];

        $po = DB::table('po')->where('id', $id)->first();
        $po_detail = DB::table('po_detail')->where('id_sj', $id)->get();
       
        $dt_sj = (array)$po;
        unset($dt_sj['id_sj']);
        unset($dt_sj['id']);
        unset($dt_sj['bayar']);

        $max_id_sj = DB::table('suratjalan')->max('id');
        $no_new = $max_id_sj + 1;
        $dt_sj['id'] = $no_new;

        $dt_detail_sj = [];

        // DB::beginTransaction();

        // try {
            $insert = DB::table('transaksi')->insert($dt_transaksi);

            // DB::commit();
            $transaksi = DB::table('transaksi')
                                ->where('id_sj', $id)
                                ->where('is_po', 1)
                                ->sum('total');

            $this->set_akun_dp($id, $jenis_byr, $total_byr);

            if ($po->total <= $transaksi) {
                $dt_sj['bayar'] = $transaksi;
                if (!isset($po->id_sj)) {
                    $update_id = DB::table('po')->where('id', $id)->update(['id_sj' => $no_new]);
                    $insert_po = DB::table('suratjalan')->insert($dt_sj);
                    foreach ($po_detail as $value) {
                        $dt_detail_sj[] = [
                                // 'id'            => $value->id,
                                'id_sj'         => $no_new,
                                'id_brg'        => $value->id_brg,
                                'nama_brg'      => $value->nama_brg,
                                'ketr'          => $value->ketr,
                                'ketr_tambahan' => $value->ketr_tambahan,
                                'satuan'        => $value->satuan,
                                'qty'           => $value->qty,
                                'hpp'           => $value->hpp,
                                'harga'         => $value->harga,
                                'potongan'      => $value->potongan,
                                'subtotal'      => $value->subtotal,
                                'created_at'    => date('Y-m-d H:i:s'),
                                'user_add'      => $id_user,
                                'status'        => $value->status
                        ];
                    }
                    $insert_detail_po = DB::table('suratjalan_detail')->insert($dt_detail_sj);
                    $this->set_akun($dt_sj, $dt_detail_sj, $jenis_byr, $total_byr);
                }
            }
        //     $res = [
        //         'code'  => 200,
        //         'msg'   => 'Data Berhasil Disimpan'
        //     ];
        // } catch (Exception $th) {
        //     DB::rollback();
        //     $res = [
        //         'code'  => 400,
        //         'msg'   => 'Data Gagal Disimpan'.$th->getMessage()
        //     ];
        // }

        // $data['response'] = $res;
        // return response()->json($data);
    }

    public function delete(Request $req)
    {
        $id = $req->_id;

        $delete = DB::table('po')->where('id', $id)->delete();

        if ($delete) {
            $delete_detail_po = DB::table('po_detail')->where('id_sj', $id)->delete();
            $delete_transaksi = DB::table('transaksi')->where('id_sj', $id)->where('is_po', 1)->delete();
            $delete_jurnal = DB::table('jurnal')->where('jenis_jurnal', 'ina-dp')->where('ref', $id)->delete();
            $res = [
                'code'  => 200,
                'msg'   => 'Data Berhasil Dihapus'
            ];
        } else {
            $res = [
                'code'  => 400,
                'msg'   => 'Data Gagal Dihapus'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    public function batal_po(Request $req)
    {
        $id_users = session::get('id_user');
        $id = $req->_id;
        $ket = $req->_ket;
        
        $data_btl_sj = [
            'is_batal' => $id_users,
            'batal' => $ket,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $cek_batal = DB::table('po')->where('id', $id)->whereNotNull('is_batal')->first();

        if (isset($cek_batal)) {
            $res = [
                'code' => 400,
                'msg' => 'Data sudah pernah dibatalkan'
            ];
        } elseif (!isset($ket)){
            $res = [
                'code' => 400,
                'msg' => 'Isi Keterangan Pembatalan'
            ];
        } else {
            $update = DB::table('po')->where('id', $id)->update($data_btl_sj);

            if ($update) {

                $res = [
                    'code' => 200,
                    'msg' => 'Data berhasil dibatalkan'
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Data gagal dibatalkan'
                ];
            }
        }
        return response()->json($res);
    }


}
