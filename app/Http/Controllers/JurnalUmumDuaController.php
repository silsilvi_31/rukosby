<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\SjExport;

class JurnalUmumDuaController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function form()
    {
        $satuan = DB::table('satuan')->where('status', null)->get();
        $jenis_brg = DB::table('jenis_barang')->where('status', null)->get();
        $akun= DB::table('akun')->where('status', null)->get();

        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_brg;
        $data['akun'] = $akun;
        return view('admin.jurnalUmumDua.form')->with($data);
    }

    public function form_edit($id_ju)
    {
        $id_ju = base64_decode($id_ju);
        $jurnal_umum = DB::table('jurnal_umum as a')
                            ->leftJoin('akun as b', 'a.no_akun', '=', 'b.no_akun')    
                            ->where('id_ju', $id_ju)
                            ->first();

        $satuan = DB::table('satuan')->where('status', null)->get();
        $jenis_brg = DB::table('jenis_barang')->where('status', null)->get();
        $akun= DB::table('akun')->where('status', null)->get();

        $data['id_ju'] = $jurnal_umum->id_ju;
        $data['tgl'] = date('d-m-Y', strtotime($jurnal_umum->tgl));
        $data['nama'] = $jurnal_umum->nama;
        $data['no_akun'] = $jurnal_umum->no_akun;
        $data['nama_akun'] = $jurnal_umum->akun;
        $data['form'] = 'edit';
        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_brg;
        $data['akun'] = $akun;

        return view('admin.jurnalUmumDua.form')->with($data);
    }


    public function set_akun($jurnalUmum, $jurnalUmumDetail)
    {
        $akun[0]['tgl'] = $jurnalUmum['tgl'];
        $akun[0]['id_item'] = null;
        $akun[0]['no_akun'] = $jurnalUmum['no_akun'];
        $akun[0]['jenis_jurnal'] = 'ju';
        $akun[0]['ref'] = strtolower($jurnalUmum['id_ju']);
        $akun[0]['nama'] = $jurnalUmum['nama'];
        $akun[0]['keterangan'] = $jurnalUmumDetail['ketr'];
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = null;
        $akun[0]['grup'] = 1;
        $akun[0]['qty'] = $jurnalUmumDetail['qty'];
        $akun[0]['harga'] = $jurnalUmumDetail['harga'];
        $akun[0]['total'] = $jurnalUmumDetail['subtotal'];

        $akun[1]['tgl'] = $jurnalUmum['tgl'];
        $akun[1]['id_item'] = null;
        $akun[1]['no_akun'] = $jurnalUmumDetail['no_akun_brg'];
        $akun[1]['jenis_jurnal'] = 'ju';
        $akun[1]['ref'] = strtolower($jurnalUmum['id_ju']);
        $akun[1]['nama'] = $jurnalUmum['nama'];
        $akun[1]['keterangan'] = $jurnalUmumDetail['ketr'];
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = null;
        $akun[1]['grup'] = 2;
        $akun[1]['qty'] = $jurnalUmumDetail['qty'];
        $akun[1]['harga'] =  $jurnalUmumDetail['harga'];
        $akun[1]['total'] = $jurnalUmumDetail['subtotal'];

        $insert = DB::table('jurnal')->insert($akun);
    }

    public function update_jurnal($id_ju, $nama, $tgl)
    {
        $update_jl = DB::table('jurnal')->where('ref', $id_ju)->where('jenis_jurnal', 'ju')->update([
            'nama' => $nama,
            'tgl'  => $tgl
        ]);
    }


    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $no_jurnal_umum = $req->_noJurnalUmum;
        $tgl = date("Y-m-d", strtotime($req->_tgl)) ;
        $nama = $req->_nama;
        $no_akun_debit = $req->_noAkunDebit;

        $nama_brg = $req->_namaBrg;
        $no_akun_kredit = $req->_noAkunKredit;
        $qty = $req->_qty;
        $harga = $req->_harga;
        $subtotal = $req->_subtotal;
        $ketr = $req->_ketr;

        $data_jurnal = [
            'id_ju' => $no_jurnal_umum,
            'tgl' => $tgl,
            'nama' => $nama,
            'no_akun' => $no_akun_debit,
            'user_add'      => $id_user,
            'created_at'    => date('Y-m-d H:i:s')
        ];

        $data_detail_jurnal = [
            'id_ju' => $no_jurnal_umum,
            'nama_brg' => $nama_brg,
            'no_akun_brg' => $no_akun_kredit,
            'qty' => $qty,
            'harga' => $harga,
            'subtotal' => $subtotal,
            'ketr' => $ketr,
        ];

        $cek_ju = DB::table('jurnal_umum')->where('id_ju', $no_jurnal_umum)->first();

        if (!$tgl || !$nama || !$no_akun_debit) {
            $res = [
                'code' => 300,
                'msg' => 'Data Belum diisi lengkap'
            ];
        } else {
            if (is_null($cek_ju)) {
                $insert_ju = DB::table('jurnal_umum')->insert($data_jurnal);
                if ($insert_ju) {
                    $insert_ju_detail = DB::table('jurnal_umum_detail')->insertGetId($data_detail_jurnal);
                    $jumlah = DB::table('jurnal_umum_detail')->where('id_ju',$no_jurnal_umum)->where('status',NULL)->sum('subtotal');
                    $update_total_ju = DB::table('jurnal_umum')->where('id_ju', $no_jurnal_umum)->update([
                        'total' => $jumlah
                    ]);
                    $this->set_akun($data_jurnal, $data_detail_jurnal);
                    $res = [
                        'code' => 200,
                        'msg' => 'Data Berhasil disimpan'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Data Gagal disimpan'
                    ];
                }
            } elseif (!is_null($cek_ju)) {
                $update_ju = DB::table('jurnal_umum')->where('id_ju', $no_jurnal_umum)->update($data_jurnal);

                if ($update_ju) {
                    $data_ju = DB::table('jurnal_umum')->where('id_ju', $no_jurnal_umum)->first();
                    $cek_count = DB::table('jurnal_umum_detail')->where('id_ju', $no_jurnal_umum)->count('id');

                    $res = [
                        'code' => 200,
                        'msg' => 'Berhasil Diupdate',
                    ];

                    if ($cek_count == 0) {
                        $insert_ju_detail = DB::table('jurnal_umum_detail')->insertGetId($data_detail_jurnal);
                        $jumlah = DB::table('jurnal_umum_detail')->where('id_ju',$no_jurnal_umum)->where('status',NULL)->sum('subtotal');
                        $update_total_ju = DB::table('jurnal_umum')->where('id_ju', $no_jurnal_umum)->update([
                            'total' => $jumlah
                        ]);
                        $this->set_akun($data_jurnal, $data_detail_jurnal);
                    } elseif ( (isset($id_brg) && isset($cek_count) ) ) { 
                        $res = [
                            'code' => 300,
                            'msg' => 'Hanya Bisa input 1 transaksi',
                        ];
                    }
                    $this->update_jurnal($no_jurnal_umum, $nama, $tgl);
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Data Gagal disimpan'
                    ];
                }
            }
        }
        return response()->json($res);
    }
}
