<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\NeracaExport;

class NeracaController extends Controller
{
    private $sj;
    private $beli;
    private $kwitansi;
    private $ju;
    private $opname;
    
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");
        $this->sj = DB::table('suratjalan')
                        ->where('is_cek_nota', 1)
                        ->select('id', 'is_batal')
                        ->get();

        $this->beli = DB::table('beli')
                        ->where('is_cek_beli', 1)
                        ->select('id_beli','user_batal')
                        ->get();

        $this->kwitansi = DB::table('kwitansi')
                        ->where('is_cek_kwi', 1)
                        ->select('no_kwi')
                        ->get();

        $this->ju = DB::table('jurnal_umum')
                        ->where('is_cek_jurnal', 1)
                        ->select('id_ju')
                        ->get();

        $this->opname = DB::table('log_opname_stok')
                        ->where('is_cek_opname', 1)
                        ->select('id')
                        ->get();
    }

    public function index()
    {
        return view('admin.neraca.index');
    }

    public function total($data)
    {
        $total = 0;
        foreach ($data as $value) {
            $total += $value->total;            
        }
        return $total;
    }

    public function datatable_aktiva(Request $req)
    {
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $tgl_dua = date('Y-m-d', strtotime($req->_tglDua));

        $aktiva = DB::table('akun')
                            ->where('parent_id', 1)
                            ->orWhereNull('parent_id')
                            ->where('kel', 1)
                            ->orderBy('no_akun')
                            ->get();
        
        $parent_jurnal = DB::table('parent_jurnal')
                            ->where('status', 'tutup')
                            ->orderBy('created_at', 'DESC')
                            ->first();
    
        // dd($parent_jurnal);
        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? strtotime($parent_jurnal->tgl_akhir) : NULL;
        $tgl_awal_int = strtotime($tgl);
        $tgl_dua_int = strtotime($tgl_dua);

        if ( $tgl_awal_int <= $tgl_akhir ) {
            $tgl = NULL;
        }

        if ( $tgl_dua_int <= $tgl_akhir ) {
           $tgl_dua = NULL;
        }

        // dd([$tgl, $tgl_dua]);

        $dt = [];
        $total_d = 0;
        $total_k = 0;
        $total_all = 0;

        $fer = 0;

        $penjualan = $this->sj->map(function ($val) {
            return $val->id;
        });

        $pembelian = $this->beli->map(function ($val) {
            return $val->id_beli;
        });

        $batal_pembelian = $this->beli->filter(function ($val) {
            return isset($val->user_batal);
        })->map(function ($v) {
            return $v->id_beli;
        });

        $batal_penjualan = $this->sj->filter(function ($val) {
            return isset($val->is_batal);
        })->map(function ($v) {
            return $v->id;
        });

        $kwitansi_ = $this->kwitansi->map(function ($val) {
            return $val->no_kwi;
        });

        $jurnal_umum = $this->ju->map(function ($val) {
            return $val->id_ju;
        });

        $opname = $this->opname->map(function ($val) {
            return $val->id;
        });
                                
        foreach ($aktiva as $v) {
            //debit
            $sql_data_debit_penjualan = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $penjualan)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'ina');
            
            $sql_data_debit_pembelian = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('bm', $pembelian)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'beli');

            $sql_data_debit_kwitansi = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $kwitansi_)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'kwitansi');

            $sql_data_debit_ju = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $jurnal_umum)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'ju');

            $sql_data_debit_opname = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $opname)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'opname');

            $sql_data_debit_lain = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->where('status', null)
                                                ->whereIn('jenis_jurnal', ['beli-batal','ina-batal','beli-retur', 'ina-retur', 'ina-dp', 'lr', 'gaji', 'gaji-mingguan', 'awalan' ]);

            //kredit
            $sql_data_kredit_penjualan = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $penjualan)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'ina');

            $sql_data_kredit_pembelian = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('bm', $pembelian)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'beli');

            $sql_data_kredit_kwitansi = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $kwitansi_)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'kwitansi');

            $sql_data_kredit_ju = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $jurnal_umum)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'ju');

            $sql_data_kredit_opname = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $opname)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'opname');

            $sql_data_kredit_lain = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->where('status', null)
                                                ->whereIn('jenis_jurnal', ['beli-batal','ina-batal','beli-retur', 'ina-retur', 'ina-dp', 'lr', 'gaji', 'gaji-mingguan','awalan' ]);                                

            // $sql_data_debit = DB::table('jurnal')
            //                     ->where('no_akun', $v->no_akun)
            //                     ->where('map', 'd');

            // $sql_data_kredit = DB::table('jurnal')
            //                     ->where('no_akun', $v->no_akun)
            //                     ->where('map', 'k');
                                

            if (isset($tgl) && isset($tgl_dua)) {
                $data_debit_penjualan = $sql_data_debit_penjualan->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_debit_pembelian = $sql_data_debit_pembelian->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_debit_kwitansi = $sql_data_debit_kwitansi->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_debit_ju = $sql_data_debit_ju->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_debit_opname = $sql_data_debit_opname->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_debit_lain = $sql_data_debit_lain->whereBetween('tgl', [$tgl, $tgl_dua])->get();

                $data_debit = $data_debit_penjualan->merge($data_debit_pembelian)
                                                    ->merge($data_debit_kwitansi)
                                                    ->merge($data_debit_ju)
                                                    ->merge($data_debit_opname)
                                                    ->merge($data_debit_lain);

                $data_kredit_penjualan = $sql_data_kredit_penjualan->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit_pembelian = $sql_data_kredit_pembelian->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit_kwitansi = $sql_data_kredit_kwitansi->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit_ju = $sql_data_kredit_ju->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit_opname = $sql_data_kredit_opname->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit_lain = $sql_data_kredit_lain->whereBetween('tgl', [$tgl, $tgl_dua])->get();

                $data_kredit = $data_kredit_penjualan->merge($data_kredit_pembelian)
                                                    ->merge($data_kredit_kwitansi)
                                                    ->merge($data_kredit_ju)
                                                    ->merge($data_kredit_opname)
                                                    ->merge($data_kredit_lain);

                // $data_debit = $sql_data_debit->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                // $data_kredit = $sql_data_kredit->whereBetween('tgl', [$tgl, $tgl_dua])->get();
            } 

            if (isset($tgl) && !isset($tgl_dua)) {
                $data_debit_penjualan = $sql_data_debit_penjualan->where('tgl', $tgl)->get();
                $data_debit_pembelian = $sql_data_debit_pembelian->where('tgl', $tgl)->get();
                $data_debit_kwitansi = $sql_data_debit_kwitansi->where('tgl', $tgl)->get();
                $data_debit_ju = $sql_data_debit_ju->where('tgl', $tgl)->get();
                $data_debit_opname = $sql_data_debit_opname->where('tgl', $tgl)->get();
                $data_debit_lain = $sql_data_debit_lain->where('tgl', $tgl)->get();

                $data_debit = $data_debit_penjualan->merge($data_debit_pembelian)
                                                    ->merge($data_debit_kwitansi)
                                                    ->merge($data_debit_ju)
                                                    ->merge($data_debit_opname)
                                                    ->merge($data_debit_lain);

                $data_kredit_penjualan = $sql_data_kredit_penjualan->where('tgl', $tgl)->get();
                $data_kredit_pembelian = $sql_data_kredit_pembelian->where('tgl', $tgl)->get();
                $data_kredit_kwitansi = $sql_data_kredit_kwitansi->where('tgl', $tgl)->get();
                $data_kredit_ju = $sql_data_kredit_ju->where('tgl', $tgl)->get();
                $data_kredit_opname = $sql_data_kredit_opname->where('tgl', $tgl)->get();
                $data_kredit_lain = $sql_data_kredit_lain->where('tgl', $tgl)->get();

                $data_kredit = $data_kredit_penjualan->merge($data_kredit_pembelian)
                                                    ->merge($data_kredit_kwitansi)
                                                    ->merge($data_kredit_ju)
                                                    ->merge($data_kredit_opname)
                                                    ->merge($data_kredit_lain);

                // $data_debit = $sql_data_debit->where('tgl', $tgl)->get();
                // $data_kredit = $sql_data_kredit->where('tgl', $tgl)->get();
            }

            if (isset($tgl_dua) && !isset($tgl)) {
                $data_debit_penjualan = $sql_data_debit_penjualan->where('tgl', $tgl_dua)->get();
                $data_debit_pembelian = $sql_data_debit_pembelian->where('tgl', $tgl_dua)->get();
                $data_debit_kwitansi = $sql_data_debit_kwitansi->where('tgl', $tgl_dua)->get();
                $data_debit_ju = $sql_data_debit_ju->where('tgl', $tgl_dua)->get();
                $data_debit_opname = $sql_data_debit_opname->where('tgl', $tgl_dua)->get();
                $data_debit_lain = $sql_data_debit_lain->where('tgl', $tgl_dua)->get();

                $data_debit = $data_debit_penjualan->merge($data_debit_pembelian)
                                                    ->merge($data_debit_kwitansi)
                                                    ->merge($data_debit_ju)
                                                    ->merge($data_debit_opname)
                                                    ->merge($data_debit_lain);

                $data_kredit_penjualan = $sql_data_kredit_penjualan->where('tgl', $tgl_dua)->get();
                $data_kredit_pembelian = $sql_data_kredit_pembelian->where('tgl', $tgl_dua)->get();
                $data_kredit_kwitansi = $sql_data_kredit_kwitansi->where('tgl', $tgl_dua)->get();
                $data_kredit_ju = $sql_data_kredit_ju->where('tgl', $tgl_dua)->get();
                $data_kredit_opname = $sql_data_kredit_opname->where('tgl', $tgl_dua)->get();
                $data_kredit_lain = $sql_data_kredit_lain->where('tgl', $tgl_dua)->get();

                $data_kredit = $data_kredit_penjualan->merge($data_kredit_pembelian)
                                                    ->merge($data_kredit_kwitansi)
                                                    ->merge($data_kredit_ju)
                                                    ->merge($data_kredit_opname)
                                                    ->merge($data_kredit_lain);

                // $data_debit = $sql_data_debit->where('tgl', $tgl_dua)->get();
                // $data_kredit = $sql_data_kredit->where('tgl', $tgl_dua)->get();
            }

            if ( !isset($tgl) && !isset($tgl_dua) ) {
                $data_debit_penjualan = $sql_data_debit_penjualan->where('tgl', '')->get();
                $data_debit_pembelian = $sql_data_debit_pembelian->where('tgl', '')->get();
                $data_debit_kwitansi = $sql_data_debit_kwitansi->where('tgl', '')->get();
                $data_debit_ju = $sql_data_debit_ju->where('tgl', '')->get();
                $data_debit_opname = $sql_data_debit_opname->where('tgl', '')->get();
                $data_debit_lain = $sql_data_debit_lain->where('tgl', '')->get();

                $data_debit = $data_debit_penjualan->merge($data_debit_pembelian)
                                                    ->merge($data_debit_kwitansi)
                                                    ->merge($data_debit_ju)
                                                    ->merge($data_debit_opname)
                                                    ->merge($data_debit_lain);

                $data_kredit_penjualan = $sql_data_kredit_penjualan->where('tgl', '')->get();
                $data_kredit_pembelian = $sql_data_kredit_pembelian->where('tgl', '')->get();
                $data_kredit_kwitansi = $sql_data_kredit_kwitansi->where('tgl', '')->get();
                $data_kredit_ju = $sql_data_kredit_ju->where('tgl', '')->get();
                $data_kredit_opname = $sql_data_kredit_opname->where('tgl', '')->get();
                $data_kredit_lain = $sql_data_kredit_lain->where('tgl', '')->get();

                $data_kredit = $data_kredit_penjualan->merge($data_kredit_pembelian)
                                                    ->merge($data_kredit_kwitansi)
                                                    ->merge($data_kredit_ju)
                                                    ->merge($data_kredit_opname)
                                                    ->merge($data_kredit_lain);

                // $data_debit = $sql_data_debit->where('tgl', '')->get();
                // $data_kredit = $sql_data_kredit->where('tgl', '')->get();
            }
             
            $debit =  $this->total($data_debit);
            $kredit = $this->total($data_kredit);

            $total = $debit - $kredit;

            $total_fix = ($total) < 0 ? '('.str_replace('-', '', number_format($total, 0, ',', '.')).')' : number_format($total, 0, ',', '.');
           
            $dt[] = [
                        'no_akun' => $v->no_akun,
                        'akun' => $v->akun,
                        'parent_id' => $v->parent_id,
                        'total_debit' => number_format($debit, 0, ',', '.'),
                        'total_kredit' => number_format($kredit, 0, ',', '.'),
                        'total' => $total_fix,
            ];

            $total_d += $debit;
            $total_k += $kredit;
        }

        $total_all = $total_d - ($total_k);
        $total_all_fix = ($total_all) < 0 ? '('.str_replace('-', '', number_format($total_all, 0, ',', '.')).')' : number_format($total_all, 0, ',', '.');
        $dataQ['tt_debit'] = number_format($total_d, 0, ',', '.');
        $dataQ['tt_kredit'] = number_format($total_k, 0, ',', '.');
        $dataQ['total'] = $total_all_fix;
        $dataQ['data'] = $dt;
        // dd($dataQ);
        return response()->json($dataQ);
    }

    public function get_laba($tgl, $tgl_dua)
    {
        $penjualan = $this->sj->map(function ($val) {
            return $val->id;
        });

        $pembelian = $this->beli->map(function ($val) {
            return $val->id_beli;
        });

        $batal_pembelian = $this->beli->filter(function ($val) {
            return isset($val->user_batal);
        })->map(function ($v) {
            return $v->id_beli;
        });

        $batal_penjualan = $this->sj->filter(function ($val) {
            return isset($val->is_batal);
        })->map(function ($v) {
            return $v->id;
        });

        $kwitansi_ = $this->kwitansi->map(function ($val) {
            return $val->no_kwi;
        });

        $jurnal_umum = $this->ju->map(function ($val) {
            return $val->id_ju;
        });

        $opname = $this->opname->map(function ($val) {
            return $val->id;
        });

        //debit
        $sql_data_debit_penjualan = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'd')
                                        ->whereIn('ref', $penjualan)
                                        ->where('status', null)
                                        ->where('jenis_jurnal', 'ina')
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();

        $sql_data_debit_pembelian = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'd')
                                        ->whereIn('bm', $pembelian)
                                        ->where('status', null)
                                        ->where('jenis_jurnal', 'beli')
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();

        $sql_data_debit_kwitansi = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'd')
                                        ->whereIn('ref', $kwitansi_)
                                        ->where('status', null)
                                        ->where('jenis_jurnal', 'kwitansi')
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();

        $sql_data_debit_ju = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'd')
                                        ->whereIn('ref', $jurnal_umum
                                        ->where('status', null))
                                        ->where('jenis_jurnal', 'ju')
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();

        $sql_data_debit_opname = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'd')
                                        ->whereIn('ref', $opname)
                                        ->where('status', null)
                                        ->where('jenis_jurnal', 'opname')
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();

        $sql_data_debit_lain = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'd')
                                        ->whereIn('jenis_jurnal', ['beli-batal','ina-batal','beli-retur', 'ina-retur', 'ina-dp', 'lr', 'gaji', 'gaji-mingguan','awalan' ])
                                        ->where('status', null)
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();
        
        // //kredit
        $sql_data_kredit_penjualan = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'k')
                                        ->whereIn('ref', $penjualan)
                                        ->where('status', null)
                                        ->where('jenis_jurnal', 'ina')
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();

        $sql_data_kredit_pembelian = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'k')
                                        ->whereIn('bm', $pembelian)
                                        ->where('status', null)
                                        ->where('jenis_jurnal', 'beli')
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();

        $sql_data_kredit_kwitansi = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'k')
                                        ->whereIn('ref', $kwitansi_)
                                        ->where('status', null)
                                        ->where('jenis_jurnal', 'kwitansi')
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();

        $sql_data_kredit_ju = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'k')
                                        ->whereIn('ref', $jurnal_umum
                                        ->where('status', null))
                                        ->where('jenis_jurnal', 'ju')
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();

        $sql_data_kredit_opname = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'k')
                                        ->whereIn('ref', $opname)
                                        ->where('status', null)
                                        ->where('jenis_jurnal', 'opname')
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();

        $sql_data_kredit_lain = DB::table('jurnal')
                                        ->whereBetween('no_akun', ['400', '699'])
                                        ->where('map', 'k')
                                        ->whereIn('jenis_jurnal', ['beli-batal','ina-batal','beli-retur', 'ina-retur', 'ina-dp', 'lr', 'gaji', 'gaji-mingguan','awalan' ])
                                        ->where('status', null)
                                        ->whereBetween('tgl', [$tgl, $tgl_dua])
                                        // ->whereNotIn('id', $id_jurnalQ)
                                        ->get();
                        
        $sql_data_debit = $sql_data_debit_penjualan->merge($sql_data_debit_pembelian)
                                        ->merge($sql_data_debit_kwitansi)
                                        ->merge($sql_data_debit_ju)
                                        ->merge($sql_data_debit_opname)
                                        ->merge($sql_data_debit_lain);   
                                        
        $sql_data_kredit = $sql_data_kredit_penjualan->merge($sql_data_kredit_pembelian)
                                        ->merge($sql_data_kredit_kwitansi)
                                        ->merge($sql_data_kredit_ju)
                                        ->merge($sql_data_kredit_opname)
                                        ->merge($sql_data_kredit_lain);   

        $debit =  $this->total($sql_data_debit);
        $kredit = $this->total($sql_data_kredit); 
        
        // $total = $debit - $kredit;
        // $sql_data_debit = DB::table('jurnal')
        //                         ->whereBetween('no_akun', ['400', '600'])
        //                         ->whereBetween('tgl', [$tgl, $tgl_dua])
        //                         ->where('map', 'd')
        //                         ->sum('total');

        // $sql_data_kredit = DB::table('jurnal')
        //                         ->whereBetween('no_akun', ['400', '600'])
        //                         ->whereBetween('tgl', [$tgl, $tgl_dua])
        //                         ->where('map', 'k')
        //                         ->sum('total');

        $total_laba = 0;

        $total_laba = abs($debit - $kredit);
        $laba = $debit - $kredit;

        $data['total_debit'] = $debit;
        $data['total_kredit'] = $kredit;
        $data['total_laba'] = $total_laba;
        $data['laba'] = $laba;

        return $data;
    }

    public function datatable_pasiva(Request $req)
    {
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $tgl_dua = date('Y-m-d', strtotime($req->_tglDua));

        $pasiva = DB::table('akun')
                            ->whereIn('parent_id', [2, 3])
                            ->orWhereNull('parent_id')
                            ->whereIn('kel', [2,3])
                            ->orderBy('no_akun')
                            ->get();

        $parent_jurnal = DB::table('parent_jurnal')
                            ->where('status', 'tutup')
                            ->orderBy('created_at', 'DESC')
                            ->first();
                            
    
        // dd($parent_jurnal);
        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? strtotime($parent_jurnal->tgl_akhir) : NULL;
        $tgl_awal_int = strtotime($tgl);
        $tgl_dua_int = strtotime($tgl_dua);

        if ( $tgl_awal_int <= $tgl_akhir ) {
            $tgl = NULL;
        }

        if ( $tgl_dua_int <= $tgl_akhir ) {
           $tgl_dua = NULL;
        }

        $dt = [];
        $total_d = 0;
        $total_k = 0;
        $total_all = 0;
        $total_laba = $this->get_laba($tgl, $tgl_dua)['total_laba'];
        $laba = $this->get_laba($tgl, $tgl_dua)['laba'];

        $penjualan = $this->sj->map(function ($val) {
            return $val->id;
        });

        $pembelian = $this->beli->map(function ($val) {
            return $val->id_beli;
        });

        $batal_pembelian = $this->beli->filter(function ($val) {
            return isset($val->user_batal);
        })->map(function ($v) {
            return $v->id_beli;
        });

        $batal_penjualan = $this->sj->filter(function ($val) {
            return isset($val->is_batal);
        })->map(function ($v) {
            return $v->id;
        });

        $kwitansi_ = $this->kwitansi->map(function ($val) {
            return $val->no_kwi;
        });

        $jurnal_umum = $this->ju->map(function ($val) {
            return $val->id_ju;
        });

        $opname = $this->opname->map(function ($val) {
            return $val->id;
        });

        foreach ($pasiva as $v) {
            //debit
            $sql_data_debit_penjualan = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $penjualan)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'ina');
            
            $sql_data_debit_pembelian = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('bm', $pembelian)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'beli');

            $sql_data_debit_kwitansi = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $kwitansi_)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'kwitansi');

            $sql_data_debit_ju = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $jurnal_umum)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'ju');

            $sql_data_debit_opname = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->whereIn('ref', $opname)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'opname');

            $sql_data_debit_lain = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'd')
                                                ->where('status', null)
                                                ->whereIn('jenis_jurnal', ['beli-batal','ina-batal','beli-retur', 'ina-retur', 'ina-dp', 'lr', 'gaji', 'gaji-mingguan', 'awalan' ]);

             //kredit
             $sql_data_kredit_penjualan = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $penjualan)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'ina');

            $sql_data_kredit_pembelian = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('bm', $pembelian)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'beli');

            $sql_data_kredit_kwitansi = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $kwitansi_)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'kwitansi');

            $sql_data_kredit_ju = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $jurnal_umum)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'ju');

            $sql_data_kredit_opname = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->whereIn('ref', $opname)
                                                ->where('status', null)
                                                ->where('jenis_jurnal', 'opname');

            $sql_data_kredit_lain = DB::table('jurnal')
                                                ->where('no_akun', $v->no_akun)
                                                ->where('map', 'k')
                                                ->where('status', null)
                                                ->whereIn('jenis_jurnal', ['beli-batal','ina-batal','beli-retur', 'ina-retur', 'ina-dp', 'lr', 'gaji', 'gaji-mingguan', 'awalan' ]);                                   

            // $sql_data_debit = DB::table('jurnal')
            //                     ->where('no_akun', $v->no_akun)
            //                     ->where('map', 'd');
                                
            // $sql_data_kredit = DB::table('jurnal')
            //                     ->where('no_akun', $v->no_akun)
            //                     ->where('map', 'k');

            if (isset($tgl) && isset($tgl_dua)) {
                $data_debit_penjualan = $sql_data_debit_penjualan->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_debit_pembelian = $sql_data_debit_pembelian->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_debit_kwitansi = $sql_data_debit_kwitansi->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_debit_ju = $sql_data_debit_ju->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_debit_opname = $sql_data_debit_opname->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_debit_lain = $sql_data_debit_lain->whereBetween('tgl', [$tgl, $tgl_dua])->get();

                $data_debit = $data_debit_penjualan->merge($data_debit_pembelian)
                                                    ->merge($data_debit_kwitansi)
                                                    ->merge($data_debit_ju)
                                                    ->merge($data_debit_opname)
                                                    ->merge($data_debit_lain);

                $data_kredit_penjualan = $sql_data_kredit_penjualan->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit_pembelian = $sql_data_kredit_pembelian->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit_kwitansi = $sql_data_kredit_kwitansi->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit_ju = $sql_data_kredit_ju->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit_opname = $sql_data_kredit_opname->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit_lain = $sql_data_kredit_lain->whereBetween('tgl', [$tgl, $tgl_dua])->get();

                $data_kredit = $data_kredit_penjualan->merge($data_kredit_pembelian)
                                                    ->merge($data_kredit_kwitansi)
                                                    ->merge($data_kredit_ju)
                                                    ->merge($data_kredit_opname)
                                                    ->merge($data_kredit_lain);

                // $data_debit = $sql_data_debit->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                // $data_kredit = $sql_data_kredit->whereBetween('tgl', [$tgl, $tgl_dua])->get();
            } 

            if (isset($tgl) && !isset($tgl_dua)) {
                $data_debit_penjualan = $sql_data_debit_penjualan->where('tgl', $tgl)->get();
                $data_debit_pembelian = $sql_data_debit_pembelian->where('tgl', $tgl)->get();
                $data_debit_kwitansi = $sql_data_debit_kwitansi->where('tgl', $tgl)->get();
                $data_debit_ju = $sql_data_debit_ju->where('tgl', $tgl)->get();
                $data_debit_opname = $sql_data_debit_opname->where('tgl', $tgl)->get();
                $data_debit_lain = $sql_data_debit_lain->where('tgl', $tgl)->get();

                $data_debit = $data_debit_penjualan->merge($data_debit_pembelian)
                                                    ->merge($data_debit_kwitansi)
                                                    ->merge($data_debit_ju)
                                                    ->merge($data_debit_opname)
                                                    ->merge($data_debit_lain);

                $data_kredit_penjualan = $sql_data_kredit_penjualan->where('tgl', $tgl)->get();
                $data_kredit_pembelian = $sql_data_kredit_pembelian->where('tgl', $tgl)->get();
                $data_kredit_kwitansi = $sql_data_kredit_kwitansi->where('tgl', $tgl)->get();
                $data_kredit_ju = $sql_data_kredit_ju->where('tgl', $tgl)->get();
                $data_kredit_opname = $sql_data_kredit_opname->where('tgl', $tgl)->get();
                $data_kredit_lain = $sql_data_kredit_lain->where('tgl', $tgl)->get();

                $data_kredit = $data_kredit_penjualan->merge($data_kredit_pembelian)
                                                    ->merge($data_kredit_kwitansi)
                                                    ->merge($data_kredit_ju)
                                                    ->merge($data_kredit_opname)
                                                    ->merge($data_kredit_lain);

                // $data_debit = $sql_data_debit->where('tgl', $tgl)->get();
                // $data_kredit = $sql_data_kredit->where('tgl', $tgl)->get();
            }

            if (isset($tgl_dua) && !isset($tgl)) {
                $data_debit_penjualan = $sql_data_debit_penjualan->where('tgl', $tgl_dua)->get();
                $data_debit_pembelian = $sql_data_debit_pembelian->where('tgl', $tgl_dua)->get();
                $data_debit_kwitansi = $sql_data_debit_kwitansi->where('tgl', $tgl_dua)->get();
                $data_debit_ju = $sql_data_debit_ju->where('tgl', $tgl_dua)->get();
                $data_debit_opname = $sql_data_debit_opname->where('tgl', $tgl_dua)->get();
                $data_debit_lain = $sql_data_debit_lain->where('tgl', $tgl_dua)->get();

                $data_debit = $data_debit_penjualan->merge($data_debit_pembelian)
                                                    ->merge($data_debit_kwitansi)
                                                    ->merge($data_debit_ju)
                                                    ->merge($data_debit_opname)
                                                    ->merge($data_debit_lain);

                $data_kredit_penjualan = $sql_data_kredit_penjualan->where('tgl', $tgl_dua)->get();
                $data_kredit_pembelian = $sql_data_kredit_pembelian->where('tgl', $tgl_dua)->get();
                $data_kredit_kwitansi = $sql_data_kredit_kwitansi->where('tgl', $tgl_dua)->get();
                $data_kredit_ju = $sql_data_kredit_ju->where('tgl', $tgl_dua)->get();
                $data_kredit_opname = $sql_data_kredit_opname->where('tgl', $tgl_dua)->get();
                $data_kredit_lain = $sql_data_kredit_lain->where('tgl', $tgl_dua)->get();

                $data_kredit = $data_kredit_penjualan->merge($data_kredit_pembelian)
                                                    ->merge($data_kredit_kwitansi)
                                                    ->merge($data_kredit_ju)
                                                    ->merge($data_kredit_opname)
                                                    ->merge($data_kredit_lain);

                // $data_debit = $sql_data_debit->where('tgl', $tgl_dua)->get();
                // $data_kredit = $sql_data_kredit->where('tgl', $tgl_dua)->get();
            }

            if ( !isset($tgl) && !isset($tgl_dua) ) {
                $data_debit_penjualan = $sql_data_debit_penjualan->where('tgl', '')->get();
                $data_debit_pembelian = $sql_data_debit_pembelian->where('tgl', '')->get();
                $data_debit_kwitansi = $sql_data_debit_kwitansi->where('tgl', '')->get();
                $data_debit_ju = $sql_data_debit_ju->where('tgl', '')->get();
                $data_debit_opname = $sql_data_debit_opname->where('tgl', '')->get();
                $data_debit_lain = $sql_data_debit_lain->where('tgl', '')->get();

                $data_debit = $data_debit_penjualan->merge($data_debit_pembelian)
                                                    ->merge($data_debit_kwitansi)
                                                    ->merge($data_debit_ju)
                                                    ->merge($data_debit_opname)
                                                    ->merge($data_debit_lain);

                $data_kredit_penjualan = $sql_data_kredit_penjualan->where('tgl', '')->get();
                $data_kredit_pembelian = $sql_data_kredit_pembelian->where('tgl', '')->get();
                $data_kredit_kwitansi = $sql_data_kredit_kwitansi->where('tgl', '')->get();
                $data_kredit_ju = $sql_data_kredit_ju->where('tgl', '')->get();
                $data_kredit_opname = $sql_data_kredit_opname->where('tgl', '')->get();
                $data_kredit_lain = $sql_data_kredit_lain->where('tgl', '')->get();

                $data_kredit = $data_kredit_penjualan->merge($data_kredit_pembelian)
                                                    ->merge($data_kredit_kwitansi)
                                                    ->merge($data_kredit_ju)
                                                    ->merge($data_kredit_opname)
                                                    ->merge($data_kredit_lain);

                // $data_debit = $sql_data_debit->where('tgl', '')->get();
                // $data_kredit = $sql_data_kredit->where('tgl', '')->get();
            }
             
            $debit =  $this->total($data_debit);
            $kredit = $this->total($data_kredit);

            $total = $debit - $kredit;

            $total_fix = ($total) < 0 ? '('.str_replace('-', '', number_format($total, 0, ',', '.')).')' : number_format($total, 0, ',', '.');
           
            $dt[] = [
                        'no_akun' => $v->no_akun,
                        'akun' => $v->akun,
                        'parent_id' => $v->parent_id,
                        'total_debit' => number_format($debit, 0, ',', '.'),
                        'total_kredit' => number_format($kredit, 0, ',', '.'),
                        'total' => ($v->no_akun == 330) ? number_format($total_laba, 0, ',', '.') : $total_fix,
                        // 'opsi' => 'opsi'
            ];

            $total_d += $debit;
            $total_k += $kredit;
        }

        $total_all = $total_d - ($total_k) + $laba;
        $total_all_fix = ($total_all) < 0 ? '('.str_replace('-', '', number_format($total_all, 0, ',', '.')).')' : number_format($total_all, 0, ',', '.');
        $dataQ['tt_debit'] = number_format($total_d, 0, ',', '.');
        $dataQ['tt_kredit'] = number_format($total_k, 0, ',', '.');
        $dataQ['total'] = $total_all_fix;
        $dataQ['data'] = $dt;
        $dataQ['ttl'] = $this->get_laba($tgl, $tgl_dua)['total_laba'];
        // dd($dataQ);
        return response()->json($dataQ);
    }

    public function excel_neraca($tgl)
    {
        $pecah = explode('&', $tgl);
          $tgl_m = $pecah[0];
            $tgl_a = $pecah[1];  

            $tgl_m_format = date('Y-m-d', strtotime($tgl_m));
            $tgl_a_format = date('Y-m-d', strtotime($tgl_a));

            // AWAL AKTIVA
            $aktiva = DB::table('akun')
                            ->where('parent_id', 1)
                            ->orWhereNull('parent_id')
                            ->where('kel', 1)
                            ->orderBy('no_akun')
                            ->get();

            $dt_aktiva = [];
            $total_d_aktiva = 0;
            $total_k_aktiva = 0;
            $total_all_aktiva = 0;
            foreach ($aktiva as $v) {
                $data_debit_aktiva = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'd')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
    
                $data_kredit_aktiva = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'k')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
                    
                $debit_aktiva =  $this->total($data_debit_aktiva);
                $kredit_aktiva = $this->total($data_kredit_aktiva);
    
                $total_aktiva = $debit_aktiva - $kredit_aktiva;
    
                $total_fix_aktiva = ($total_aktiva) < 0 ? '('.str_replace('-', '', $total_aktiva).')' : $total_aktiva;
                
                $dt_aktiva[] = (object) [
                            'no_akun_aktiva' => $v->no_akun,
                            'akun_aktiva' => $v->akun,
                            'parent_id_aktiva' => $v->parent_id,
                            'debit_aktiva' => $debit_aktiva,
                            'kredit_aktiva' => $kredit_aktiva,
                            'total_aktiva' => $total_fix_aktiva,
                ];
    
                $total_d_aktiva += $debit_aktiva;
                $total_k_aktiva += $kredit_aktiva;
            }
            $total_all_aktiva = $total_d_aktiva - ($total_k_aktiva);
            $total_all_fix = ($total_all_aktiva) < 0 ? '('.str_replace('-', '', $total_all_aktiva).')' : $total_all_aktiva;
            // AKHIR AKTIVA

            // AWAL PASIVA
            $pasiva = DB::table('akun')
                            ->whereIn('parent_id', [2, 3])
                            ->orWhereNull('parent_id')
                            ->whereIn('kel', [2,3])
                            ->orderBy('no_akun')
                            ->get();

            $dt_pasiva = [];
            $total_d_pasiva = 0;
            $total_k_pasiva = 0;
            $total_all_pasiva = 0;
            foreach ($pasiva as $v) {
                $data_debit_pasiva = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'd')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
    
                $data_kredit_pasiva = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'k')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
                    
                $debit_pasiva =  $this->total($data_debit_pasiva);
                $kredit_pasiva = $this->total($data_kredit_pasiva);
    
                $total_pasiva = $debit_pasiva - $kredit_pasiva;
    
                $total_fix_pasiva = ($total_pasiva) < 0 ? '('.str_replace('-', '', $total_pasiva).')' : $total_pasiva;
                
                $dt_pasiva[] = (object) [
                            'no_akun_pasiva' => $v->no_akun,
                            'akun_pasiva' => $v->akun,
                            'parent_id_pasiva' => $v->parent_id,
                            'debit_pasiva' => $debit_pasiva,
                            'kredit_pasiva' => $kredit_pasiva,
                            'total_pasiva' => $total_fix_pasiva,
                ];
    
                $total_d_pasiva += $debit_pasiva;
                $total_k_pasiva += $kredit_pasiva;
            }
            $total_all_pasiva = $total_d_pasiva - ($total_k_pasiva);
            $total_all_fix_pasiva = ($total_all_pasiva) < 0 ? '('.str_replace('-', '', $total_all_pasiva).')' : $total_all_pasiva;
            // AKHIR PASIVA

            $dt['rekap'] = $dt_aktiva;
            $dt['tt_debit_aktiva'] = $total_d_aktiva;
            $dt['tt_kredit_aktiva'] = $total_k_aktiva;
            $dt['tt_fix_aktiva'] = $total_all_fix;
            $dt['rekap_pasiva'] = $dt_pasiva;
            $dt['tt_debit_pasiva'] = $total_d_pasiva;
            $dt['tt_kredit_pasiva'] = $total_k_pasiva;
            $dt['tt_fix_pasiva'] = $total_all_fix_pasiva;
            $dt['tgl_m'] = $tgl_m;
            $dt['tgl_a'] = $tgl_a;

        $nama_file = "Rekap Neraca ".$tgl_m."-".$tgl_a.".xlsx";
        return Excel::download(new NeracaExport($dt), $nama_file); 
    }

    public function cek_tgl(Request $req)
    {
        $tgl = strtotime($req->_tgl);

        $parent_jurnal = DB::table('parent_jurnal')
                                        ->where('status', 'tutup')
                                        ->orderBy('created_at', 'DESC')
                                        ->first();
                
        // dd($parent_jurnal);
        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? strtotime($parent_jurnal->tgl_akhir) : NULL;

        if (!is_null($tgl_akhir)) {
            if ($tgl > $tgl_akhir) {
                $res = [
                        'tgl' => $req->_tgl
                ];
            } else {
                $res = [
                    'tgl' => null
                ];
            }
        } else {
            $res = [
                'tgl' => $req->_tgl
            ];
        }
        return response()->json($res);
    }

}